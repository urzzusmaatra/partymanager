using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrostSnail.Core.Identifier
{
    [CreateAssetMenu(fileName = "Identifier", menuName = "FrostSnail/Core/Identifier")]
    public class Identifier : ScriptableObject
    {
        public int Id { get => this.GetInstanceID(); }
    }
}