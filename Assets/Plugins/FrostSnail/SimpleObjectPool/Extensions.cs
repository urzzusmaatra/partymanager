﻿using System.Linq;

namespace FrostSnail.SimpleObjectPool.Internal
{
    public static class Extensions
    {
        public static bool TryRemoveObject(this IPoolableObject[] arr, IPoolableObject obj)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                if(arr[i] == obj)
                {
                    arr[i] = null;
                    arr.OrderBy(x => x != null);
                    return true;
                }
            }

            return false;
        }

        public static bool TryPopObject(this IPoolableObject[] arr, out IPoolableObject obj)
        {
            //allways poping from the back so we dont need to order
            obj = default(IPoolableObject);

            for (int i = arr.Length - 1; i >= 0; i--)
            {
                if(arr[i] != null)
                {
                    obj = arr[i];
                    arr[i] = null;
                    return true;
                }
            }

            return false;
        }

        public static bool TryAdd(this IPoolableObject[] arr, IPoolableObject obj)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                if(arr[i] == null)
                {
                    arr[i] = obj;
                    return true;
                }
            }

            return false;
        }
    }
}