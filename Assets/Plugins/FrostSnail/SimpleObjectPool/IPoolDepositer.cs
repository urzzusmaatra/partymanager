﻿namespace FrostSnail.SimpleObjectPool
{
    public interface IPoolDepositer
    {
        void Deposit(IPoolableObject depositingObject);
    }
}