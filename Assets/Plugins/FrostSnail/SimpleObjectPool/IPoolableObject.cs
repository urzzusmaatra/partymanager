﻿namespace FrostSnail.SimpleObjectPool
{
    public interface IPoolableObject
    {
        void ResetObject();
        void Destroy();
    }
}