﻿using UnityEngine;

namespace FrostSnail.SimpleObjectPool
{
    using Internal;
    public abstract class ObjectPool : IPoolDepositer
    {
        private const int POOL_SIZE = 64;

        private IPoolableObject[] _deposited;
        private IPoolableObject[] _withdrawn;

        protected ObjectPool()
        {
            _deposited = new IPoolableObject[POOL_SIZE];
            _withdrawn = new IPoolableObject[POOL_SIZE];
        }

        public void Deposit(IPoolableObject poolableObject)
        {
            if (!_withdrawn.TryRemoveObject(poolableObject))
            {
                LogWarning($"{poolableObject} was never withdrawn. Destroying.");
            }

            if (!_deposited.TryAdd(poolableObject))
            {
                LogWarning($"Deposit pool is full. Destroying {poolableObject}.");
                poolableObject.Destroy();
            }
        }

        protected IPoolableObject Withdraw()
        {
            if(!_deposited.TryPopObject(out IPoolableObject poolableObject))
            {
                LogWarning($"Deposit array is empty, creating new one.");
                poolableObject = Create();
            }

            if (_withdrawn.TryAdd(poolableObject))
            {
                LogWarning($"Unable to add {poolableObject} to withdrawl array.");
            }

            poolableObject.ResetObject();
            return poolableObject;
        }

        protected abstract IPoolableObject Create();

        protected void LogWarning(string warningText)
        {
            Debug.LogWarning("<b>HUDModule</b> " + warningText);
        }

    }

}