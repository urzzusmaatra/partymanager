﻿using UnityEngine;

namespace FrostSnail.Broker
{
    [System.Serializable]
    public class SerializedBroker<T>
    {
        [SerializeField]
        private float weight;
        [SerializeField]
        private T obj;

        public float Weight { get => weight; }
        public T Obj { get => obj; }
    }
}