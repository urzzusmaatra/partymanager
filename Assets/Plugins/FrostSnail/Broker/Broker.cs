﻿namespace FrostSnail.Broker
{
    public class Broker<T>
    {
        private float weight;
        private T commodity;

        public Broker(float weight, T commodity)
        {
            this.weight = weight;
            this.commodity = commodity;
        }

        public T Commodity => commodity;
        public float Weight => weight;
    }
}