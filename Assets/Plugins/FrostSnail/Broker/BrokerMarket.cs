﻿using Extensions;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace FrostSnail.Broker
{
    public class BrokerMarket<T> : IList<Broker<T>>
    {
        private List<Broker<T>> _market;
        private float _totalWeight;

        public BrokerMarket()
        {
            _market = new List<Broker<T>>();
            _totalWeight = 0f;
        }

        public void AddCommodity(float weight, T commodity)
        {
            _market.Add(new Broker<T>(weight, commodity));
            _totalWeight += weight;
        }
        public void RemoveCommodity(T commodity)
        {
            for (int i = _market.Count - 1; i >= 0; i--)
            {
                if (_market[i].Commodity.Equals(commodity))
                {
                    _totalWeight -= _market[i].Weight;
                    _market.RemoveAt(i);
                }
            }
        }

        public void RemoveCommodityType<B>() where B : T
        {
            for (int i = _market.Count - 1; i >= 0; i--)
            {
                if (_market[i].Commodity is B)
                {
                    _totalWeight -= _market[i].Weight;
                    _market.RemoveAt(i);
                }
            }
        }

        public T GetCommodity() =>  GetCommodity(Random.value * _totalWeight);

        public T GetCommodity(float weight)
        {
            if (_totalWeight == 0f || this._market.Count == 0)//garbage
            {
                Debug.LogError($"{this}Market is empty!");
                return default(T);
            }

            if (weight >= _totalWeight)
            {
                return this._market.LastMember().Commodity;
            }

            foreach (var broker in _market)
            {
                //this way if weight is over max it will return last making "infinite" calls plausible
                weight -= broker.Weight;
                if (weight <= 0f)
                {
                    return broker.Commodity;
                }
            }

            Debug.LogError($"{this} Market crashed, somehow there was no broker found for weight: {weight}");
            return default(T);//shoudn't happen
        }

        public T GetCommodityWeightNormalized(float normalized) =>  GetCommodity(normalized * _totalWeight);

        public float Weight => this._totalWeight;

        public void RecalculateWeight()
        {
            _totalWeight = 0f;
            foreach (var broker in _market)
            {
                _totalWeight += broker.Weight;
            }
        }

        public Broker<T> this[int index] { get => ((IList<Broker<T>>)_market)[index]; set => ((IList<Broker<T>>)_market)[index] = value; }

        public int Count => _market.Count;

        public bool IsReadOnly => ((IList<Broker<T>>)_market).IsReadOnly;

        public void Add(Broker<T> item)
        {
            _market.Add(item);
            _totalWeight += item.Weight;
        }

        public void Clear()
        {
            _market.Clear();
            _totalWeight = 0f;
        }

        public bool Contains(Broker<T> item)
        {
            return _market.Contains(item);
        }

        public void CopyTo(Broker<T>[] array, int arrayIndex)
        {
            _market.CopyTo(array, arrayIndex);
        }

        public IEnumerator<Broker<T>> GetEnumerator()
        {
            return _market.GetEnumerator();
        }

        public int IndexOf(Broker<T> item)
        {
            return _market.IndexOf(item);
        }

        public void Insert(int index, Broker<T> item)
        {
            _market.Insert(index, item);
            _totalWeight += item.Weight;
        }

        public bool Remove(Broker<T> item)
        {
            if (_market.Remove(item))
            {
                _totalWeight -= item.Weight;
                return true;
            }

            return false;
        }

        public void RemoveAt(int index)
        {
            _totalWeight -= _market[index].Weight;
            _market.RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _market.GetEnumerator();
        }
    }
}