﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FrostSnail.Broker
{
    [System.Serializable]
    public class SerializedBrokerMarket<T>
    {
        [SerializeField]
        private List<SerializedBroker<T>> _brokerMarket = null;

        public BrokerMarket<T> GetBrokerMarket()
        {
            var market = new BrokerMarket<T>();

            foreach (var serializedBroker in this._brokerMarket)
            {
                market.AddCommodity(serializedBroker.Weight, serializedBroker.Obj);
            }

            return market;
        }

        public List<T> CreateListToRandomPoint()
        {
            return CreateListToNormalizedSize(UnityEngine.Random.value);
        }

        public List<T> CreateListToNormalizedSize(float normalizedValue)
        {
            Debug.Log($"{this} is creating list to normalized sized of value {normalizedValue}");
            var list = new List<T>();
            var value = normalizedValue * TotalWeight;
            foreach (var item in this._brokerMarket)
            {
                if (item.Weight > value)
                {
                    break;
                }

                list.Add(item.Obj);
            }

            return list;
        }

        private float TotalWeight
        {
            get
            {
                var totalWeight = 0f;
                foreach (var item in _brokerMarket)
                {
                    totalWeight += item.Weight;
                }
                return totalWeight;
            }
        }

        public T GetWeighted(float weight)
        {
            foreach (var item in _brokerMarket)
            {
                weight -= item.Weight;
                if(weight <= 0)
                {
                    return item.Obj;
                }
            }

            return _brokerMarket[_brokerMarket.Count - 1].Obj;
        }

        public T GetWeightNormalized(float normalizedWeight) => GetWeighted(TotalWeight * normalizedWeight);
    }
}