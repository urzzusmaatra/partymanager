using System;
using UnityEngine;

namespace FrostSnail.Coverage
{
    [System.Serializable]
    public struct CoverageArea
    {
        [SerializeField]
        private float baseMin;
        [SerializeField]
        private float baseMax;
        [SerializeField]
        private float currentMin;
        [SerializeField]
        private float currentMax;

        public CoverageArea(float baseMin, float baseMax, float currentMin, float currentMax)
        {
            this.baseMin = baseMin;
            this.baseMax = baseMax;
            this.currentMin = currentMin;
            this.currentMax = currentMax;
        }

        public static CoverageArea Zero => new CoverageArea(0f, 0f, 0f, 0f);
        public static CoverageArea FullCoverage(float minLimit, float maxLimit) => new CoverageArea(minLimit, maxLimit, minLimit, maxLimit);

        public float BaseMin
        {
            get => baseMin;
            set
            {
                baseMin = value;
                ReconcileValues();
            }
        }

        public bool SqrContains(float sqrMagnitude) => (currentMin * currentMin) <= sqrMagnitude && sqrMagnitude <= (currentMax * currentMax);

        public bool Contains(float value) => currentMin <= value && value <= currentMax;

        public float BaseMax
        {
            get => baseMax;
            set
            {
                baseMax = value;
                ReconcileValues();
            }
        }

        private void ReconcileValues()
        {
            if (currentMin < baseMin)
            {
                currentMin = baseMin;
            }
            else if (currentMin > baseMax)
            {
                currentMin = baseMax;
            }

            if (currentMax < baseMin)
            {
                currentMax = baseMin;
            }
            else if (currentMax > baseMax)
            {
                currentMax = baseMax;
            }

            if (currentMin > currentMax)
            {
                currentMin = currentMax;
            }
        }

        public float CurrentMin
        {
            get => currentMin;
            set
            {
                TrySetCurrent(ref currentMin, value);
                currentMax = (currentMax < currentMin) ? currentMin : currentMax;
            }
        }

        public float CurrentMax
        {
            get => currentMax;
            set
            {
                TrySetCurrent(ref currentMax, value);
                currentMin = (currentMax < currentMin) ? currentMax : currentMin;
            }
        }

        private void TrySetCurrent(ref float current, float requested)
        {
            if (requested > baseMin && requested < baseMax)
            {
                current = requested;
            }
        }

        public bool IsInArea(float range) => range >= currentMin && range <= currentMax;

        public float MinRatio => currentMin / (baseMax - baseMin);
        public float MaxRatio => currentMax / (baseMax - baseMin);

        public override string ToString()
        {
            return $"Coverage <b>{baseMin}</b>-><b>{currentMin}</b> && <b>{currentMax}</b><-<b>{baseMax}</b>";
        }

    }
}