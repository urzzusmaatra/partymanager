﻿using System;
using UnityEngine;
namespace FrostSnail.Events.Internal
{
    public class EventChannelComponent : MonoBehaviour, IEventChannel
    {
        private Action<EventData> listeners;
        public void RegisterMethod(Action<EventData> callback)
        {
            listeners += callback;
        }

        public void UnregisterMethod(Action<EventData> callback)
        {
            listeners -= callback;
        }

        public void SendEvent<T>(T data) where T : EventData
        {
            SendEvent(data, transform);
        }

        public void SendEvent<T>(T data, Transform sender) where T : EventData
        {
            data.Sender = sender;
            listeners?.Invoke(data);
            EventFactory.ReturnEvent(data);
        }

        private Action<EventChannelComponent> OnObjectDestoryed;

        public void RegisterDestructionCallback(Action<EventChannelComponent> destructionCallback)
        {
            OnObjectDestoryed += destructionCallback;
        }

        public void UnregisterDestructionCallback(Action<EventChannelComponent> destructionCallback)
        {
            OnObjectDestoryed -= destructionCallback;
        }

        private void OnDestroy()
        {
            OnObjectDestoryed?.Invoke(this);
            OnObjectDestoryed = null;
        }
    }
}