﻿using System;
using UnityEngine;

namespace FrostSnail.Events.Internal
{
    public class EventHierarchyChannelComponent : MonoBehaviour, IEventChannel
    {
        enum HierechyDirection
        {
            Up,
            Down
        }

        [SerializeField]
        private HierechyDirection searchDirection = HierechyDirection.Up;
        private IEventChannel eventChannel;
        private IEventChannel EventChannel
        {
            get
            {
                if (eventChannel != null)
                    return eventChannel;

                switch (searchDirection)
                {
                    case HierechyDirection.Up:
                        if (transform.parent != null)
                        {
                            eventChannel = transform.parent.GetComponentInParent<IEventChannel>();
                        }
                        break;
                    case HierechyDirection.Down:
                        foreach (Transform child in transform)
                        {
                            eventChannel = child.GetComponentInChildren<IEventChannel>();
                            if (eventChannel != null)
                                break;
                        }
                        break;
                    default:
                        Debug.LogError($"Unsupported search direction {searchDirection}", transform);
                        break;
                }

                if(eventChannel == null)
                    Debug.LogError($"Event channel not found in {searchDirection} hierarchy!", transform);

                return eventChannel;
            }
        }

        public void RegisterDestructionCallback(Action<EventChannelComponent> destructionCallback)
        {
            EventChannel.RegisterDestructionCallback(destructionCallback);
        }

        public void RegisterMethod(Action<EventData> callback)
        {
            EventChannel.RegisterMethod(callback);
        }

        public void SendEvent<T>(T data) where T : EventData
        {
            EventChannel.SendEvent(data);
        }

        public void SendEvent<T>(T data, Transform sender) where T : EventData
        {
            EventChannel.SendEvent(data, sender);
        }

        public void UnregisterDestructionCallback(Action<EventChannelComponent> destructionCallback)
        {
            EventChannel.UnregisterDestructionCallback(destructionCallback);
        }

        public void UnregisterMethod(Action<EventData> callback)
        {
            EventChannel.UnregisterMethod(callback);
        }
    }
}