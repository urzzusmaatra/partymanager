using System;
using UnityEngine;

namespace FrostSnail.Events.Internal
{
    public class EventProxyChannelComponent : MonoBehaviour, IEventChannel
    {
        [SerializeField]
        private EventChannelComponent eventComponent;

        public void RegisterDestructionCallback(Action<EventChannelComponent> destructionCallback)
        {
            ((IEventChannel)eventComponent).RegisterDestructionCallback(destructionCallback);
        }

        public void RegisterMethod(Action<EventData> callback)
        {
            ((IEventChannel)eventComponent).RegisterMethod(callback);
        }

        public void SendEvent<T>(T data) where T : EventData
        {
            ((IEventChannel)eventComponent).SendEvent(data);
        }

        public void SendEvent<T>(T data, Transform sender) where T : EventData
        {
            ((IEventChannel)eventComponent).SendEvent(data, sender);
        }

        public void UnregisterDestructionCallback(Action<EventChannelComponent> destructionCallback)
        {
            ((IEventChannel)eventComponent).UnregisterDestructionCallback(destructionCallback);
        }

        public void UnregisterMethod(Action<EventData> callback)
        {
            ((IEventChannel)eventComponent).UnregisterMethod(callback);
        }
    }
}