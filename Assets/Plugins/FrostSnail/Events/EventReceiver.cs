﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace FrostSnail.Events
{

    public class EventReceiver
    {
        private HashSet<IEventChannel> eventComponents;
        private List<ActionPack> actionPacks;

        public EventReceiver()
        {
            eventComponents = new HashSet<IEventChannel>();
            actionPacks = new List<ActionPack>();
        }

        public EventReceiver(IEventChannel component) : this()
        {
            RegisterComponent(component);
        }

        public void RegisterComponent(IEventChannel component)
        {
            if (eventComponents.Add(component))
            {
                component.RegisterMethod(EventCallback);
            }
        }

        public bool TryRegisterObject(GameObject candidate)
        {
            if(candidate.TryGetComponent(out IEventChannel eventComponent))
            {
                RegisterComponent(eventComponent);
                return true;
            }

            return false;
        }

        public void UnregisterComponent(IEventChannel component)
        {
            if (eventComponents.Remove(component))
            {
                component.UnregisterMethod(EventCallback);
                component.UnregisterDestructionCallback(ObjectDestroyed);
            }
        }

        public void UnregisterAll()
        {
            foreach (var component in eventComponents)
            {
                component.UnregisterMethod(EventCallback);
                component.UnregisterDestructionCallback(ObjectDestroyed);
            }
        }

        public bool TryUnregisterObject(GameObject candidate)
        {
            if (candidate.TryGetComponent(out IEventChannel eventComponent))
            {
                UnregisterComponent(eventComponent);
                return true;
            }

            return false;
        }

        private void EventCallback(EventData data)
        {
            foreach (var actionPack in actionPacks)
            {
                if(actionPack.GetActionType == data.GetType())
                {
                    actionPack.EventCallback(data);
                    return;
                }
            }
        }

        public void RegisterMethod<T>(Action<T> method) where T : EventData
        {
            foreach (var actionPack in actionPacks)
            {
                if(actionPack.GetActionType == typeof(T))
                {
                    actionPack.RegisterAction(method);
                    return;
                }
            }
            var newActionPack = new ActionPack<T>();
            newActionPack.RegisterAction(method);
            actionPacks.Add(newActionPack);
        }

        public void UnregisterMethod<T>(Action<T> method) where T : EventData
        {
            foreach (var actionPack in actionPacks)
            {
                if (actionPack.GetActionType == typeof(T))
                {
                    actionPack.UnregisterAction(method);
                    break;
                }
            }
        }

        private void ObjectDestroyed(IEventChannel eventComponent)
        {
            UnregisterComponent(eventComponent);
        }

        private abstract class ActionPack
        {
            public abstract bool IsOfType<T>() where T : EventData;
            public abstract void RegisterAction<T>(Action<T> action) where T : EventData;
            public abstract void UnregisterAction<T>(Action<T> action) where T : EventData;

            public abstract Type GetActionType { get; }

            public abstract void EventCallback(EventData data);
        }

        private class ActionPack<T> : ActionPack where T : EventData
        {
            private Action<T> actions;
            public override Type GetActionType => typeof(T);

            public override bool IsOfType<B>()
            {
                return typeof(B) == typeof(T);
            }

            public override void RegisterAction<T1>(Action<T1> action)
            {
                if (typeof(T) == typeof(T1))
                {
                    actions += action as Action<T>;
                }
                else
                {
                    throw new Exception($"Action type missmatch. Required {typeof(T)}, got {typeof(T1)}.");
                }
            }

            public override void UnregisterAction<T1>(Action<T1> action)
            {
                if (typeof(T) == typeof(T1))
                {
                    actions -= action as Action<T>;
                }
                else
                {
                    throw new Exception($"Action type missmatch. Required {typeof(T)}, got {typeof(T1)}.");
                }
            }

            public override void EventCallback(EventData data)
            {
                actions?.Invoke(data as T);
            }
        }

    }

}
