﻿using UnityEngine;
using System;

namespace FrostSnail.Events
{
    public abstract class EventData
    {
        public Transform Sender { get; set; }

        public bool TrySendTo(Transform obj) => TrySendTo(obj.gameObject);

        public bool TrySendTo(GameObject obj)
        {
            if(obj.TryGetComponent(out IEventChannel eventComponent))
            {
                SendTo(eventComponent);
                return true;
            }
            Debug.LogError($"Object {obj} doesn't have EventComponent");
            return false;
        }

        public void SendTo(IEventChannel eventComponent) => eventComponent.SendEvent(this);

        public void SendTo(IEventChannel eventComponent, Transform sender) => eventComponent.SendEvent(this, sender);
    }
}