﻿using FrostSnail.Events.Internal;
using System;
using UnityEngine;

namespace FrostSnail.Events
{
    public interface IEventChannel
    {
        void RegisterDestructionCallback(Action<EventChannelComponent> destructionCallback);
        void RegisterMethod(Action<EventData> callback);
        void SendEvent<T>(T data) where T : EventData;
        void SendEvent<T>(T data, Transform sender) where T : EventData;
        void UnregisterDestructionCallback(Action<EventChannelComponent> destructionCallback);
        void UnregisterMethod(Action<EventData> callback);
    }
}