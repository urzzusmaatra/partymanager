﻿using System.Collections.Generic;

namespace FrostSnail.Events
{
    public static class EventFactory
    {
        private static Dictionary<int, List<EventData>> eventPool = new Dictionary<int, List<EventData>>();

        public static void GetEvent<T>(out T eventObject ) where T : EventData, new()
        {
            eventObject = GetEvent<T>();
        }

        public static T GetEvent<T>() where T : EventData, new()
        {
            var hash = typeof(T).GetHashCode();
            if(eventPool.TryGetValue(hash, out List<EventData> events))
            {
                if (events.Count > 0)
                {
                    return events.PopLast<EventData>() as T;
                }
            }

            return new T();
            
        }

        public static void ReturnEvent<T>(T eventObject) where T : EventData
        {
            var hash = typeof(T).GetHashCode();
            if (eventPool.TryGetValue(hash, out List<EventData> events))
            {
                events.Add(eventObject);
            }
            else
            {
                var newPool = new List<EventData>();
                newPool.Add(eventObject);
                eventPool[hash] = newPool;

            }
        }

        private static T PopLast<T>(this List<T> list)
        {
            var lastIndex = list.Count - 1;
            T member = list[lastIndex];
            list.RemoveAt(lastIndex);
            return member;
        }
    }
}