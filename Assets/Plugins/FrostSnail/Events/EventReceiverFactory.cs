﻿namespace FrostSnail.Events
{
    public static class EventReceiverFactory
    {
        public static EventReceiver CreateEventReceiver()
        {
            return new EventReceiver();
        }

        public static EventReceiver CreateEventReceiver(IEventChannel objectTolisten)
        {
            if (objectTolisten == null)
                throw new System.Exception("Empty receiver provided. Please investigate!");

            return new EventReceiver(objectTolisten);
        }
        public static bool CreateEventHandler(out EventReceiver eventHandler, UnityEngine.GameObject objectTolisten)
        {
            eventHandler = null;
            if (objectTolisten.TryGetComponent(out IEventChannel eventComponent))
            {
                eventHandler = CreateEventReceiver(eventComponent);
                return true;
            }
            return false;
        }

        public static bool CreateEventHandler(out EventReceiver eventHandler, params UnityEngine.GameObject[] objectsTolisten)
        {
            eventHandler = null;
            foreach (var objectTolisten in objectsTolisten)
            {
                if (objectTolisten.TryGetComponent(out IEventChannel eventComponent))
                {
                    if (eventHandler == null)
                    {
                        eventHandler = new EventReceiver(eventComponent);
                    }
                    else
                    {
                        eventHandler.RegisterComponent(eventComponent);
                    }
                }
            }
            return eventHandler != null;
        }
    }

}