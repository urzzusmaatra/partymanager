﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FrostSnail.InputResolving.IO
{
    [System.Serializable]
    public class SerializableInputControllerDataObject
    {
        public string controllerId;
        public List<SerializableInputContextDataObject> serializableContextCollection;

        public SerializableInputControllerDataObject()
        {
            controllerId = string.Empty;
            serializableContextCollection = new List<SerializableInputContextDataObject>();
        }
        public SerializableInputControllerDataObject(string controllerId, ReadOnlyCollection<InputContext> contexts)
        {
            this.controllerId = controllerId;
            serializableContextCollection = new List<SerializableInputContextDataObject>();
            foreach (var context in contexts)
            {
                serializableContextCollection.Add(new SerializableInputContextDataObject(context));
            }
        }

    }
}