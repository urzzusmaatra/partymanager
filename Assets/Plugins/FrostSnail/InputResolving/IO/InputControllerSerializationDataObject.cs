﻿using UnityEngine;
using FrostSnail.Core.DataUtility;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace FrostSnail.InputResolving.IO
{
    public static class InputControllerSerializationDataObject
    {
        private static Dictionary<string, DataIO> dataHandlers = new Dictionary<string, DataIO>();
        private static ISerializationProvider serializer = new DataSerializer();
        public static void SaveData(string controllerId, ReadOnlyCollection<InputContext> contexts)
        {
            DataIO dataHandler;
            if (!dataHandlers.TryGetValue(controllerId, out dataHandler))
            {
                dataHandler = new DataIO(Application.persistentDataPath, controllerId, DataIO.EXTENSION_JSON, serializer);
            }

            var data = new SerializableInputControllerDataObject(controllerId, contexts);
            dataHandler.SaveData(data);
        }

        public static bool LoadData(string controllerId, out SerializableInputControllerDataObject serializedControlls)
        {
            DataIO dataHandler;
            if (!dataHandlers.TryGetValue(controllerId, out dataHandler))
            {
                dataHandler = new DataIO(Application.persistentDataPath, controllerId, DataIO.EXTENSION_JSON, serializer);
            }

            serializedControlls = new SerializableInputControllerDataObject();
            return dataHandler.LoadData(ref serializedControlls) == DataIO.Result.Success;
        }

        private class DataSerializer : ISerializationProvider
        {
            public void OverwriteObject<T>(string data, ref T obj)
            {
                JsonUtility.FromJsonOverwrite(data, obj);
            }

            public string SerializeObject<T>(T obj)
            {
                return JsonUtility.ToJson(obj);
            }
        }
    }

}