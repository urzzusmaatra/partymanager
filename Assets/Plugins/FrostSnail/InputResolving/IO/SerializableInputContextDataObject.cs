﻿using System.Collections.Generic;

namespace FrostSnail.InputResolving.IO
{
    [System.Serializable]
    public class SerializableInputContextDataObject
    {
        public string contextId;
        public List<InputDefinition> inputList;

        public SerializableInputContextDataObject(InputContext inputContext)
        {
            contextId = inputContext.contextId;
            inputList = new List<InputDefinition>();
            foreach (var inputDefinition in inputContext.GetInputDefinitions())
            {
                inputList.Add(inputDefinition);
            }
        }

    }
}