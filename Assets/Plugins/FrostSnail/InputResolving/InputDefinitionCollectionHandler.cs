﻿using FrostSnail.InputResolving.Internal;//remove this from service
using System;
using System.Collections.ObjectModel;

namespace FrostSnail.InputResolving
{
    public class InputDefinitionCollectionHandler
    {
        /// <summary>
        /// This is how external objects can get all input info and request rebinding
        /// </summary>
        private readonly InputContext context;
        private readonly InputContextController controller;
        public ReadOnlyCollection<InputBeautified> InputCollection { get; private set; }

        public InputDefinitionCollectionHandler(InputContext context, InputContextController controller)
        {
            this.context = context;
            this.controller = controller;
        }

        public void RefreshCollection()
        {
            InputCollection = context.GetBeautifiedInput().AsReadOnly();
            OnCollectionRefreshed?.Invoke();
        }

        public bool RequestRebind(int inputDefinitionId)
        {
            return controller.RequestRebind(inputDefinitionId, context);
        }

        public bool IsContextId(in string contextId) => this.context.contextId == contextId;

        public bool IsRebinding => controller.IsRebinding;

        public Action OnCollectionRefreshed;

    }

}
