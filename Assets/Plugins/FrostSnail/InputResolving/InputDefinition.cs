﻿using UnityEngine;

namespace FrostSnail.InputResolving
{
    [System.Serializable]
    public class InputDefinition
    {
        //public access variables are here for easy sertialising

        public string name;

        public int id;

        public KeyCode key;

        public void ChangeKeyCode(KeyCode newKeyCode)
        {
            Debug.Log("Changing input: " + name + " from: " + key.ToString() + " to: " + newKeyCode.ToString());
            this.key = newKeyCode;
        }

        public InputDefinition(string name, KeyCode key)
        {
            this.name = name;
            this.id = Animator.StringToHash(name);//this should be safe enough
            this.key = key;
        }
    }

}
