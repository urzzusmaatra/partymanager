﻿using FrostSnail.InputResolving.Internal;//remove this from service
using UnityEngine;

namespace FrostSnail.InputResolving
{
    //maybe to create input rebind object that will hold all rebin info (just wrapping input controller into rebind friendly way) and hide controllers

    public class InputContextControllerAccessor
    {
        ///wrap InputContextCollectionController
        ///allow creation of input, getting rebind handlers and getting input accessors 

        private InputDefinitionCollectionHandler inputRebindHandler;
        private InputContextController controller;

        public bool IsLoaded { get; private set; }
        public InputContextControllerAccessor(string inputCollectionId)
        {
            controller = new InputContextController(inputCollectionId);
            IsLoaded = controller.TryLoadData();
        }

        public void AddInputToContext(string contextId, string inputName, KeyCode key)
        {
            var inputContext = controller.GetCreateInputContext(contextId);
            inputContext.TryAddInputDefinition(new InputDefinition(inputName, key));

        }

        public bool CheckId(in string id)
        {
            return controller.collectionId == id;
        }

        public void SaveData() => this.controller.SaveData();

        public InputContextAccessor GetAccessor(in string contextId) => this.controller.GetAccessor(contextId);

        public InputDefinitionCollectionHandler GetDefinitionHandler(in string contextId)
        {
            foreach (var handler in this.controller.GetRebindHandlers())
            {
                if(handler.IsContextId(contextId))
                {
                    return handler;
                }
            }

            return null;
        }
    }
}
