﻿using UnityEngine.UI;

namespace FrostSnail.InputResolving
{
    public struct InputBeautified
    {
        public readonly string name;
        public readonly string key;
        public readonly Image icon;//use this for controlelr or anything thats is setup with a picture
        public readonly int id;
        public InputBeautified(string name, string key, Image icon, int id)
        {
            this.name = name;
            this.key = key;
            this.icon = icon;
            this.id = id;
        }

        public static InputBeautified CreateText(string name, string key, int id)
        {
            return new InputBeautified(name, key, null, id);
        }
        //analyse and enable when ready for icons
        //public static InputBeautified CrateIcon(string name, Image icon, int id)
        //{
        //    return new InputBeautified(name, string.Empty, icon, id);
        //}
    }

}
