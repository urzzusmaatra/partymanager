﻿using UnityEngine;

namespace FrostSnail.InputResolving
{
    public class InputAccessor
    {
        //access single input variation
        private InputDefinition inputDefinition;

        public InputAccessor(InputDefinition inputDefinition)
        {
            this.inputDefinition = inputDefinition;
        }

        public KeyCode GetKey
        {
            get => inputDefinition.key;
        }

        public bool IsKeyDown => Input.GetKeyDown(GetKey);
        public bool IsKey => Input.GetKey(GetKey);
        public bool IsKeyUp => Input.GetKeyUp(GetKey);
    }

}
