﻿namespace FrostSnail.InputResolving.Internal
{
    public class RebindData
    {
        public readonly int keyId;
        public readonly InputContext context;

        public RebindData(int keyId, InputContext context)
        {
            this.keyId = keyId;
            this.context = context;
        }
    }
}