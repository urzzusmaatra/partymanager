﻿using UnityEngine;

namespace FrostSnail.InputResolving.Internal
{
    public class InputReadObject : MonoBehaviour
    {
        //just reads input in update for rebind purposes
        //should contain banned input read? or maybe other objects should use controll that?
        public delegate void KeyPressed(KeyCode keyCodeUsed);
        public KeyPressed KeyPressedEvent;

        private KeyCode[] _allKeyCodes;
        private int cachedAmmount = 0;
        private void Awake()
        {
            //caching everything to avoid casting everything again
            var keysList = System.Enum.GetValues(typeof(KeyCode));
            _allKeyCodes = new KeyCode[keysList.Length];
            foreach (KeyCode key in keysList)
            {
                _allKeyCodes[cachedAmmount] = key;
                cachedAmmount++;
            }
        }

        private void Start()
        {
            this.gameObject.SetActive(false);
        }

        private void Update()
        {
            for (int i = 0; i < cachedAmmount; i++)
            {
                var keyCode = _allKeyCodes[i];
                if (UnityEngine.Input.GetKeyDown(keyCode))
                {
                    KeyPressedEvent?.Invoke(keyCode);
                    this.gameObject.SetActive(false);
                    break;//i think there is no way to 
                }
            }    
        }

        internal void TryDestroy()
        {
            //if noone listens to it destroy the object, new one will be created when needed
            if(KeyPressedEvent == null)
            {
                Instance = null;
                Destroy(this);
            }
        }

        internal void StartListening()
        {
            this.gameObject.SetActive(true);
        }

        //creating this object a singltone
        private static InputReadObject inputReadObject;
        public static InputReadObject Instance
        {
            get
            {
                if(inputReadObject == null)
                {
                    inputReadObject = new GameObject("InputReadObject", typeof(InputReadObject)).GetComponent<InputReadObject>();
                }

                return inputReadObject;
            }
            private set { inputReadObject = value; }
        }
    }
}