﻿using UnityEngine;
using System.Collections.Generic;
using FrostSnail.InputResolving.IO;
using System.Linq;

namespace FrostSnail.InputResolving.Internal
{
    public class InputContextController
    {
        public readonly string collectionId;//player1 for example
        private List<InputContext> contextCollection;
        private List<InputDefinitionCollectionHandler> inputContextRebindHandlersCollection;
        private RebindData rebindData;
        public InputContextController(string collectionId)
        {
            this.collectionId = collectionId;
            InputReadObject.Instance.KeyPressedEvent += KeyPressedCallback;
            this.contextCollection = new List<InputContext>();
        }

        /// <summary>
        /// Destructor is used to make shure the InputReadObject didn't survive he deletion of all Controllers
        /// </summary>
        ~InputContextController()
        {
            InputReadObject.Instance.KeyPressedEvent -= KeyPressedCallback;
            InputReadObject.Instance.TryDestroy();
        }

        public InputContextAccessor GetAccessor(in string contextId)
        {
            foreach (var context in contextCollection)
            {
                if (context.contextId == contextId)
                {
                    return new InputContextAccessor(context);
                }
            }

            return null;//error maybe?
        }

        public bool AddInputContext(InputContext inputContext)
        {
            foreach (var context in contextCollection)
            {
                if (context.contextId == inputContext.contextId)
                {
                    Debug.LogError(string.Format("Trying to add input context with id: {0} that allready exits!", inputContext.contextId));
                    return false;
                }
            }

            this.contextCollection.Add(inputContext);
            return true;
        }

        public InputContext GetCreateInputContext(string contextId)
        {
            return GetCreateInputContext(contextId, null);
        }

        public InputContext GetCreateInputContext(string contextId, List<InputDefinition> inputs)
        {
            if (this.contextCollection != null)
            {
                var context = this.contextCollection.Find(x => x.contextId == contextId);

                if (context != null)
                {
                    if (inputs != null)
                    {
                        foreach (var input in inputs)
                        {
                            context.TryAddInputDefinition(input);
                        }
                    }
                    return context;
                }
            }
            var inputContext = inputs == null ? new InputContext(contextId) : new InputContext(contextId, inputs);
            AddInputContext(inputContext);
            return inputContext;
        }

        private void KeyPressedCallback(KeyCode keyUsed)
        {
            //implement rebinding logic here
            this.rebindData.context.ChangeInputDefinition(this.rebindData.keyId, keyUsed);
            SaveData();
            foreach (var inputContextRebinder in this.inputContextRebindHandlersCollection)
            {
                inputContextRebinder.RefreshCollection();
            }
        }

        public bool RequestRebind(int rebindInputDefinitionId, InputContext context)
        {
            if (IsRebinding)
            {
                return false;
            }

            this.rebindData = new RebindData(rebindInputDefinitionId, context);
            IsRebinding = true;
            InputReadObject.Instance.StartListening();
            return true;
        }

        public bool IsRebinding { get; private set; }
        /// <summary>
        /// Get all rebind handlers from this controller. Creates a copy for your leasure.
        /// </summary>
        /// <returns></returns>
        public List<InputDefinitionCollectionHandler> GetRebindHandlers()
        {
            if (inputContextRebindHandlersCollection == null)
            {
                InitializeRebinders();
            }

            return this.inputContextRebindHandlersCollection;
        }

        private void InitializeRebinders()
        {
            this.inputContextRebindHandlersCollection = new List<InputDefinitionCollectionHandler>(this.contextCollection.Count);
            //initialize rebinders
            foreach (var context in contextCollection)
            {
                inputContextRebindHandlersCollection.Add(new InputDefinitionCollectionHandler(context, this));
            }
        }

        public bool TryLoadData()
        {
            if (InputControllerSerializationDataObject.LoadData(collectionId, out SerializableInputControllerDataObject serializableInput))
            {
                foreach (var serializableInputContext in serializableInput.serializableContextCollection)
                {
                    AddInputContext(new InputContext(serializableInputContext.contextId, serializableInputContext.inputList.ToList<InputDefinition>()));
                }

                return true;
            }
            Debug.Log("Failed to load data named: " + collectionId);
            return false;
        }

        public void SaveData()
        {
            InputControllerSerializationDataObject.SaveData(collectionId, contextCollection.AsReadOnly());
        }
    }
}