﻿using UnityEngine;

namespace FrostSnail.InputResolving
{
    public class InputContextAccessor
    {
        ///this is how external systems get their input checked, via this accessor
        ///i.e. object that needs to check input should load this object (maybe create monobehavior that will be loaded automagicly with this, and maybe oprimise process a bit?

        private InputContext inputContext;

        public InputContextAccessor(InputContext inputContext)
        {
            this.inputContext = inputContext;
        }

        public int GetInputId(string name)
        {
            return inputContext.GetInputId(name);
        }

        public KeyCode GetKeyCode(int id)
        {
            return inputContext.GetKeyCode(id);
        }
        public InputAccessor GetAccessor(string name)
        {
            return GetAccessor(GetInputId(name));
        }
        public InputAccessor GetAccessor(int id)
        {
            return new InputAccessor(inputContext.GetInputDefinition(id));
        }
    }

}
