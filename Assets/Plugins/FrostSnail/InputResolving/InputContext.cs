﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace FrostSnail.InputResolving
{
    public class InputContext
    {
        ///contains input definitionas for one specific input context
        ///input context can be UI, InGame, InMenu, Ship, Car, etc.
        ///each context can share same keys, this is why the context is different
        ///this may not be 

        internal readonly string contextId;
        private List<InputDefinition> inputDefinitionCollection;
        private List<InputBeautified> beautifiedInput;

        public InputContext(string contextId) : this(contextId, new List<InputDefinition>()) { }

        public InputContext(string contextId, List<InputDefinition> inputDefinition)
        {
            this.contextId = contextId;
            this.inputDefinitionCollection = inputDefinition;
            this.beautifiedInput = new List<InputBeautified>();
        }

        internal int GetInputId(string name)
        {
            foreach (var inputDefinition in this.inputDefinitionCollection)
            {
                if (inputDefinition.name == name)
                {
                    return inputDefinition.id;
                }
            }

            throw new System.Exception("Input with name " + name + " doesn't exist in input context " + contextId);
        }

        internal KeyCode GetKeyCode(int id)
        {
            foreach (var inputDefinition in this.inputDefinitionCollection)
            {
                if(inputDefinition.id == id)
                {
                    return inputDefinition.key;
                }
            }

            throw new System.Exception("Input with id " + id + " doesn't exist in input context " + contextId);
        }

        internal bool TryAddInputDefinition(InputDefinition inputDefinition, bool doCheckup = true)
        {
            if(doCheckup)
            {
                if(!IsDefinitionUnique(inputDefinition))
                {
                    Debug.LogWarning(string.Format("Input definition with key: {0} name: {1} id: {2} is not unique",inputDefinition.key, inputDefinition.name, inputDefinition.id));
                    return false;
                }
            }

            AddInputDefinition(inputDefinition);
            return true;
        }

        private bool IsDefinitionUnique(InputDefinition inputDefinition)
        {
            foreach (var definition in this.inputDefinitionCollection)
            {
                if(definition.id == inputDefinition.id || definition.key == inputDefinition.key || definition.name == inputDefinition.name)
                {
                    return false;
                }
            }

            return true;
        }

        private void AddInputDefinition(InputDefinition inputDefinition)
        {
            this.inputDefinitionCollection.Add(inputDefinition);
            this.beautifiedInput.Add(InputBeautified.CreateText(inputDefinition.name, inputDefinition.key.ToString(), inputDefinition.id));
        }

        internal List<InputBeautified> GetBeautifiedInput()
        {
            return beautifiedInput;
        }

        internal void ChangeInputDefinition(int keyId, KeyCode newKeyCode)
        {
            foreach (var definition in this.inputDefinitionCollection)
            {
                if(definition.id == keyId)
                {
                    definition.ChangeKeyCode(newKeyCode);
                    //remember, this is not saved yet
                    break;
                }
            }

            for (int i = 0; i < this.beautifiedInput.Count; i++)
            {
                var input = this.beautifiedInput[i];
                if (input.id == keyId)
                {
                    input = InputBeautified.CreateText(input.name, newKeyCode.ToString(), keyId);
                }
            }
        }

        internal InputDefinition GetInputDefinition(int id)
        {
            return inputDefinitionCollection.Find(x => x.id == id);
        }

        internal ReadOnlyCollection<InputDefinition> GetInputDefinitions()
        {
            return this.inputDefinitionCollection.AsReadOnly();
        }
    }
}
