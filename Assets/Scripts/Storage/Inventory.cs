﻿using System.Collections.Generic;
using UnityEngine;
using Utility.Logging;

namespace CubeEngine.Storage
{
    public class Inventory : IInventory
    {
        private IInventoryOwner _owner;
        private Dictionary<int, IStorage> _inventory;
        private float _chachedWeight;
        private bool _setWeightDirty;

        public Inventory(IInventoryOwner owner)
        {
            this._owner = owner;
            this._inventory = new Dictionary<int, IStorage>();
        }

        /// <summary>
        /// Gets current inventory weigth with all storages in it. Weight is cached but when inventory changes it will be recalculated.
        /// </summary>
        public float CurrentWeight
        {
            get
            {
                if (!this._setWeightDirty)
                {
                    return this._chachedWeight;
                }

                this._setWeightDirty = false;
                this._chachedWeight = 0f;
                foreach (var storage in this._inventory.Values)
                {
                    this._chachedWeight += storage.TotalWeight;
                }

                return this._chachedWeight;
            }

        }

        /// <summary>
        /// Create storage of an item type
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool TryCreateStorageOf(IStoragable item, int count = 0)
        {
            if (_inventory.ContainsKey(item.Id))
            {
                return false;
            }

            _inventory.Add(item.Id, new Storage(item, count));
            return true;
        }

        /// <summary>
        /// Gets storage for item type.
        /// If doesn't exist, creates storage for new item type.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="storage"></param>
        public void GetOrCreate(IStoragable item, out IStorage storage)
        {
            if (!this._inventory.TryGetValue(item.Id, out storage))
            {
                storage = new Storage(item, 0);
                this._inventory.Add(item.Id, storage);
            }
        }

        /// <summary>
        /// Adds item to the storage with requested quantity.
        /// </summary>
        /// <param name="storagable"></param>
        /// <param name="quantity"></param>
        /// <returns>False if current quantity exceeds total weigth possible. It doesn't add any items then.</returns>
        public bool TryAddItem(IStoragable storagable, int quantity)
        {
            //currently we deny in bulk
            if (this.CurrentWeight + (quantity * storagable.Weight) > this._owner.GetMaxWeight)
            {
                //unable to add that much stuff to storage
                return false;
            }

            GetOrCreate(storagable, out IStorage storage);
            storage.AddQuantity(quantity);
            this._setWeightDirty = true;
            return true;

        }

        /// <summary>
        /// Tries to remove items from storage by requested quantity. It will remove betweene 0 and qunatity depending on the qurrent quantity.
        /// </summary>
        /// <param name="storagable"></param>
        /// <param name="quantity"></param>
        /// <returns>Between 0 and reqwuested quantity</returns>
        public int TryTakeItem(IStoragable storagable, int quantity)
        {
            if (!this._inventory.TryGetValue(storagable.Id, out IStorage storage))
            {
                Log.LogBaseError($"Storagable item {storagable} doesn't exist in this inventory {this} context!");
                return 0;
            }
            this._setWeightDirty = true;
            return storage.TryGetQuantity(quantity);
        }
    }
}