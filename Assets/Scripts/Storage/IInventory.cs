﻿namespace CubeEngine.Storage
{
    public interface IInventory
    {
        float CurrentWeight { get; }

        void GetOrCreate(IStoragable item, out IStorage storage);
        bool TryAddItem(IStoragable storagable, int quantity);
        bool TryCreateStorageOf(IStoragable item, int count = 0);
        int TryTakeItem(IStoragable storagable, int quantity);
    }
}