﻿//using CubeEngine.Core;

namespace CubeEngine.Storage
{

    /// <summary>
    /// Container for specific item
    /// </summary>
    public class Storage : IStorage
    {
        /// <summary>
        /// Once set for one type of item there is realy no reason to change it
        /// </summary>
        private readonly IStoragable _item;
        public IStoragable Item => this._item;

        public int Quantity { get; private set; }

        public float TotalWeight => this.Item.Weight * this.Quantity;
        /// <summary>
        /// Create
        /// </summary>
        /// <param name="item"></param>
        /// <param name="quantity"></param>
        public Storage(IStoragable item, int quantity = 0)
        {
            this._item = item;
            this.Quantity = quantity;
        }

        /// <summary>
        /// Is this storage empty (non of items left)
        /// </summary>
        public bool IsEmpty => !(this.Quantity > 0);

        /// <summary>
        /// Adds requested quantity
        /// </summary>
        /// <param name="quantity"></param>
        public void AddQuantity(int quantity)
        {
            //until we introduce stack sizes?
            this.Quantity += quantity;
        }

        /// <summary>
        /// Gets qunantity between 0 and requested depending on current storage quantity
        /// </summary>
        /// <param name="quantity"></param>
        /// <returns></returns>
        public int TryGetQuantity(int quantity)
        {
            if(this.Quantity < quantity)
            {
                quantity = this.Quantity;
                this.Quantity = 0;
                return quantity;
            }
            this.Quantity -= quantity;
            return quantity;
        }
    }
}