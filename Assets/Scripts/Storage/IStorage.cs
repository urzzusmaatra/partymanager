﻿//using CubeEngine.Core;

namespace CubeEngine.Storage
{
    public interface IStorage
    {
        bool IsEmpty { get; }
        IStoragable Item { get; }
        int Quantity { get; }
        float TotalWeight { get; }

        void AddQuantity(int quantity);
        int TryGetQuantity(int quantity);
    }
}