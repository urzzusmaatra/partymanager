﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

namespace CubeEngine.Storage
{
    public class InventoryTests
    {
        private class MockInventoryOwner : IInventoryOwner
        {
            public float GetMaxWeight { get; set; }
        }

        private class MockItem : IStoragable
        {
            public float Weight { get; set; }

            public int Id { get; set; }

        }

        [Test]
        public void AddingOneItemToTheInventory()
        {
            IInventory inventory = new Inventory(new MockInventoryOwner() { GetMaxWeight = 100f });
            var result = inventory.TryAddItem(new MockItem() { Weight = 10f, Id = 1 }, 1);
            Assert.IsTrue(result);
        }

        [Test]
        public void AddingFullInventory()
        {
            IInventory inventory = new Inventory(new MockInventoryOwner() { GetMaxWeight = 100f });
            var itemOne = new MockItem() { Weight = 10f, Id = 1 };
            var itemTwo = new MockItem() { Weight = 10f, Id = 2 };
            var result = inventory.TryAddItem(itemOne, 5);
            Assert.IsTrue(result);
            result = inventory.TryAddItem(itemTwo, 5);
            Assert.IsTrue(result);
        }

        [Test]
        public void OverweightSingleItemToInventory()
        {
            IInventory inventory = new Inventory(new MockInventoryOwner() { GetMaxWeight = 100f });
            var result = inventory.TryAddItem(new MockItem() { Weight = 101f, Id = 1 }, 1);
            Assert.IsFalse(result);
        }

        [Test]
        public void OverweightWithManyItemsInventory()
        {
            IInventory inventory = new Inventory(new MockInventoryOwner() { GetMaxWeight = 100f });
            var result = inventory.TryAddItem(new MockItem() { Weight = 10.1f, Id = 1 }, 10);
            Assert.IsFalse(result);
        }

        [Test]
        public void RemovingNonExistantItem()
        {
            Debug.unityLogger.logEnabled = false;
            var toTake = 1;
            IInventory inventory = new Inventory(new MockInventoryOwner() { GetMaxWeight = 100f });
            var item = new MockItem() { Weight = 10f, Id = 1 };
            Debug.unityLogger.logEnabled = false;
            var count = inventory.TryTakeItem(item, toTake);
            Debug.unityLogger.logEnabled = true;
            Assert.AreEqual(0, count);
        }

        [Test]
        public void RemovingItemsFromInventory()
        {
            var toTake = 1;
            IInventory inventory = new Inventory(new MockInventoryOwner() { GetMaxWeight = 100f });
            var item = new MockItem() { Weight = 10f, Id = 1 };
            inventory.TryAddItem(item, 10);
            var count = inventory.TryTakeItem(item, toTake);
            Assert.AreEqual(toTake, count);
        }

        [Test]
        public void RemovingAllItemsFromInventory()
        {
            var toTake = 10;
            IInventory inventory = new Inventory(new MockInventoryOwner() { GetMaxWeight = 100f });
            var item = new MockItem() { Weight = 10f, Id = 1 };
            var result = inventory.TryAddItem(item, toTake);
            Assert.IsTrue(result);
            var count = inventory.TryTakeItem(item, toTake);
            Assert.AreEqual(toTake, count);
        }

        [Test]
        public void RemovingTooManyItemsFromInventory()
        {
            var toTake = 10;
            var putIn = 8;
            IInventory inventory = new Inventory(new MockInventoryOwner() { GetMaxWeight = 100f });
            var item = new MockItem() { Weight = 10f, Id = 1 };
            inventory.TryAddItem(item, putIn);
            var count = inventory.TryTakeItem(item, toTake);
            Assert.AreEqual(putIn, count);
        }

        [Test]
        public void RemovingOtherItemsFromInventory()
        {
            var toTake = 10;
            var putIn = 8;
            IInventory inventory = new Inventory(new MockInventoryOwner() { GetMaxWeight = 100f });
            var item = new MockItem() { Weight = 1f, Id = 1 };
            var itemTwo = new MockItem() { Weight = 1f, Id = 2 };
            inventory.TryAddItem(item, putIn);
            inventory.TryAddItem(itemTwo, putIn);
            var count = inventory.TryTakeItem(item, toTake);
            Assert.AreEqual(putIn, count);
            count = inventory.TryTakeItem(itemTwo, toTake);
            Assert.AreEqual(putIn, count);
        }
    }
}