﻿//using CubeEngine.Core;

namespace CubeEngine.Storage
{
    public interface IInventoryOwner
    {
        float GetMaxWeight { get; }
    }
}