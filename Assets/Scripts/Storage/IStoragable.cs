﻿//using CubeEngine.Core;

namespace CubeEngine.Storage
{
    /// <summary>
    /// Identifies stuff that can go to storage and inventory
    /// </summary>
    public interface IStoragable
    {
        float Weight { get; }
        
        int Id { get; }
    }
}