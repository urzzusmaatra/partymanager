using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorTileValueComponent : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer spriteRenderer;
    [SerializeField]
    private Gradient color;
    public void SetValue(float value)
    {
        var normalized = Mathf.InverseLerp(-1f, 1f, value);
        spriteRenderer.color = this.color.Evaluate(normalized);
        gameObject.name = gameObject.name.Replace("Clone", value.ToString());
    }
}
