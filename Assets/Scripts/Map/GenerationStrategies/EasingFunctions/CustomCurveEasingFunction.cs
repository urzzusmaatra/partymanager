﻿using UnityEngine;

namespace Map.GenerationStrategies.EasingFunctions
{
    [CreateAssetMenu(fileName = "CustomCurveEasingFunction", menuName = "Map/EasingFunctions/CustomCurveEasingFunction")]
    public class CustomCurveEasingFunction : EasingFunction
    {
        [SerializeField]
        private UnityEngine.AnimationCurve curve;
        protected override float ChurnRatio(float ratio) => curve.Evaluate(ratio);
    }
}