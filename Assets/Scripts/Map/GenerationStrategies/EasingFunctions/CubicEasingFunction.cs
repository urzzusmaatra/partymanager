﻿using UnityEngine;

namespace Map.GenerationStrategies.EasingFunctions
{
    [CreateAssetMenu(fileName = "CubicEasingFunction", menuName = "Map/EasingFunctions/CubicEasingFunction")]
    public class CubicEasingFunction : EasingFunction
    {
        //-2(x^3) + 3(x^2) from http://pcgbook.com/wp-content/uploads/chapter04.pdf
        protected override float ChurnRatio(float ratio) => -2 * (ratio * ratio * ratio) + 3 * (ratio * ratio);
    }
}