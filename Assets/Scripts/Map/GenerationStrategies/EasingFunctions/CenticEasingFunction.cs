﻿using UnityEngine;

namespace Map.GenerationStrategies.EasingFunctions
{
    [CreateAssetMenu(fileName = "CenticEasingFunction", menuName = "Map/EasingFunctions/CenticEasingFunction")]
    public class CenticEasingFunction : EasingFunction
    {
        protected override float ChurnRatio(float ratio)
        {
            //6(x^5) - 15(x^4) + 10(x^3) from https://weber.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf
            return 6 * (ratio * ratio * ratio * ratio * ratio)
                - 15 * (ratio * ratio * ratio * ratio)
                + 10 * (ratio * ratio * ratio);
        }
    }
}