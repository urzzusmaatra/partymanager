﻿using UnityEngine;

namespace Map.GenerationStrategies.EasingFunctions
{
    [CreateAssetMenu(fileName = "QuinticEasingFunction", menuName = "Map/EasingFunctions/QuinticEasingFunction")]
    public class QuinticEasingFunction : EasingFunction
    {
        //return x === 0 ? 0 : Math.pow(2, 10 * x - 10); typescript; from https://easings.net/#easeInExpo
        protected override float ChurnRatio(float ratio) => ratio * ratio * ratio * ratio * ratio;
    }
}