﻿using UnityEngine;

namespace Map.GenerationStrategies.EasingFunctions
{
    [CreateAssetMenu(fileName = "EasingCombinator", menuName = "Map/EasingFunctions/EasingCombinator")]
    public class EasingFunctionCombinator : EasingFunction
    {
        [SerializeField]
        private Operations.Operation oparation;

        protected override float ChurnRatio(float ratio)
        {
            var val = -2 * (ratio * ratio * ratio) + 3 * (ratio * ratio);
            return oparation.Operate(val);
        }
    }
}