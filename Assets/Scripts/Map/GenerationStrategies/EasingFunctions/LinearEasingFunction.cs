﻿using UnityEngine;

namespace Map.GenerationStrategies.EasingFunctions
{
    [CreateAssetMenu(fileName = "LinearEasingFunction", menuName = "Map/EasingFunctions/LinearEasingFunction")]
    public class LinearEasingFunction : EasingFunction
    {
        protected override float ChurnRatio(float ratio) => ratio;
    }
}