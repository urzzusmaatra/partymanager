﻿using UnityEngine;

namespace Map.GenerationStrategies.EasingFunctions
{
    [CreateAssetMenu(fileName = "ExponentialEasingFunction", menuName = "Map/EasingFunctions/ExponentialEasingFunction")]
    public class ExponentialEasingFunction : EasingFunction
    {
        //return x === 0 ? 0 : Math.pow(2, 10 * x - 10); typescript; from https://easings.net/#easeInExpo
        protected override float ChurnRatio(float ratio) => Mathf.Pow(2f, (10f * ratio  - 10f));
    }
}