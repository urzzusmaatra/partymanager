using UnityEngine;

namespace Map.GenerationStrategies.EasingFunctions
{
    //TODO: Implement these https://easings.net/ looks fun
    public abstract class EasingFunction : ScriptableObject
    {
        protected float GetRatio(int point, int frequency) => point % frequency / (float)frequency;
        protected float Interpolate(float l, float r, float ratio) => l * (1f - ratio) + r * ratio;
        public float GetValue(float start, float end, int point, int frequency)
        {
            var ratio = GetRatio(point, frequency);
            ratio = ChurnRatio(ratio);
            return Interpolate(start, end, ratio);
        }

        protected abstract float ChurnRatio(float ratio);
    }
}