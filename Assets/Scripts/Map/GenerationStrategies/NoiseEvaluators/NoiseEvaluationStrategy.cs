﻿using UnityEngine;
using FrostSnail.Pangea;
using FrostSnail.Pangea.GenerationStrategies;
using System.Collections.Generic;

namespace Map.GenerationStrategies.NoiseEvaluators
{
    public abstract class NoiseEvaluationStrategy : MonoBehaviour, IMapGenerationStrategy
    {
        public abstract float Evaluate(int x, int y, INoiseAccessor noise, uint seed);
    }
}