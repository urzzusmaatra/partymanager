﻿using UnityEngine;
using FrostSnail.Pangea;
using System.Collections.Generic;
using Map.GenerationStrategies.PositionEvaluators;

namespace Map.GenerationStrategies.NoiseEvaluators.Implementations
{
    public class TreeGenerationNoiseEvaluationStrategy : NoiseEvaluationStrategy
    {
        [SerializeField]
        private List<PositionEvaluationStrategy> positionalEvaluators;
        [SerializeField]
        private List<NoiseEvaluationStrategy> noiseEvaluators;
        [SerializeField]
        private NoiseEvaluationStrategy typeSegragatorNoise;
        [SerializeField, Range(0f,1f)]
        private float segragatorStrength = 1f;
        public override float Evaluate(int x, int y, INoiseAccessor noise, uint seed)
        {
            var posEvalCount = positionalEvaluators.Count;
            var positionEvaluation = (posEvalCount == 0) ? 1f : 0f;//if 0 then consider noise evaluation full strength
            foreach (var evaluator in positionalEvaluators)
            {
                positionEvaluation += evaluator.Evaluate(x, y) / posEvalCount;
            }

            var noiseEvaluation = 0f;
            var noiseEvalCount = 1f;
            foreach (var evaluator in noiseEvaluators)
            {
                noiseEvaluation += evaluator.Evaluate(x, y, noise, seed) / noiseEvalCount;

                noiseEvalCount += 1f;
            }

            var evaluation = noiseEvaluation * positionEvaluation; ;

            if (typeSegragatorNoise != null)
            {
                evaluation += typeSegragatorNoise.Evaluate(x, y, noise, seed) * segragatorStrength;
            }

            return Mathf.Clamp(evaluation, -1f, 1f); ;
        }
    }
}