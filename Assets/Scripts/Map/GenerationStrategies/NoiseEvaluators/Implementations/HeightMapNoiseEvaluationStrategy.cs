﻿using UnityEngine;
using FrostSnail.Pangea;
using System.Collections.Generic;

namespace Map.GenerationStrategies.NoiseEvaluators.Implementations
{
    public class HeightMapNoiseEvaluationStrategy : NoiseEvaluationStrategy
    {
        [SerializeField]
        private List<NoiseEvaluationStrategy> evaluators;

        public override float Evaluate(int x, int y, INoiseAccessor noise, uint seed)
        {
            float result = 0f;
            float value = 1f;
            float divisor = 0f;

            foreach (var evaluator in evaluators)
            {
                result += evaluator.Evaluate(x, y, noise, seed) * value;
                divisor += value;
                value *= .5f;
            }

            result /= divisor;

            return result;
        }
    }
}