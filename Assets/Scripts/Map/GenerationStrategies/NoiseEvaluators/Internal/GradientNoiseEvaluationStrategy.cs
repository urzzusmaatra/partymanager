﻿using UnityEngine;
using FrostSnail.Pangea;
using Sirenix.OdinInspector.Editor;

namespace Map.GenerationStrategies.NoiseEvaluators.Internal
{
    using EasingFunctions;

    public class GradientNoiseEvaluationStrategy : NoiseEvaluationStrategy
    {
        //why like this? this is why
        //https://www.youtube.com/watch?v=MJ3bvCkHJtE&ab_channel=Fataho
        private const float FULL_CIRCLE_RAD = Mathf.PI * 2f;

        [SerializeField]
        private int frequency = 4;
        [SerializeField]
        private Operations.Operation apexValueOperator;
        [SerializeField]
        private Operations.Operation fillOperation;
        [SerializeField]
        private EasingFunction easingFunction;

        public override float Evaluate(int x, int y, INoiseAccessor noise, uint seed)
        {
            Vector2Int point = new Vector2Int(x, y);

            GetLatticePoints(point, out Vector2Int topLeft, out Vector2Int topRight, out Vector2Int bottomLeft, out Vector2Int bottomRight);

            float topLeftValue = GetApexValue(topLeft, point, noise, seed);
            float topRightValue = GetApexValue(topRight, point, noise, seed);
            var top = easingFunction.GetValue(topLeftValue, topRightValue, y, frequency);

            float bottomLeftValue = GetApexValue(bottomLeft, point, noise, seed);
            float bottomRightValue = GetApexValue(bottomRight, point, noise, seed);
            var bottom = easingFunction.GetValue(bottomLeftValue, bottomRightValue, y, frequency);

            var finalValue = easingFunction.GetValue(top, bottom, x, frequency);

            return finalValue;
        }

        private Vector2 GetFillDirection(float fill)
        {
            //fill = Mathf.Clamp01(fill);

            if (fillOperation != null)
                fill = fillOperation.Operate(fill);


            var circleFill = FULL_CIRCLE_RAD * fill;
            var cosPosition = Vector3.right * Mathf.Cos(circleFill);
            var sinPosition = Vector3.up * Mathf.Sin(circleFill);

            return cosPosition + sinPosition;
        }

        private float GetApexValue(Vector2Int apexPosition, Vector2Int pointPosition, INoiseAccessor noise, uint seed)
        {
            var dirValue = noise.Evaluate(apexPosition.x, apexPosition.y, seed);
            var dirStrenght = noise.Evaluate(apexPosition.x, apexPosition.y, 1, seed) * 2f;
            Vector2 apexPeronalDirection = GetFillDirection(dirValue) * dirStrenght;

            Vector2 apexToPointDirection = (new Vector2(pointPosition.x, pointPosition.y) - new Vector2(apexPosition.x, apexPosition.y));
            apexToPointDirection = apexToPointDirection / frequency;//normalize to frequency

            var apexValue = Vector2.Dot(apexPeronalDirection, apexToPointDirection);

            if (apexValueOperator != null)
                apexValue = apexValueOperator.Operate(apexValue);

            return apexValue;
        }

        private void GetLatticePoints(Vector2Int point, out Vector2Int topLeft, out Vector2Int topRight, out Vector2Int bottomLeft, out Vector2Int bottomRight)
        {
            var xLow = point.x - (point.x % frequency);
            var yLow = point.y - (point.y % frequency);

            topLeft = new Vector2Int(xLow, yLow);
            topRight = new Vector2Int(xLow, yLow + frequency);
            bottomLeft = new Vector2Int(xLow + frequency, yLow);
            bottomRight = new Vector2Int(xLow + frequency, yLow + frequency);
        }

        [Sirenix.OdinInspector.DisableInPlayMode]
        [Sirenix.OdinInspector.Button("Rename object")]
        private void RenameObject(string name = "Gradient")
        {
            string easingFunctionName = string.Empty;

            if(easingFunction)
            {
                easingFunctionName = easingFunction.name.Replace("EasingFunction", "");
                easingFunctionName = $"f({easingFunctionName})";
            }

            gameObject.name = $"Fz({frequency}) {easingFunctionName} Gradient";
        }
    }
}