﻿using FrostSnail.Core.Shuffler;
using FrostSnail.Pangea;
using UnityEngine;

namespace Map.GenerationStrategies.NoiseEvaluators.Internal
{
    public class AsIsNoiseEvaluationStrategy : NoiseEvaluationStrategy
    {
        [SerializeField]
        private bool negateResult = false;
        [SerializeField]
        private int modifier = 0;
        public override float Evaluate(int x, int y, INoiseAccessor noise, uint seed)
        {
            return noise.Evaluate(x, y, modifier, seed) * (negateResult ? -1f : 1f);
        }
    }
}