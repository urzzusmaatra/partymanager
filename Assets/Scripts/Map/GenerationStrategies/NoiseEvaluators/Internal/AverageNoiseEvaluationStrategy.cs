﻿using FrostSnail.Pangea;
using System.Collections.Generic;
using UnityEngine;

namespace Map.GenerationStrategies.NoiseEvaluators.Internal
{
    public class AverageNoiseEvaluationStrategy : NoiseEvaluationStrategy
    {
        [SerializeField]
        private List<NoiseEvaluationStrategy> strategies;
        public override float Evaluate(int x, int y, INoiseAccessor noise, uint seed)
        {
            var value = 0f;
            foreach (var strategy in strategies)
            {
                value += strategy.Evaluate(x, y, noise, seed);
            }

            return value / strategies.Count;
        }
    }
}