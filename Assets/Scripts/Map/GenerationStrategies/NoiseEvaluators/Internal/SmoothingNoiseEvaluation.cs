﻿using UnityEngine;
using FrostSnail.Pangea;

namespace Map.GenerationStrategies.NoiseEvaluators.Internal
{
    using EasingFunctions;
    public class SmoothingNoiseEvaluation : NoiseEvaluationStrategy
    {
        [SerializeField] private uint frequency = 10;
        private int Frequency => (int)frequency;
        [SerializeField]
        private EasingFunction easingFunction;

        public override float Evaluate(int x, int y, INoiseAccessor noise, uint seed)
        {
            var xBase = x - (x % Frequency);
            var yBase = y - (y % Frequency);

            float topLeftValue = noise.Evaluate(xBase, yBase, seed);
            float topRightValue = noise.Evaluate(xBase, yBase + Frequency, seed);
            var top = easingFunction.GetValue(topLeftValue, topRightValue, y, Frequency);

            float bottomLeftValue = noise.Evaluate(xBase + Frequency, yBase, seed);
            float bottomRightValue = noise.Evaluate(xBase + Frequency, yBase + Frequency, seed);
            var bottom = easingFunction.GetValue(bottomLeftValue, bottomRightValue, y, Frequency);

            var finalValue = easingFunction.GetValue(top, bottom, x, Frequency);

            return finalValue;
        }

    }
}