﻿using FrostSnail.Pangea;
using UnityEngine;
using FrostSnail.Pangea.GenerationStrategies;

namespace Map.GenerationStrategies
{
    public class CreateSomeTiles : MonoBehaviour, IMapGenerationStrategy
    {
        [SerializeField]
        private AnimationCurve filter;
        [SerializeField]
        private int tileId;
        private const int EMPTY_TILE_ID = 0;

        private bool IsTileEmpty(int tileId) => tileId == EMPTY_TILE_ID;

        public float Evaluate(int x, int y, INoiseAccessor noiseMap, uint seed)
        {
            return noiseMap.Evaluate(x, y, seed);
        }

    }
}