﻿using Map.GenerationStrategies.PositionEvaluators;
using UnityEngine;

namespace Map.GenerationStrategies.PositionEvaluators.Internal
{
    public class CircledCurvePositionalEvaluationStrategy : PositionEvaluationStrategy
    {
        [SerializeField]
        private AnimationCurve gradient;
        [SerializeField]
        private Vector2Int size = Vector2Int.one;

        public override float Evaluate(int x, int y)
        {
            var center = new Vector2((float)size.x / 2f, (float)size.y / 2f);
            var refPosition = new Vector2(x % size.x, y % size.y);
            var normalizedPosition = new Vector2((float)refPosition.x / center.x, (float)refPosition.y / (center.y));

            var magnitude = normalizedPosition.magnitude;

            return gradient.Evaluate(magnitude);
        }
    }
}