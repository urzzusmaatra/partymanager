﻿using FrostSnail.Pangea;
using Map.GenerationStrategies.PositionEvaluators;
using UnityEngine;

namespace Map.GenerationStrategies.PositionEvaluators.Internal
{
    public class CrossPositionalEvaluationStrategy : PositionEvaluationStrategy
    {
        [SerializeField]
        private Vector2Int size = Vector2Int.one;
        [SerializeField]
        private Vector2Int center = Vector2Int.one;
        [SerializeField]
        private uint halfThicknes = 1;
        private int HalfThickness => (int)halfThicknes;
        [SerializeField]
        private float valueIn = 0f;
        [SerializeField]
        private float valueOut = 1f;
        public override float Evaluate(int x, int y)
        {
            var refPosition = new Vector2Int(x % size.x, y % size.y);
            var xSize = new Vector2Int(center.x - HalfThickness, center.x + HalfThickness);
            var ySize = new Vector2Int(center.y - HalfThickness, center.y + HalfThickness);

            if (xSize.x < refPosition.x && xSize.y > refPosition.x)
                return valueIn;

            if (ySize.x < refPosition.y && ySize.y > refPosition.y)
                return valueIn;

            return valueOut;
        }
    }
}