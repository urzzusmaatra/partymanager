﻿using UnityEngine;

namespace Map.GenerationStrategies.PositionEvaluators
{
    public abstract class PositionEvaluationStrategy : MonoBehaviour
    {
        public abstract float Evaluate(int x, int y);
    }
}