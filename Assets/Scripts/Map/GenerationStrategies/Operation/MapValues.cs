﻿using UnityEngine;

namespace Map.GenerationStrategies.Operations
{
    [CreateAssetMenu(fileName = "MapValues", menuName = "Map/Operators/MapValues")]
    public class MapValues : Operation
    {
        [SerializeField] private float fromMax = 1f;
        [SerializeField] private float toMax = 1f;
        [SerializeField] private float fromMin = 0f;
        [SerializeField] private float toMin = 0f;
        public override float Operate(float value)
        {
            return Mathf.Lerp(toMax, toMin, Mathf.InverseLerp(fromMax, fromMin, value));
            //return Map(value, fromMax, toMax, fromMin, toMin);
        }

        public float Map(float value, float from1, float to1, float from2, float to2)
        {
            //return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
            return Mathf.Lerp(toMax, toMin, Mathf.InverseLerp(fromMax, fromMin, value));
        }
    }
}