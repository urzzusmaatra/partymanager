﻿using UnityEngine;

namespace Map.GenerationStrategies.Operations
{
    [CreateAssetMenu(fileName = "MultiplayOperator", menuName = "Map/Operators/Multiply")]
    public class MultiplyOperation : Operation
    {
        [SerializeField]
        private float multiplicator;

        public override float Operate(float value)
        {
            return value * multiplicator;
        }
    }
}