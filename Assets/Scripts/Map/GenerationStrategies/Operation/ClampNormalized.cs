﻿using UnityEngine;

namespace Map.GenerationStrategies.Operations
{
    [CreateAssetMenu(fileName = "ClampNormalized", menuName = "Map/Operators/Clamp01")]
    public class ClampNormalized : Operation
    {
        public override float Operate(float value)
        {
            return Mathf.Clamp01(value);
        }
    }
}