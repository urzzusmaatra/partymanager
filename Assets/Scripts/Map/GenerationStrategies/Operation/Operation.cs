﻿using UnityEngine;

namespace Map.GenerationStrategies.Operations
{
    public abstract class Operation : ScriptableObject
    {
        public abstract float Operate(float value);
    }
}