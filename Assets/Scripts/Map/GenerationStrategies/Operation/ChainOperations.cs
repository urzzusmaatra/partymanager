﻿using UnityEngine;
using System.Collections.Generic;

namespace Map.GenerationStrategies.Operations
{
    [CreateAssetMenu(fileName = "ChainOperations", menuName = "Map/Operators/ChainOperations")]
    public class ChainOperations : Operation
    {
        [SerializeField]
        private List<Operation> operations;

        public override float Operate(float value)
        {
            foreach (var operation in operations)
            {
                value = operation.Operate(value);
            }

            return value;
        }
    }
}