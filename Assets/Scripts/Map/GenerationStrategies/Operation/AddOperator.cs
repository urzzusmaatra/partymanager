﻿using UnityEngine;

namespace Map.GenerationStrategies.Operations
{
    [CreateAssetMenu(fileName = "AddOperator", menuName = "Map/Operators/Add")]
    public class AddOperator : Operation
    {
        [SerializeField]
        private float value = 0f;

        public override float Operate(float value)
        {
            return value + this.value;
        }
    }
}