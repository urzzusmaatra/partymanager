﻿using UnityEngine;

namespace Map.GenerationStrategies.Operations
{
    [CreateAssetMenu(fileName = "RoundToNearestInt", menuName = "Map/Operators/RoundToNearestInt")]
    public class RoundToNearestInt : Operation
    {
        [SerializeField]
        private int rounder;

        public override float Operate(float value)
        {
            return Mathf.Ceil(value * rounder) / rounder;
        }
    }
}