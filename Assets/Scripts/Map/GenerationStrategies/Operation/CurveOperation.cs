﻿using UnityEngine;

namespace Map.GenerationStrategies.Operations
{
    [CreateAssetMenu(fileName = "CurveOperator", menuName = "Map/Operators/Animation Curve")]
    public class CurveOperation : Operation
    {
        [SerializeField]
        private AnimationCurve curveManipulator;
        public override float Operate(float value)
        {
            return curveManipulator.Evaluate(value);
        }
    }
}