﻿using UnityEngine;
using Map.MapServices;
using Map.MapServices.Internal;

namespace Map.GenerationStrategies
{
    [RequireComponent(typeof(MapComponent))]
    public class ColorTiles : MonoBehaviour, ITileModifier
    {
        public void ModifyTile(int x, int y, GameObject tile, float value)
        {
            var colorComponent = tile.GetComponent<ColorTileValueComponent>();
            colorComponent.SetValue(value);
        }
    }
}