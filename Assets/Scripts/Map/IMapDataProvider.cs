﻿using FrostSnail.Pangea.GenerationStrategies;

namespace Map
{
    public interface IMapDataProvider
    {
        IMapGenerationStrategy GenerationStrategy { get; }
        bool HasModifier { get; }
        string MapName { get; }
        int Modifier { get; }
    }
}