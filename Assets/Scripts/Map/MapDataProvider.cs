﻿using UnityEngine;
using FrostSnail.Pangea.GenerationStrategies;
using Map.MapServices.Internal;

namespace Map
{
    public abstract class MapDataProvider : MonoBehaviour, IMapDataProvider
    {
        public abstract IMapGenerationStrategy GenerationStrategy { get; }
        public abstract string MapName { get; }
        public abstract bool HasModifier { get; }
        public abstract int Modifier { get; }
    }
}