﻿using UnityEngine;
using FrostSnail.Events;
using FrostSnail.Events.Internal;

namespace Map.MapServices.Events
{
    public static class MapChannel
    {
        private static IEventChannel channel;
        public static IEventChannel Channel
        {
            get
            {
                //take care for this chaching here when reloading scenes etc.
                if (channel == null)
                {
                    var channelObject = new GameObject("MapChannel", typeof(EventChannelComponent));
                    channelObject.TryGetComponent(out channel);
                }

                return channel;
            }
        }
    }
}