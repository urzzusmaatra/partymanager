﻿using UnityEngine;
using Utility.Logging;
using FrostSnail.Events;
using Map.MapServices.Events;
using Map.MapService.Internal.MapEvaluators;
using Services;

#if UNITY_EDITOR
using Map.MapService.Internal.Debug;
#endif

namespace Map.MapServices.Internal
{
    [RequireComponent(typeof(MapEvaluatorCollectionComponent))]
    public class MapComponent : MonoBehaviour
    {
        [SerializeField]
        private Vector2 expectedTileSize = Vector2.one;
        private float mapHeight = 0f;
        private GameObject[,] map;
        [SerializeField]
        private int mapSize = 56;
        public int MapSize => mapSize;

        [UnityEngine.Serialization.FormerlySerializedAs("mapGenerationStrategy")]
        private MapEvaluatorCollectionComponent mapEvaluatorCollectionComponent;
        private EventReceiver eventHandler;

        private IMapService mapService;

        private void Start()
        {
            mapEvaluatorCollectionComponent = GetComponent<MapEvaluatorCollectionComponent>();

            eventHandler = EventReceiverFactory.CreateEventReceiver(MapChannel.Channel);
            eventHandler.RegisterMethod<RequestMapCreationEvent>(RequestMapCreationEventReaction);
            eventHandler.RegisterMethod<RequestMapClearEvent>(RequestMapClearEventReaction);

#if UNITY_EDITOR
            MapDebugObject.TryCreate();
#endif
        }

        private void OnDestroy()
        {
            if(eventHandler != null)
            {
                eventHandler.UnregisterMethod<RequestMapCreationEvent>(RequestMapCreationEventReaction);
                eventHandler.UnregisterMethod<RequestMapClearEvent>(RequestMapClearEventReaction);
            }
        }

        private bool IsMapSet => map != null;
        
        private void ClearMap()
        {
            for (int x = map.GetLength(0) - 1; x >= 0; x--)
            {
                for (int y = map.GetLength(1) - 1; y >= 0; y--)
                {
                    RemoveTile(map[x, y]);
                }
            }
            map = null;
        }

        private void RemoveTile(GameObject tile)
        {
            if (tile)
            {
                GameObject.Destroy(tile);
            }
        }
        
        private void CreateMap()
        {
            if (!isActiveAndEnabled)
                return;

            var mapService = GameServices.GetService<IMapService>();
            if (mapService == null)
            {
                Log.LogBaseError("No MapService avaliable.", transform);
                return;
            }

            (mapService as MapService).map = this;

            if (IsMapSet)
            {
                ClearMap();
            }

            map = new GameObject[mapSize, mapSize];

            mapEvaluatorCollectionComponent.PrepareEvaluation();

            ITileModifier tileModifier = GetComponent<ITileModifier>();

            for (int x = 0; x < mapSize; x++)
            {
                for (int y = 0; y < mapSize; y++)
                {
                    if(!mapEvaluatorCollectionComponent.GetTilePrefab(x, y, out GameObject tilePrefab, out float value))
                    {
                        continue;
                    }

                    //move pool somewhere else
                    //if (!TilePool.Exists)
                    //    TilePool.CreateInstance(16000, tilePrefab);
                    GameObject tile = null;
                    if(tilePrefab != null)
                    {
                        tile =  GameObject.Instantiate(tilePrefab);
                    }

                    if (tile != null)
                        AddTile(x, y, tile/*, value*/);

                    if (tileModifier != null)
                    {
                        tileModifier.ModifyTile(x, y, tile, value);
                    }
                }
            }

            var mapCreatedEvent = EventFactory.GetEvent<MapCreatedEvent>();
            mapCreatedEvent.SendTo(MapChannel.Channel, transform);
            AstarPath.active.Scan();//for now here
        }

        private void AddTile(int x, int y, GameObject tile/*, float value*/)
        {
            if (map == null)
            {
                Log.LogBaseError("Map size isn't set!");
                return;
            }

            tile.transform.SetParent(transform);
            tile.transform.localPosition = new Vector3(x * expectedTileSize.x, y * expectedTileSize.y, mapHeight);
            map[x, y] = tile;
        }

        private void RequestMapCreationEventReaction(RequestMapCreationEvent data) => CreateMap();
        private void RequestMapClearEventReaction(RequestMapClearEvent data) => ClearMap();

        [Sirenix.OdinInspector.DisableInEditorMode]
        [Sirenix.OdinInspector.Button("Create map")]
        public void RequestMapCreation()
        {
            var createEvent = EventFactory.GetEvent<RequestMapCreationEvent>();
            createEvent.SendTo(MapChannel.Channel, transform);
        }

        [Sirenix.OdinInspector.DisableInEditorMode]
        [Sirenix.OdinInspector.Button("Clear map")]
        public void RequestMapClearance()
        {
            var clearEvent = EventFactory.GetEvent<RequestMapClearEvent>();
            clearEvent.SendTo(MapChannel.Channel, transform);
        }
    }
}