﻿using System.Collections.Generic;
using UnityEngine;
using FrostSnail.Broker;

namespace Map.MapServices.Internal
{
    [CreateAssetMenu(fileName = "MapTileData", menuName = "Map/TileData")]
    public class MapTileData : ScriptableObject
    {
        [SerializeField]
        private SerializedBrokerMarket<GameObject> tiles;

        public GameObject GetTilePrefab(float value)
        {
            return tiles.GetWeightNormalized(value);
        }
    }
}