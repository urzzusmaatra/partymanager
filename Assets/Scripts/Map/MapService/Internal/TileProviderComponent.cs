﻿using UnityEngine;

namespace Map.MapServices.Internal
{
    public class TileProviderComponent : MonoBehaviour
    {
        //tilepool bro, remember it
        public GameObject PopTile(GameObject prefab)
        {
            return Instantiate(prefab);
        }

        public void Push(GameObject tile)
        {
            //how on earth am I going to return tile back without knowing to which pool it belongs
            Destroy(tile);
        }
    }
}