﻿using FrostSnail.Pangea;
using UnityEngine;

namespace Map.MapServices.Internal
{
    [System.Serializable]
    public class MapPartTileEvaluator
    {
        [SerializeField]
        private MapTileData tileData;
        [SerializeField]
        private string mapEvaluatorName;
        [SerializeField]
        private bool enabled = true;
        public string MapEvaluatorName => mapEvaluatorName;

        public GameObject GetTilePrefab(float value) => tileData.GetTilePrefab(value);

        public bool ShouldPlace(float value, float treshold)
        {
            if (!enabled)
                return false;

            value = Mathf.InverseLerp(-1f, 1f, value);

            return 1f - value <= treshold;
        }

        public float GetValue(MapGenerator mapGenerator, int x, int y) => mapGenerator.EvaluatePosition(x, y);

    }

}