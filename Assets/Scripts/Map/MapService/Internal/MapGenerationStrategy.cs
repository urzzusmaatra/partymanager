﻿using FrostSnail.Pangea;
using Map.MapServices.Internal;
using Services;
using System;
using UnityEngine;
using Worlds;

namespace Map.MapServices
{
    public class MapGenerationStrategy : MonoBehaviour
    {
        private IMapService mapService;
        private IWorldService worldService;

        private MapGenerator mapGenerator;

        [SerializeField]
        private MapPartTileEvaluator mapTileEvaluator;

        private void Start()
        {
            GameServices.GetService(GetMapService); 
            GameServices.GetService(GetWorldService);
        }
            
        public void PrepareEvaluation()
        {
            mapService.TryGetMapGenerator(mapTileEvaluator.MapEvaluatorName, out mapGenerator);
            mapGenerator.ResetSeed(worldService.Seed);
        }

        public float GetPositionValue(int x, int y)
        {
            return mapTileEvaluator.GetValue(mapGenerator, x, y);
        }

        private bool GetMapService(IService service)
        {
            if (service is IMapService mapService)
            {
                this.mapService = mapService;
                return true;
            }

            return false;
        }

        private bool GetWorldService(IService service)
        {
            if (service is IWorldService worldService)
            {
                this.worldService = worldService;
                return true;
            }

            return false;
        }
    }
}