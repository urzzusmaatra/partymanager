﻿using UnityEngine;

namespace Map.MapServices.Internal
{
    [System.Serializable]
    public struct TileKeyValue
    {
        public int key;
        public GameObject tile;
    }
}