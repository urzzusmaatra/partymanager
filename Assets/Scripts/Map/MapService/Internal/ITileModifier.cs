﻿using UnityEngine;

namespace Map.MapServices
{
    public interface ITileModifier
    {
        public void ModifyTile(int x, int y, GameObject tile, float value);
    }
}