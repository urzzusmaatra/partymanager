﻿using Utility.Logging.Internal;

namespace Map.MapServices.Internal
{
    public class MapServiceLogChannelHandler : ChannelHandler
    {
        public override string ChannelName => MapServiceLogChannel.NAME;
    }

    public class MapServiceLogChannel
    {
        public const string NAME = "MapServiceChannel";
    }
}