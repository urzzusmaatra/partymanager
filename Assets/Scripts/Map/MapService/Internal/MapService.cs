using FrostSnail.Core.Shuffler;
using FrostSnail.Core.Shuffler.Randoms;
using FrostSnail.Pangea;
using System.Collections.Generic;
using UnityEngine;
using Utility.Logging;

namespace Map.MapServices.Internal
{
    public class MapService : IMapService
    {
        private Dictionary<string, MapGenerator> mapDatas;

        private INoise noise;
        private INoise Noise //is this nopise really maps buisiness? 
        {
            get
            {
                if (noise == null)
                {
                    noise = new SquirrelNoise5();
                }
                return noise;
            }
        }

        public MapComponent map { private get; set; }
        public int MapSize => map.MapSize;

        private MapService()
        {
            mapDatas = new Dictionary<string, MapGenerator>();
        }

        internal static IMapService CreateBasicService() => new MapService();

        public void SetMapGenerator(IMapDataProvider mapGenerator)
        {
            if (mapGenerator.HasModifier)
            {
                mapDatas.Add(mapGenerator.MapName, MapGenerator.Create(Noise, 0, mapGenerator.GenerationStrategy, mapGenerator.Modifier));
            }
            else
            {
                mapDatas.Add(mapGenerator.MapName, MapGenerator.Create(Noise, 0, mapGenerator.GenerationStrategy));
            }
        }

        public bool TryGetMapGenerator(string name, out MapGenerator value) => mapDatas.TryGetValue(name, out value);

        public float GetValueFromNoise(uint seed, int x, int y = 0, int z = 0, int w = 0) => Noise.GetValue(x, y, z, w, seed);

        public bool IsTileOnMap(Vector2Int spawnerPosition)
        {
            if(spawnerPosition.x < 0 || spawnerPosition.y < 0)
            {
                return false;
            }

            if(spawnerPosition.x >= MapSize || spawnerPosition.y >= MapSize)
            {
                return false;
            }

            return true;
        }

        public bool IsTileWalkable(Vector2Int spawnerPosition)
        {
            Log.LogBaseError("IsTileWalkable(Vector2Int spawnerPosition) is not implemented");
            return true;
        }
    }

}