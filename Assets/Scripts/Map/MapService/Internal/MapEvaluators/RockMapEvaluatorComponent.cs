﻿namespace Map.MapService.Internal.MapEvaluators
{
    public class RockMapEvaluatorComponent : MapEvaluatorComponent
    {
        protected override float EntityStrength => worldService.CurrentWorldChunkInfo.rockStrenght;
    }
}