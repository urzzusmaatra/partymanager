﻿using Map.GenerationStrategies;

namespace Map.MapService.Internal.MapEvaluators
{
    public class DebugMapEvaluatorComponent : MapEvaluatorComponent
    {
        protected override float EntityStrength => 1f;

    }
}