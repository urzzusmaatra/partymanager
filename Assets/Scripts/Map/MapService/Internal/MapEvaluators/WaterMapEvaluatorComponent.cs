﻿namespace Map.MapService.Internal.MapEvaluators
{
    public class WaterMapEvaluatorComponent : MapEvaluatorComponent
    {
        protected override float EntityStrength => worldService.CurrentWorldChunkInfo.waterStrenght;
    }
}