﻿namespace Map.MapService.Internal.MapEvaluators
{
    public class GrassMapEvaluatorComponent : MapEvaluatorComponent
    {
        protected override float EntityStrength => worldService.CurrentWorldChunkInfo.grassStrenght;
    }
}