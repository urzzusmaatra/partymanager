﻿namespace Map.MapService.Internal.MapEvaluators
{
    public class FloraMapEvaluatorComponent : MapEvaluatorComponent
    {
        protected override float EntityStrength => worldService.CurrentWorldChunkInfo.floraStrenght;
    }
}