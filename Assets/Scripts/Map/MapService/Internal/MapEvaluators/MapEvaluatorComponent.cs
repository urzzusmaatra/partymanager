﻿using UnityEngine;
using Map.MapServices;
using FrostSnail.Pangea;
using Services;
using Worlds;
using Map.MapServices.Internal;

namespace Map.MapService.Internal.MapEvaluators
{
    public abstract class MapEvaluatorComponent : MonoBehaviour
    {
        private IMapService mapService;
        protected IWorldService worldService;

        [SerializeField]
        private MapPartTileEvaluator mapTileEvaluator;

        protected abstract float EntityStrength { get; }

        private void Awake()
        {
            GameServices.GetService(GetMapService);
            GameServices.GetService(GetWorldService);
        }

        public void Initalize()
        {
            mapService.TryGetMapGenerator(mapTileEvaluator.MapEvaluatorName, out MapGenerator mapGenerator);
            mapGenerator.ResetSeed(worldService.Seed);
        }

        public bool TryEvaluateMapPosition(int x, int y, out GameObject tilePrefab, out float value)
        {
            tilePrefab = null;

            mapService.TryGetMapGenerator(mapTileEvaluator.MapEvaluatorName, out MapGenerator mapGenerator);
            value = mapTileEvaluator.GetValue(mapGenerator, x, y);

            if (mapTileEvaluator.ShouldPlace(value, EntityStrength))
            {
                tilePrefab = mapTileEvaluator.GetTilePrefab(value);
                return true;
            }

            return false;
        }

        private bool GetMapService(IService service)
        {
            if (service is IMapService mapService)
            {
                this.mapService = mapService;
                return true;
            }

            return false;
        }

        private bool GetWorldService(IService service)
        {
            if (service is IWorldService worldService)
            {
                this.worldService = worldService;
                return true;
            }

            return false;
        }
    }
}