﻿using FrostSnail.Pangea;
using Services;
using UnityEngine;
using Worlds;
using System.Collections.Generic;
using static Utility.Logging.Log;
using Map.MapServices;

namespace Map.MapService.Internal.MapEvaluators
{

    public class MapEvaluatorCollectionComponent : MonoBehaviour
    {
        [SerializeField]
        private List<MapEvaluatorComponent> mapEvaluators;

        public void PrepareEvaluation()
        {
            foreach (var mapEvaluator in mapEvaluators)
            {
                mapEvaluator.Initalize();
            }
        }

        public bool GetTilePrefab(int x, int y, out GameObject tilePrefab, out float value)
        {
            tilePrefab = null;
            value = 0f;

            foreach (var mapEvaluator in mapEvaluators)
            {
                if (mapEvaluator.TryEvaluateMapPosition(x, y, out tilePrefab, out value))
                    return true;
            }

            return false;
        }

    }
}