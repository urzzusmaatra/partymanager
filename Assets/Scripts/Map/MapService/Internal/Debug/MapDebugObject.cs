﻿using UnityEngine;
using FrostSnail.Events;
using Map.MapServices.Events;
using Worlds.Events;

namespace Map.MapService.Internal.Debug
{
#if !UNITY_EDITOR //keeping this to disallow usage outside the editor
public class MapDebugObject : MonoBehaviour {}
#else
    public class MapDebugObject : MonoBehaviour
    {
        private static GameObject cachedInstance;
        public static void TryCreate()
        {
            if (cachedInstance == null)
            {
                cachedInstance = new GameObject("MapDebugObject", typeof(MapDebugObject));
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.M))
            {
                RequestWorldCreation();
            }

            if(Input.GetKeyDown(KeyCode.R))
            {
                var createEvent = EventFactory.GetEvent<RequestSeedRegenerationEvent>();
                createEvent.SendTo(WorldChannel.Channel, transform);
                RequestWorldCreation();
            }
        }

        private void RequestWorldCreation()
        {
            var createEvent = EventFactory.GetEvent<RequestMapCreationEvent>();
            createEvent.SendTo(MapChannel.Channel, transform);
        }
    }
#endif
}