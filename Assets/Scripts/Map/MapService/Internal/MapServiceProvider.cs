using UnityEngine;
using Services;
using Services.Internal;

namespace Map.MapServices.Internal
{
    public class MapServiceProvider : MonoBehaviour
    {
        void Start()
        {
            GameServices.Provide(ServicesConfiguration.GetMapService());
        }
    }
}