using FrostSnail.Pangea;
using Services;
using UnityEngine;

namespace Map.MapServices
{
    public interface IMapService : IService
    {
        void SetMapGenerator(IMapDataProvider mapGenerator);
        bool TryGetMapGenerator(string name, out MapGenerator value);
        float GetValueFromNoise(uint seed, int x, int y = 0, int z = 0, int w = 0);

        int MapSize { get; }

        bool IsTileOnMap(Vector2Int spawnerPosition);
        bool IsTileWalkable(Vector2Int spawnerPosition);
    }

}