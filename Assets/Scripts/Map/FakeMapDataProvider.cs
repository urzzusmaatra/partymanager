﻿using UnityEngine;
using FrostSnail.Pangea.GenerationStrategies;
using Map.MapServices.Internal;

namespace Map
{
    [RequireComponent(typeof(IMapGenerationStrategy))]
    public class FakeMapDataProvider : MapDataProvider
    {
        public override IMapGenerationStrategy GenerationStrategy => GetComponent<IMapGenerationStrategy>();

        [SerializeField]
        private string mapName = string.Empty;
        public override string MapName => mapName;

        [SerializeField]
        private bool hasModifier = false;
        public override bool HasModifier => hasModifier;

        [SerializeField]
        [Sirenix.OdinInspector.ShowIf(condition: "hasModifier", animate: true)]
        private int modifier = 0;
        public override int Modifier => modifier;
    }
}