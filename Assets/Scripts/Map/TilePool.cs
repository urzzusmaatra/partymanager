﻿using System.Collections.Generic;
using UnityEngine;
using Utility.Logging;

namespace Map
{
    public class TilePool
    {
        private static TilePool _instance;
        public static TilePool Instance
        {
            get
            {
                if (_instance == null)
                    Log.LogBaseError("Create Tile instance by calling CreateInstance(size, prefab)");

                return _instance;
            }
        }

        public static void CreateInstance(int poolSize, GameObject prefab)
        {
            if(_instance == null)
                _instance = new TilePool(poolSize, prefab);
        }
        public static bool Exists => _instance != null;

        private Vector3 AwayPosition = Vector3.one * 1000f;

        private GameObject prefab;
        private GameObject[] avaliableTiles;
        private int avaliabilityIndex = -1;
        private int size;
        public TilePool(int size, GameObject prefab)
        {
            avaliableTiles = new GameObject[size];
            this.prefab = prefab;
            this.size = size;
        }

        public bool CanPop => avaliabilityIndex >= 0;

        public GameObject Pop()
        {
            //recash avialibility index if scene change destorys pooled objects
            var obj = avaliableTiles[avaliabilityIndex];
            avaliableTiles[avaliabilityIndex] = null;
            avaliabilityIndex--;
            return obj;
        }

        public GameObject Create()
        {
            return GameObject.Instantiate(prefab, AwayPosition, Quaternion.identity);
        }
        public void Return(GameObject tile)
        {
            if (avaliabilityIndex >= size)
            {
                GameObject.Destroy(tile);
            }
            else
            {
                avaliabilityIndex++;
                avaliableTiles[avaliabilityIndex] = tile;
                tile.transform.position = AwayPosition;
                tile.transform.SetParent(null);
            }
        }
    }
}