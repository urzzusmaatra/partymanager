using Map.MapServices;
using Services;
using System.Collections.Generic;
using UnityEngine;

namespace Map
{
    public class MapDataLoader : MonoBehaviour
    {
        [SerializeField]
        private List<MapDataProvider> dataProviders;

        void Start()
        {
            GameServices.GetService(GetMapService);
        }

        private bool GetMapService(IService service)
        {
            if (service is IMapService mapService)
            {
                PushMapDataIntoService(mapService);
                return true;
            }

            return false;
        }

        private void PushMapDataIntoService(IMapService service)
        {
            foreach (var dataProvider in dataProviders)
            {
                service.SetMapGenerator(dataProvider);
            }
        }
    }
}