﻿using System.Collections.Generic;
using UnityEngine;
using DecisionMaking.Strategy;
using Sirenix.OdinInspector;
using Utility.Logging;

namespace DecisionMaking
{
    public abstract class DecisionComponent : MonoBehaviour
    {
        [SerializeField]
        private List<ActionStrategyProvider> _strategyProviders;
        protected List<ActionStrategy> _strategies;
        public Transform Owner { get; private set; }

        public void InitializeStrategies()
        {
            this._strategies = new List<ActionStrategy>(this._strategyProviders.Count);
            foreach (var strategyProvider in _strategyProviders)
            {
                this._strategies.Add(strategyProvider.Strategy);
            }

            this.Owner = transform;
            foreach (var strategy in _strategies)
            {
                strategy.Setup(this);
                strategy.InitializeStrategy();
            }
        }

        public void ExecuteStrategies()
        {
            foreach (var strategy in _strategies)
            {
                if (!strategy.IsDormant)
                {
                    strategy.ExecuteStrategy();
                }
            }
        }

        private void OnValidate()
        {
            Validate();
        }

        [Button("Validate")]
        private void Validate()
        {
            if (_strategyProviders != null)
            {
                foreach (var strategy in _strategyProviders)
                {
                    if (strategy == null) continue;

                    foreach (System.Type type in strategy.GetRequredComponents)
                    {
                        if (!TryGetComponent(type, out Component component))
                        {
                            Log.LogBaseError(string.Format("Strategy {0} required component of type {1} to work.", strategy, type));
                        }
                    }
                }
            }
        }
    }
}