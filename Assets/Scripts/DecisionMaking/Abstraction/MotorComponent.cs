﻿namespace DecisionMaking
{
    //motor happens every update
    public class MotorComponent : DecisionComponent
    {
        private void Start()
        {
            InitializeStrategies();
        }

        private void Update()
        {
            ExecuteStrategies();
        }
    }
}