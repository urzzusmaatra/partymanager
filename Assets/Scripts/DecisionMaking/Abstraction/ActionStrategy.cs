﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DecisionMaking.Strategy
{
    public abstract class ActionStrategy
    {
        protected DecisionComponent owner;

        public bool IsDormant { get; protected set; }

        public void Setup(DecisionComponent owner)
        {
            this.owner = owner;
            IsDormant = false;
        }

        public abstract void InitializeStrategy();

        public abstract void ExecuteStrategy();

        protected Transform Transform => this.owner.transform;
    }
}