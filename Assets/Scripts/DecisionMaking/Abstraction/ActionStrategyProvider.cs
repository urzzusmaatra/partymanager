﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DecisionMaking.Strategy
{
    public abstract class ActionStrategyProvider : ScriptableObject
    {
        public abstract ActionStrategy Strategy { get; }

        internal abstract IEnumerable<Type> GetRequredComponents { get; }
    }
}