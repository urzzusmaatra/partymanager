﻿using PofyTools;

namespace DecisionMaking
{
    //brain has a tick
    public class BrainComponent : DecisionComponent
    {
        private float _maxReflexTime = .2f;
        private Timer _reflexTimer;
        private void Awake()
        {
            this._reflexTimer = new Timer("ThinkingTime", this._maxReflexTime);//this should be governed by some property
        }

        private void Start()
        {
            InitializeStrategies();
        }

        private void Update()
        {
            if (this._reflexTimer.IsReady)
            {
                ExecuteStrategies();
                this._reflexTimer.ResetTimer();
            }
        }
    }
}