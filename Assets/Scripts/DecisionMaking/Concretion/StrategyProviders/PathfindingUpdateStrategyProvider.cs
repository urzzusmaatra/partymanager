﻿using ActorBehaviour;
using PMPathfinding;
using System;
using System.Collections.Generic;
using UnityEngine;
using DecisionMaking.Concretion.Strategy;
using DecisionMaking.Strategy;

namespace DecisionMaking.Concretion.Strategy
{
    [CreateAssetMenu(fileName = "PathfindingUpdateStrategyProvider", menuName = "DecisionMaking/ActionStrategy/PathfindingUpdateStrategyProvider")]
    public class PathfindingUpdateStrategyProvider : ActionStrategyProvider
    {
        public override ActionStrategy Strategy => new PathfindingUpdateStrategy();

        internal override IEnumerable<Type> GetRequredComponents => new Type[] { typeof(PathfindingComponent), typeof(MovementComponent) };
    }
}