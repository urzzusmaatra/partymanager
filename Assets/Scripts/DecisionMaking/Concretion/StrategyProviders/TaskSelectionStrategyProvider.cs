﻿using DecisionMaking.Concretion.Strategy.Data;
using System;
using System.Collections.Generic;
using Task;
using UnityEngine;
using DecisionMaking.Strategy;

namespace DecisionMaking.Concretion.Strategy
{
    [CreateAssetMenu(fileName = "TaskSelectionStrategyProvider", menuName = "DecisionMaking/ActionStrategy/TaskSelectionStrategyProvider")]
    public class TaskSelectionStrategyProvider : ActionStrategyProvider
    {
        public override ActionStrategy Strategy => new TaskSelectionStrategy();

        internal override IEnumerable<Type> GetRequredComponents => new Type[] { typeof(TaskManagerComponent), typeof(ITaskDataReceiver) };
    }
}