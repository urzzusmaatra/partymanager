﻿using DecisionMaking.Concretion.Strategy.Data;
using PMPathfinding;
using System;
using System.Collections.Generic;
using UnityEngine;
using DecisionMaking.Concretion.Strategy;
using DecisionMaking.Strategy;

namespace DecisionMaking.Concretion.Strategy
{
    [CreateAssetMenu(fileName = "PathfindingExecutionStrategyProvider", menuName = "DecisionMaking/ActionStrategy/PathfindingExecutionStrategyProvider")]
    public class PathfindingExecutionStrategyProvider : ActionStrategyProvider
    {
        public override ActionStrategy Strategy => new PathfindingExecutionStrategy();

        internal override IEnumerable<Type> GetRequredComponents => new Type[] { typeof(ITargetDataProvider), typeof(IDestinationDataReceiver), typeof(PathfindingComponent), typeof(ITaskDataProvider) };
    }
}