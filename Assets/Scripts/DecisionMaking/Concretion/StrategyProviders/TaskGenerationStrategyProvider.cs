﻿using System;
using System.Collections.Generic;
using Task;
using UnityEngine;
using DecisionMaking.Concretion.Strategy;
using DecisionMaking.Strategy;

namespace DecisionMaking.Concretion.Strategy
{
    [CreateAssetMenu(fileName = "TaskGenerationStrategyProvider", menuName = "DecisionMaking/ActionStrategy/TaskGenerationStrategyProvider")]
    public class TaskGenerationStrategyProvider : ActionStrategyProvider
    {
        public override ActionStrategy Strategy => new TaskGenerationStrategy();

        internal override IEnumerable<Type> GetRequredComponents => new Type[] { typeof(TaskManagerComponent) };
    }
}