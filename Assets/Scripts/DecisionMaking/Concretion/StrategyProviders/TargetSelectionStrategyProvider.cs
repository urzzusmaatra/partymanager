﻿using DecisionMaking.Concretion.Strategy.Data;
using System;
using System.Collections.Generic;
using UnityEngine;
using DecisionMaking.Concretion.Strategy;
using DecisionMaking.Strategy;

namespace DecisionMaking.Concretion.Strategy
{
    [CreateAssetMenu(fileName = "TargetSelectionStrategyProvider", menuName = "DecisionMaking/ActionStrategy/TargetSelectionStrategyProvider")]
    public class TargetSelectionStrategyProvider : ActionStrategyProvider
    {
        public override ActionStrategy Strategy => new TargetSelectionStrategy();

        internal override IEnumerable<Type> GetRequredComponents => new Type[] { typeof(ITaskDataProvider), typeof(ITargetDataReceiver) };
    }
}