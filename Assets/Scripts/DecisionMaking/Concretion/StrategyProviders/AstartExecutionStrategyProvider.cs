using DecisionMaking.Concretion.Strategy.Data;
using System;
using System.Collections.Generic;
using UnityEngine;
using DecisionMaking.Concretion.Strategy;
using DecisionMaking.Strategy;
using Pathfinding;

namespace DecisionMaking.Concretion.Strategy
{
    [CreateAssetMenu(fileName = "AstartExecutionStrategyProvider", menuName = "DecisionMaking/ActionStrategy/AstartExecutionStrategyProvider")]
    public class AstartExecutionStrategyProvider : ActionStrategyProvider
    {
        public override ActionStrategy Strategy => new AstarExecutionStrategy();

        internal override IEnumerable<Type> GetRequredComponents => new Type[] { typeof(ITargetDataProvider), typeof(IAstarAI), typeof(ITaskDataProvider) };
    }
}