﻿using UnityEngine;
using System;
using System.Collections.Generic;
using DecisionMaking.Concretion.Strategy.Data;
using DecisionMaking.Strategy;

namespace DecisionMaking.Concretion.Strategy
{
    [CreateAssetMenu(fileName = "ExecuteAbilityStrategyProvider", menuName = "DecisionMaking/ActionStrategy/ExecuteAbilityStrategyProvider")]
    public class ExecuteAbilityStrategyProvider : ActionStrategyProvider
    {
        public override ActionStrategy Strategy => new ExecuteAbilityStrategy();

        internal override IEnumerable<Type> GetRequredComponents => new Type[] { typeof(ITargetDataProvider) };
    }
}