﻿using ActorBehaviour;
using DecisionMaking.Concretion.Strategy.Data;
using System;
using System.Collections.Generic;
using UnityEngine;
using DecisionMaking.Strategy;

namespace DecisionMaking.Concretion.Strategy
{
    [CreateAssetMenu(fileName = "MovementMotorStrategyProvider", menuName = "DecisionMaking/ActionStrategy/MovementMotorStrategyProvider")]
    public class MovementMotorStrategyProvider : ActionStrategyProvider
    {
        public override ActionStrategy Strategy => new MovementMotorStrategy();

        internal override IEnumerable<Type> GetRequredComponents => new Type[] { typeof(MovementComponent), typeof(IDestinationDataProvider) };
    }
}