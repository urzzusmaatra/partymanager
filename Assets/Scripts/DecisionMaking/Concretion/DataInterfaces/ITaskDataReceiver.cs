﻿using Task;

namespace DecisionMaking.Concretion.Strategy.Data
{
    public interface ITaskDataReceiver
    {
        void SetTask(AITask task);
    }
}