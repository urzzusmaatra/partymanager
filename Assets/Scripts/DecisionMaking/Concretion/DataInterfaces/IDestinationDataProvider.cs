﻿using UnityEngine;

namespace DecisionMaking.Concretion.Strategy.Data
{
    public interface IDestinationDataProvider
    {
        Vector3 DestinationPosition { get; }
    }
}