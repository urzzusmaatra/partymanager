﻿using UnityEngine;

namespace DecisionMaking.Concretion.Strategy.Data
{
    public interface IDestinationDataReceiver
    {
        void SetDestination(Vector3 destination);

    }
}