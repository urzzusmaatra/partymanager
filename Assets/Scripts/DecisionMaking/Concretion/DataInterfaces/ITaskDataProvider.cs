﻿using Task;

namespace DecisionMaking.Concretion.Strategy.Data
{
    public interface ITaskDataProvider
    {
        AITask GetTask { get; }
    }

}