﻿using UnityEngine;

namespace DecisionMaking.Concretion.Strategy.Data
{
    public interface ITargetDataReceiver
    {
        void SetTargetPosition(Vector3 position);
        void SetTargetObject(Transform target);

        void RemoveTarget();
    }
}