﻿using UnityEngine;

namespace DecisionMaking.Concretion.Strategy.Data
{
    public interface ITargetDataProvider
    {
        bool HasTarget { get; }
        Vector3 TargetPosition { get; }
    }
}