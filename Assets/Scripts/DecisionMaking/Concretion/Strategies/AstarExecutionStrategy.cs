using Pathfinding;
using UnityEngine;
using DecisionMaking.Concretion.Strategy.Data;
using DecisionMaking.Strategy;
public class AstarExecutionStrategy : ActionStrategy
{
    private ITargetDataProvider targetDataProvider;
    private ITaskDataProvider _taskDataProvider;
    private IAstarAI _pathfinding;

    public override void InitializeStrategy()
    {
        this.targetDataProvider = Transform.GetComponent<ITargetDataProvider>();
        this._taskDataProvider = Transform.GetComponent<ITaskDataProvider>();
        this._pathfinding = Transform.GetComponent<IAstarAI>();
    }

    public override void ExecuteStrategy()
    {
        if (targetDataProvider.HasTarget)
        {
            ///TODO: Change responsibility to just make path to destination, not to choose when to stop
            ///allocate that responsibility to destination filter
            ///add responsibility to recalculate if something changes
            //a bit rudimentary, maybe we just stop moving or something...
            var task = _taskDataProvider.GetTask;
            if (task != null && task.IsPositionInActivationRange(targetDataProvider.TargetPosition))//TODO: should we somehow report actor idling when not having task?
            {
                if (!_pathfinding.isStopped)
                {
                    _pathfinding.destination = owner.transform.position;
                }
                _pathfinding.isStopped = true;
            }
            else
            {
                _pathfinding.isStopped = false;
                _pathfinding.destination = targetDataProvider.TargetPosition;
            }
        }
    }
}