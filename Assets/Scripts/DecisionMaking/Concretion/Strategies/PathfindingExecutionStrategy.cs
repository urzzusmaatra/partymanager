﻿using UnityEngine;
using PMPathfinding;
using DecisionMaking.Concretion.Strategy.Data;
using DecisionMaking.Strategy;
public class PathfindingExecutionStrategy : ActionStrategy
{
    private ITargetDataProvider _targetDataProvider;
    private ITaskDataProvider _taskDataProvider;
    private IDestinationDataReceiver _patfindingDataReceiver;
    private PathfindingComponent _pathfinding;

    public override void InitializeStrategy()
    {
        this._patfindingDataReceiver = owner.GetComponent<IDestinationDataReceiver>();
        this._targetDataProvider = owner.GetComponent<ITargetDataProvider>();
        this._pathfinding = owner.GetComponent<PathfindingComponent>();
        this._taskDataProvider = Transform.GetComponent<ITaskDataProvider>();
    }

    public override void ExecuteStrategy()
    {
        if (this._targetDataProvider.HasTarget)
        {
            if (this._taskDataProvider.GetTask.IsPositionInActivationRange(owner.transform.position))
            {
                this._patfindingDataReceiver.SetDestination(owner.transform.position);
            }
            else
            {
                //var approachRange = this._taskDataProvider.GetTask.Ability.ActivationRange;
                //var targetPoint = this._targetDataProvider.TargetPosition;
                //var offset = (Transform.position - targetPoint).normalized * approachRange;

                this._pathfinding.CreatePath(owner.transform.position, this._targetDataProvider.TargetPosition);

                this._patfindingDataReceiver.SetDestination(this._pathfinding.GetPoint);
            }
        }
    }
}