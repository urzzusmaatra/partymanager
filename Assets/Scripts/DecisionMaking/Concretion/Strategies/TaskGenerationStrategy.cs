﻿using Task;
using DecisionMaking.Strategy;

public class TaskGenerationStrategy : ActionStrategy
{
    private TaskManagerComponent _taskComponenet;

    public override void InitializeStrategy()
    {
        this._taskComponenet = owner.GetComponent<TaskManagerComponent>();
    }

    public override void ExecuteStrategy()
    {
        this._taskComponenet.GenerateTasks();
    }
}
