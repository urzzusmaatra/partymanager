﻿using DecisionMaking.Concretion.Strategy.Data;
using DecisionMaking.Strategy;
public class ExecuteAbilityStrategy : ActionStrategy
{
    private ITaskDataProvider _taskData;

    public override void InitializeStrategy()
    {
        this._taskData = owner.GetComponent<ITaskDataProvider>();
    }

    public override void ExecuteStrategy()
    {
        var task = this._taskData.GetTask;

        if (task != null && task.CanExecuteTask())
        {
            task.ExecuteTask();
        }
    }
}