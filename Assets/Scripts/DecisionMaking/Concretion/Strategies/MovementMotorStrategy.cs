﻿using ActorBehaviour;
using DecisionMaking.Concretion.Strategy.Data;
using DecisionMaking.Strategy;
public class MovementMotorStrategy : ActionStrategy
{
    private MovementComponent _movement;
    private IDestinationDataProvider _destinationProvider;

    public override void InitializeStrategy()
    {

        this._movement = owner.GetComponent<MovementComponent>();
        this._destinationProvider = owner.GetComponent<IDestinationDataProvider>();
    }

    public override void ExecuteStrategy()
    {
        this._movement.DestinationPosition = this._destinationProvider.DestinationPosition;
        this._movement.Move();
    }
}