﻿using DecisionMaking.Concretion.Strategy.Data;
using DecisionMaking.Strategy;
public class TargetSelectionStrategy : ActionStrategy
{
    private ITaskDataProvider _taskDataProvider;
    private ITargetDataReceiver _targetDataProvider;

    public override void InitializeStrategy()
    {
        this._taskDataProvider = this.Transform.GetComponent<ITaskDataProvider>();
        this._targetDataProvider = this.Transform.GetComponent<ITargetDataReceiver>();
    }

    public override void ExecuteStrategy()
    {
        var task = this._taskDataProvider.GetTask;
        if (task != null && task.IsValid())
        {
            this._targetDataProvider.SetTargetObject(task.Target.transform);
        }
        else
        {
            this._targetDataProvider.RemoveTarget();
        }
    }
}
