﻿using Task;
using DecisionMaking.Concretion.Strategy.Data;
using DecisionMaking.Strategy;
public class TaskSelectionStrategy : ActionStrategy
{
    private TaskManagerComponent _taskComponenet;
    private ITaskDataReceiver _taskDataReceiver;

    public override void InitializeStrategy()
    {
        this._taskComponenet = owner.GetComponent<TaskManagerComponent>();
        this._taskDataReceiver = this.Transform.GetComponent<ITaskDataReceiver>();
    }

    public override void ExecuteStrategy()
    {
        foreach (var task in this._taskComponenet.GetAllTasks)
        {
            if (task.IsValid())
            {
                this._taskDataReceiver.SetTask(task);
                return;
            }
        }
    }
}