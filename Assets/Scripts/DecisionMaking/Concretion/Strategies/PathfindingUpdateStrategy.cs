﻿using PMPathfinding;
using ActorBehaviour;
using DecisionMaking.Strategy;
public class PathfindingUpdateStrategy : ActionStrategy
{
    private PathfindingComponent _pathfinding;
    private MovementComponent _movementComponent;

    public override void InitializeStrategy()
    {
        this._pathfinding = owner.GetComponent<PathfindingComponent>();
        this._movementComponent = owner.GetComponent<MovementComponent>();
    }

    public override void ExecuteStrategy()
    {
        this._pathfinding.UpdatePath(owner.transform.position, this._movementComponent.CurrentMovementSpeed);
    }
}