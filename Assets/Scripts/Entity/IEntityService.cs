﻿using System.Collections.Generic;
using PartyManager.Assets.Scripts.ActorEntity;
using Services;

namespace Scripts.Entity
{
    public interface IEntityService : IService
    {
        bool IntroduceActor(Actor actor);
        void RemoveActor(Actor actor);

        IReadOnlyCollection<Actor> GetActiveActors { get; }
        Actor CreateActor();
        Actor CreateActor(IActorConstructionData constructionData);
    }
}