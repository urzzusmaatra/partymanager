﻿using Extensions;
using System.Collections.Generic;
using UnityEngine;
using FrostSnail.Events;
using PartyManager.Assets.Scripts.Entity.Events;
using PartyManager.Assets.Scripts.EventChannels;
using PartyManager.Assets.Scripts.ActorEntity;

namespace Scripts.Entity
{
    public class EntityService : IEntityService
    {
        private List<Actor> _actors;
        private Actor _actorPrefab;
        public EntityService(Actor prefab)
        {
            _actors = new List<Actor>();
            _actorPrefab = prefab;
        }

        public static IEntityService CreateBasicService(Actor prefab) => new EntityService(prefab);//insted of providing prefab, create a factory to create actors an pass data to it, then add it to this service

        public IReadOnlyCollection<Actor> GetActiveActors => _actors;

        public bool IntroduceActor(Actor actor)
        {
            if (_actors.AddOnce(actor))
            {
                EventFactory.GetEvent(out ActorAddedEvent myEvent);
                myEvent.Actor = actor;
                myEvent.SendTo(GameplayEventChannel.Channel);

                return true;
            }

            return false;
        }

        public void RemoveActor(Actor actor)
        {
            if (_actors.Remove(actor))
            {
                EventFactory.GetEvent(out ActorRemovedEvent myEvent);
                myEvent.Actor = actor;
                myEvent.SendTo(GameplayEventChannel.Channel);
            }
        }
        public Actor CreateActor(IActorConstructionData constructionData)
        {
            var actor = CreateActor();
            constructionData.SetupActor(actor);
            return actor;
        }

        public Actor CreateActor() => Object.Instantiate(_actorPrefab);
    }
}