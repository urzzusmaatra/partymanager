using UnityEngine;
using Services;
using Services.Internal;
using PartyManager.Assets.Scripts.ActorEntity;

namespace Scripts.Entity
{
    public class EntityServiceProvider : MonoBehaviour
    {
        [SerializeField]
        private Actor _actorPrefab;

        private void Awake()
        {
            GameServices.Provide(ServicesConfiguration.GetEntityService(_actorPrefab));
        }
    }

}