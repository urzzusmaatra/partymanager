﻿using FrostSnail.Events;
using PartyManager.Assets.Scripts.ActorEntity;

namespace PartyManager.Assets.Scripts.Entity.Events
{
    public class ActorRemovedEvent : EventData
    {
        public Actor Actor { get; set; }
    }
}
