﻿using UnityEngine;
using Services;
using Services.Internal;

namespace FXServices
{
    public class FXServicesProviderComponent : MonoBehaviour
    {
        void Start()
        {
            GameServices.Provide(ServicesConfiguration.GetFXService());
        }
    }
}