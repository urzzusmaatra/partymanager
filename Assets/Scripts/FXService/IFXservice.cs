using UnityEngine;
using Services;

namespace FXServices
{
    public interface IFXService : IService
    {
        FXObjectComponent GetObject(FXObjectComponent prefab);
    }

    public class FXService : IFXService
    {
        private static FXService _FXService;
        internal static IFXService CreateBasicService()
        { 
            if(_FXService == null)
            {
                _FXService = new FXService();
            }
            return _FXService;
        }

        public FXObjectComponent GetObject(FXObjectComponent prefab)
        {
            //expand usage here
            return GameObject.Instantiate(prefab);
        }

    }

    public class FXObjectComponent : MonoBehaviour
    {

    }

    public class FXClientComponent : MonoBehaviour
    {

    }
}