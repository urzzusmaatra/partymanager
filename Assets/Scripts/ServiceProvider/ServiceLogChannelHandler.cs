﻿using Utility.Logging.Internal;

namespace Services
{
    public class ServiceLogChannelHandler : ChannelHandler
    {
        public override string ChannelName => ServiceLogChannel.NAME;
    }

    public class ServiceLogChannel
    {
        public const string NAME = "ServiceChannel";
    }
}