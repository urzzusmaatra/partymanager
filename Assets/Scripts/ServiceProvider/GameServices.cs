﻿using System.Collections.Generic;
using FrostSnail.Events;

namespace Services
{
    //should all services need to be IService of some generic type?
    //is there a need to introduce mandatory services and optional? 
    //does that dictate when the game is ready to be played?
    public static class GameServices
    {
        public class ServiceException : System.Exception
        {
            public ServiceException(string message) : base(message) { }
        }

        private static List<IService> _services;

        public static void Provide(IService service)
        {
            Initialize();

            if (_services.Contains(service))
                throw new ServiceException("Service already provided!");

            _services.Add(service);
            OnServiceAdded?.Invoke(service);

            ProcessSubscribers(service);
        }

        public static T GetService<T>()
        {
            Initialize();

            T foundService = default(T);
            foreach (var service in _services)
            {
                if (service is T)
                {
                    foundService = (T)service;
                }
            }

            return foundService;
        }

        public delegate void ServiceAdded(object service);
        public static ServiceAdded OnServiceAdded;

        private static bool isInitialized = false;
        private static void Initialize()
        {
            if (!isInitialized)
            {
                _services = new List<IService>();
                isInitialized = true;
            }
        }

        //TEST NEEDED TO SEE WHICH OF THE TREE ARE BEST
        
        public delegate bool ServiceSubscription(IService service);
        private static ServiceSubscription subscribers;
        public static void GetService(ServiceSubscription subscriptionMethod)
        {
            Initialize();

            foreach (var service in _services)
            {
                if (subscriptionMethod.Invoke(service))
                {
                    return;
                }
            }

            subscribers += subscriptionMethod;
        }

        private static void ProcessSubscribers(IService service)
        {
            if (subscribers == null) return;

            foreach (ServiceSubscription subscriber in subscribers.GetInvocationList())
            {
                if (subscriber(service))
                {
                    subscribers -= subscriber;
                }
            }
        }
    }

    public interface IService
    {
        //if this proves to eventualy be important
        //IEventChannel GetChannel { get; }
    }
}