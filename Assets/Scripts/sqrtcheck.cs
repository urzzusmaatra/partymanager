﻿using PofyTools;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Utility.Logging;
public class sqrtcheck : MonoBehaviour
{
    public List<float> numbers;


    [Button(DisplayParameters = true, Name = "Populate")]
    public void PopulateAtRandom(int count)
    {
        numbers = new List<float>();
        for (int i = 0; i < count; i++)
        {
            numbers.Add(Random.value * 1000f);
        }
    }

    [Button(Name = "Test")]
    public void Check()
    {
        float memNumber;
        Stopwatch stopwatch = Stopwatch.StartNew();
        foreach (var number in this.numbers)
        {
            memNumber = Mathf.Pow(number, 2f);
            memNumber = Mathf.Pow(number, 2f);
        }
        stopwatch.Stop();
        var time = stopwatch.ElapsedMilliseconds;
        stopwatch.Restart();
        foreach (var number in this.numbers)
        {
            memNumber = Mathf.Sqrt(number);
        }
        stopwatch.Stop();
        var secondTime = stopwatch.ElapsedMilliseconds;
        stopwatch.Restart();
        foreach (var number in this.numbers)
        {
            memNumber = number * number;
            memNumber = number * number;
        }

        stopwatch.Stop();
        var thirdTime = stopwatch.ElapsedMilliseconds;

        Log.LogBaseMessage("First: " + time);
        Log.LogBaseMessage("Second: " + secondTime);
        Log.LogBaseMessage("Third: " + thirdTime);
    }
}
