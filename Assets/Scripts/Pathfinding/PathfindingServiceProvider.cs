using UnityEngine;
using Services.Internal;

namespace Services
{
    public class PathfindingServiceProvider : MonoBehaviour
    {
        void Start()
        {
            GameServices.Provide(ServicesConfiguration.GetPathfindingService());
        }

    }
}