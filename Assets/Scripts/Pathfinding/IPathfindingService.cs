﻿using UnityEngine;
using PMPathfinding.Internal;

namespace Services
{
    public interface IPathfindingService : IService
    {
        bool CreatePath(Vector3 currentPosition, Vector3 destination, ref Path currentPath);
    }
}
