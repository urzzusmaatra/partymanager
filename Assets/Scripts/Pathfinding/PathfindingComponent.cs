﻿using Extensions;
using Services;
using UnityEngine;
using PMPathfinding.Internal;
using Utility.Logging;

namespace PMPathfinding
{
    public class PathfindingComponent : MonoBehaviour
    {
        private IPathfindingService _pathfidingService = null;
        private Path _currentPath;
#if UNITY_EDITOR
        [SerializeField]
        private bool _drawPath = false;
#endif
        private void Awake()
        {
            GameServices.GetService(ReceiveService);
            this._currentPath = new Path();
        }

        public void CreatePath(Vector3 currentPosition, Vector3 destination)
        {
            if (_currentPath.IsNewPathRequired(destination))
            {
                if (!_pathfidingService.CreatePath(currentPosition, destination, ref _currentPath))
                {
                    Log.LogBaseError(string.Format("Unable to find path from {0} to {1}", currentPosition, destination));
                }
            }
        }

        public void UpdatePath(Vector3 currentPosition, float velocity)
        {
            this._currentPath.UpdatePath(currentPosition, velocity);
        }

        public Vector3 GetPoint => this._currentPath.GetNextPoint;

        private bool ReceiveService(IService service)
        {
            if (service is IPathfindingService pathfindingService)
            {
                this._pathfidingService = pathfindingService;
                return true;
            }
            return false;
        }

        private void OnDrawGizmos()
        {
#if UNITY_EDITOR
            if (this._drawPath)
#endif
            {
                if (this._currentPath != null)
                {
                    this._currentPath.Draw();
                }
            }
        }
    }
}