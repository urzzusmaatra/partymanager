﻿using UnityEngine;
using PMPathfinding.Internal;
using Services;
using System;
using FrostSnail.Events;

namespace Services.Internal
{
    public class PathfindingService : IPathfindingService
    {
        public bool CreatePath(Vector3 currentPosition, Vector3 destination, ref Path currentPath)
        {
            //rudimentary for now
            float distance = (destination - currentPosition).magnitude;
            Vector3 direction = (destination - currentPosition).normalized;
            int pointsCount = Mathf.CeilToInt(distance);//experimenting, so 1 distance for each point

            currentPath.AddPoint(destination, 0);

            for (int i = 0; i < pointsCount - 1; i++)
            {
                var point = currentPosition + (direction * (pointsCount - i - 1));
                currentPath.AddPoint(point, i + 1);
            }

            currentPath.FinalizePath(pointsCount);
            return true;
        }

        internal static IPathfindingService CreateBasicService() => new PathfindingService();

    }
}
