﻿using Extensions;
using UnityEngine;

namespace PMPathfinding.Internal
{
    public class Path
    {
        public const int MAX_POINT_COUNT = 64;
        private const float _ASSUMED_VELOCITY = 1f / 30f;
        private Vector3[] points;
        private int _pathPointsCount;
        private int _currentPoint;

        public Path()
        {
            this.points = new Vector3[MAX_POINT_COUNT];
            this._pathPointsCount = 0;
            this._currentPoint = 0;
        }

        internal void UpdatePath(Vector3 currentPosition, float velocity)
        {
            if (_currentPoint == 0)
                return;

            if (IsPointClose(points[_currentPoint], currentPosition, velocity))
            {
                _currentPoint--;
            }
        }

        internal Vector3 GetNextPoint => this.points[_currentPoint];

        internal bool IsNewPathRequired(Vector3 destination)
        {
            return !IsPointClose(points[0], destination, _ASSUMED_VELOCITY);

        }

        internal void AddPoint(Vector3 point, int place)
        {
            points[place] = point;
        }

        internal void Draw()
        {
            Gizmos.color = Color.gray;
            Gizmos.DrawSphere(points[0], .05f);
            for (int i = 1; i <= this._currentPoint; i++)
            {
                var point = points[i];
                var nextPoint = points[i - 1];

                Gizmos.DrawWireCube(point, Vector3.one * .5f);
                Gizmos.DrawLine(point, nextPoint);
                Gizmos.DrawSphere(point, .05f);
            }
        }

        internal void FinalizePath(int pointsCount)
        {
            this._pathPointsCount = pointsCount;
            this._currentPoint = pointsCount -1;
        }

        private bool IsPointClose(Vector3 point, Vector3 destination, float velocity)
        {
            return (destination - point).sqrMagnitude <= velocity * velocity;
        }
    }
}