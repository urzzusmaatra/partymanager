﻿using PMPathfinding.Internal;
using Services;
using Services.Internal;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfindingTestComponent : MonoBehaviour
{
    [SerializeField] private Transform _start;
    [SerializeField] private Transform _end;
    [SerializeField] private Transform _movableObject;
    [SerializeField] private float _movementSpeed;
    private Path _path;
    private IPathfindingService _pathfindingService;

    private void Awake()
    {
        this._pathfindingService = PathfindingService.CreateBasicService();
    }
    [Button("Create Path")]
    private void CreatePath()
    {
        if (this._path == null) this._path = new Path();

        this._pathfindingService.CreatePath(this._start.position, this._end.position, ref _path);

    }
    private bool _navigate = false;

    [Button("Toggle Navigation")]
    private void StartNavigating()
    {
        this._navigate = !this._navigate;

        if(this._navigate)
        {
            this._movableObject.position = this._start.position;
        }
    }

    private void Update()
    {
        if(this._navigate)
        {
            this._movableObject.position += (this._path.GetNextPoint - this._movableObject.position).normalized * Time.deltaTime * this._movementSpeed;
            this._path.UpdatePath(this._movableObject.position, Time.deltaTime * this._movementSpeed);
        }
    }



    private void OnDrawGizmos()
    {
        if(this._path != null)
        {
            this._path.Draw();
        }
    }
}
