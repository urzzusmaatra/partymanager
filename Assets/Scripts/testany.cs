﻿using UnityEngine;

public class testany : MonoBehaviour
{
    [Range(0f,1f)]
    [SerializeField]
    private float fill = 0f;
    [SerializeField]
    private Transform guide;
    [SerializeField]
    private Transform cosTransform;
    [SerializeField]
    private Transform sinTransform;
    [SerializeField]
    private bool autoFill = false;
    [Range(0f,1f)]
    [SerializeField]
    private float autofillSpeed = 1f;
    private void Update()
    {
        if (autoFill)
            CalculateFill();

        var cos = Vector3.right * Mathf.Cos(Mathf.PI * 2f * fill);
        var sin = Vector3.up * Mathf.Sin(Mathf.PI * 2f * fill);

        SetTransformPosition(guide, cos + sin);
        SetTransformPosition(cosTransform, cos);
        SetTransformPosition(sinTransform, sin);


        dot = Vector2.Dot(one, two);
    }

    private void CalculateFill()
    {
        fill += Time.deltaTime * autofillSpeed;
        if (fill >= 1f)
            fill -= 1f;
    }

    private void SetTransformPosition(Transform transform, Vector3 position)
    {
        if (transform != null)
            transform.position = position;
    }

    public float dot;

    public Vector2 one, two;
}
