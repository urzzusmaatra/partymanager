public class LayerData 
{
    public const int ACTOR_LAYER = 1 << 8;
    public const int WEAPON_LAYER = 1 << 9;
    public const int DAMAGABLE_LAYER = 1 << 10;
}
