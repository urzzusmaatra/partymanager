using UnityEngine;
using Services.Internal;
namespace Services
{
    public class TaskServiceProvider : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            GameServices.Provide(ServicesConfiguration.GetTaskService());
        }

    }
}