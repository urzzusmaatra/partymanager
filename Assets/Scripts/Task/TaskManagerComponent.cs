﻿using AbilitySpace.Internal;
using PartyManager.Assets.Scripts.ActorEntity;
using Scripts.Entity;
using Services;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utility.Logging;

namespace Task
{
    [RequireComponent(typeof(AbilityCollectionComponent))]
    public class TaskManagerComponent : MonoBehaviour
    {
        private const int MAX_TASK_NUMBER = 128;
        private List<AITask> taskList;
        private AbilityCollectionComponent abilityCollection;//#techdebt not shure about this coupling
        private IEntityService entitiyService;
        private ITaskService taskService;
        
        private bool IsInitialized => this.entitiyService != null && this.taskService != null;
        private void Awake()
        {
            this.taskList = new List<AITask>(MAX_TASK_NUMBER);
            GameServices.GetService(ReceiveEntityService);
            GameServices.GetService(ReceiveTaskService);
        }

        private void Start()
        {
            this.abilityCollection = GetComponent<AbilityCollectionComponent>();
        }

        public void GenerateTasks()
        {
            if (!CanGenerateTasks) return;

            int taskCount = 0;

            foreach (var target in this.entitiyService.GetActiveActors)
            {
                foreach (Ability ability in this.abilityCollection.GetAllAbilities)
                {
                    if (ability.IsTargetValid(target.transform))
                    {
                        var task = CreateTask(target, ability);
                        AddTaskToList(task, taskCount);
                        IncrementTaskIndex(ref taskCount);
                    }
                }
            }

            RemoveExcessTasks(taskCount);
            taskList.OrderBy(t => t.Priority);
        }

        private AITask CreateTask(Actor target, Ability ability)
        {
            var task = this.taskService.GetTask;

            task.SetupTask(target, ability, GetComponent<Actor>());

            task.OnTaskBecameInvalid += OnTaskBecameInvalid;

            return task;
        }

        private void AddTaskToList(AITask task, int taskIndex)
        {
            if(taskIndex >= taskList.Count)
            {
                taskList.Add(task);
            }
            else
            {
                taskList[taskIndex] = task;
            }
        }

        private void IncrementTaskIndex(ref int taskIndex)
        {
            taskIndex++;

            if (taskIndex == MAX_TASK_NUMBER)
            {
                Log.LogBaseError($"Tasks exceeding max number of {MAX_TASK_NUMBER}!");
                taskIndex--;
            }
        }

        private void RemoveExcessTasks(int latestTaskIndex)
        {
            for (int i = this.taskList.Count - 1; i > latestTaskIndex - 1; i--)
            {
                var task = this.taskList[i];
                task.OnTaskBecameInvalid -= OnTaskBecameInvalid;
                this.taskService.ReturnTask(task);
                this.taskList.RemoveAt(i);
            }
        }

        public bool CanGenerateTasks => IsInitialized && this.abilityCollection.HasAbilities;

        public IReadOnlyCollection<AITask> GetAllTasks => this.taskList;

        private bool ReceiveEntityService(IService service)
        {
            return (this.entitiyService = service as IEntityService) != null;
        }

        private bool ReceiveTaskService(IService service)
        {
            return (this.taskService = service as ITaskService) != null;
        }

        private void OnTaskBecameInvalid(AITask task)
        {
            task.OnTaskBecameInvalid -= OnTaskBecameInvalid;
            this.taskService.ReturnTask(task);
            this.taskList.Remove(task);
        }

    }

}