﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Task;
using Extensions;

namespace Services.Internal
{
    public class TaskService : ITaskService
    {
        private List<AITask> _taskPool = null;

        public TaskService()
        {
            _taskPool = new List<AITask>();
        }

        public static ITaskService CreateBasicService() => new TaskService();

        public void ReturnTask(AITask task)
        {
            task.Invalidate();
            this._taskPool.Add(task);
        }

        public AITask GetTask 
        {
            get
            {
                var taskCount = this._taskPool.Count;
                if(taskCount == 0)
                {
                    return new AITask();
                }

                var task = this._taskPool[taskCount - 1];
                this._taskPool.RemoveAt(taskCount - 1);
                return task;
            }
        }

    }
}