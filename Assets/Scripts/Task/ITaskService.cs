﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Task;

namespace Services
{
    public interface ITaskService : IService
    {
        AITask GetTask { get; }

        void ReturnTask(AITask task);
    }
}