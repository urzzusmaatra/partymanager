﻿using AbilitySpace;
using AbilitySpace.Internal;
using Extensions;
using PofyTools;
using Sirenix.Utilities;
//using System;
using System.Collections;
using UnityEngine;
using FrostSnail.Events;
using Utility.Logging;
using FrostSnail.Events.Internal;
using PartyManager.Assets.Scripts.ActorEntity;

namespace Task
{
    [System.Serializable]
    public class AITask
    {
        private Actor target;
        private Ability ability;
        private bool isValid = false;
        private Actor owner;
        private EventReceiver eventHandler;

        public void SetupTask(Actor target, Ability ability, Actor owner)
        {
            if(this.isValid)
            {
                Invalidate();//we must do this to clean things up
            }

            this.owner = owner;
            this.target = target;
            this.ability = ability;
            this.isValid = true;

            this.target.OnActorRemoved += OnTargetRemoved;

            RegisterAbilityEventsCallback();
        }

        public int Priority { get; } //calculate this somehow

        public Actor Target => this.target;
        public Ability Ability => this.ability;
        public Actor Owner => owner;

        public bool CanExecuteTask()
        {
            if (!isValid)
                return false;

            if (!IsPositionInActivationRange(Target.transform.position))
                return false;

            if (!ability.IsActivatable())
                return false;

            return true;
        }

        public bool IsPositionInActivationRange(Vector3 position)
        {
            return Ability.IsDistanceInRange(Owner.transform.position.Distance(position));
        }

        public void ExecuteTask() => this.Ability.Activate(Owner.transform, Target.transform);

        internal bool IsValid()
        {
            return Target != null && Ability != null && this.isValid;
        }

        private void RegisterAbilityEventsCallback()
        {
            if (EventReceiverFactory.CreateEventHandler(out eventHandler, ability.gameObject))
            {
                eventHandler.RegisterMethod<AbilityRemovedEvent>(OnAbilityRemovedCallback);
            }
            else
            {
                Log.LogBaseError($"Failed to create ability {ability} event handler in task {this}");
            }
        }

        private void UnregisterAbilityEventsCallbacks()
        {
            if (eventHandler != null)
            {
                eventHandler.UnregisterMethod<AbilityRemovedEvent>(OnAbilityRemovedCallback);
                if (ability != null)
                {
                    eventHandler.UnregisterComponent(ability.GetComponent<EventChannelComponent>());
                }
            }
        }

        private void OnAbilityRemoved(Ability ability)
        {
            if (this.ability == ability)
            {
                Invalidate();
            }
            else
            {
                UnregisterAbilityEventsCallbacks();
            }
        }

        private void OnAbilityRemovedCallback(AbilityRemovedEvent abilityEvent)
        {
            OnAbilityRemoved(abilityEvent.Ability as Ability);
        }

        public void OnTargetRemoved(Actor actor)
        {
            if (this.target == actor)
            {
                Invalidate();
            }
            else
            {
                actor.OnActorRemoved -= OnTargetRemoved;
            }
        }

        public void Invalidate()
        {
            this.isValid = false;

            UnregisterAbilityEventsCallbacks();

            if (this.target != null)
            {
                this.target.OnActorRemoved -= OnTargetRemoved;
            }

            OnTaskBecameInvalid?.Invoke(this);
        }


        public System.Action<AITask> OnTaskBecameInvalid;
    }
}