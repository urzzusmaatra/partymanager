﻿using System.Collections.Generic;
using FrostSnail.Factions;

namespace FactionServices.Internal
{
    public abstract class RelationsExaminer : IRelationExaminer
    {
        protected IFactionCollection factions;
        public abstract void ExamineRelation(Faction faction, Faction other, float relation);

        public void SetCollection(IFactionCollection factions)
        {
            this.factions = factions;
        }
    }
    
}