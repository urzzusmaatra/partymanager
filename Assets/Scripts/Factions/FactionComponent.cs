﻿using UnityEngine;
using FrostSnail.Events;
using Services;
using FactionServices;
using FrostSnail.Factions;
using FrostSnail.Events.Internal;

namespace Factions
{
    public class FactionComponent : MonoBehaviour, IFactionProvider
    {
        [SerializeField]
        private Faction faction = Faction.None;
        [Sirenix.OdinInspector.ReadOnly]
        private string factionName;
        public Faction Faction
        {
            get => faction;
            set
            {
                if (faction != value)
                {
                    faction = value;
                    var factoryEvent = EventFactory.GetEvent<FactionChangedEventData>();
                    factoryEvent.Faction = value;
                    factionName = faction.FactionName;
                    eventComponent.SendEvent(factoryEvent);
                }
            }
        }
        
        private IFactionService factionService;

        private EventChannelComponent eventComponent;

        private void Awake()
        {
            eventComponent = GetComponent<EventChannelComponent>();
            GameServices.GetService(ReceiveService);
        }

        public FactionRelationQueryResult CheckFactionRelations(Faction otherFaction) => factionService.GetFactionReleations(Faction, otherFaction);

        private bool ReceiveService(IService service)
        {
            return (factionService = service as IFactionService) != null;
        }

        internal void SetFaction(string region)
        {
            Faction = factionService.GetFactionFromRegion(region);
        }
    }
}
