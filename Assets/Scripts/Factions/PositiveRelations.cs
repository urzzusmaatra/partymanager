﻿using FrostSnail.Factions;

namespace FactionServices.Internal
{
    public class PositiveRelations : RelationsExaminer
    {
        public override void ExamineRelation(Faction faction, Faction other, float relation)
        {
            if (relation > FactionService.FRIENDLY_RELATIONS_LIMIT)
            {
                factions.AddFaction(other);
            }
        }
    }
}