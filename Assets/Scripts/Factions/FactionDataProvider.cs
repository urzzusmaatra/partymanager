﻿using UnityEngine;
using FrostSnail.Factions;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace FactionServices.Internal
{
    public class FactionDataProvider : MonoBehaviour, IFactionsCreationStrategy, IFactionRelationsCreationStrategy
    {
        [SerializeField]
        private List<FactionData> factionsData;
        private Dictionary<Faction, FactionData> factionsDataStorage;
        public int GetFactionCount => factionsData.Count;

        private int index = 0;
        private const float DEFAULT_RELATION_IMPACT = -1f;
        public void CreateRelations(ReadOnlyCollection<Faction> readOnlyCollection, IFactionsRelationsModifier factionRelations)
        {
            for (int i = 0; i < readOnlyCollection.Count; i++)
            {
                for (int j = 1; j < readOnlyCollection.Count; j++)
                {
                    //TODO: It would be awesome if we could get max/min relations from somewhere
                    //Making them all enemies
                    factionRelations.ModifyRelations(readOnlyCollection[i], readOnlyCollection[j], DEFAULT_RELATION_IMPACT);
                }
            }
        }

        public void SetupFaction(Faction faction)
        {
            faction.FactionName = factionsData[index].name;
            faction.SetStringValue(FactionData.REGION_KEY, factionsData[index].region);
            index++;
        }
    }

    [System.Serializable]
    public struct FactionData
    {
        public string name;
        public string region;

        public const string REGION_KEY = "region";
    }
}
