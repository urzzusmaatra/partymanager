﻿using FrostSnail.Factions;

namespace FactionServices.Internal
{
    public class NegativeRelations : RelationsExaminer
    {
        public override void ExamineRelation(Faction faction, Faction other, float relation)
        {
            if (relation < FactionService.ENEMY_RELATIONS_LIMIT)
            {
                factions.AddFaction(other);
            }
        }
    }
}