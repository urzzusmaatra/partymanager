﻿using FrostSnail.Factions;
using System;

namespace FactionServices.Internal
{
    public class NeutralRelations : RelationsExaminer
    {
        public override void ExamineRelation(Faction faction, Faction other, float relation)
        {
            if (relation <= FactionService.FRIENDLY_RELATIONS_LIMIT && relation >= FactionService.FRIENDLY_RELATIONS_LIMIT)
            {
                factions.AddFaction(other);
            }
        }
    }

    
}