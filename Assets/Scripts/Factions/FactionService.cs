﻿using System.Collections.Generic;
using UnityEngine;
using FrostSnail.Factions;
using System;
using Utility.Logging;

namespace FactionServices.Internal
{
    public class FactionService : IFactionService
    {
        private FactionsManager factionManager;
        
        private RelationsExaminer positiveRelationsFinder = new PositiveRelations();
        private RelationsExaminer negativeRelationsFinder = new NegativeRelations();
        private RelationsExaminer neutralRelationsFinder = new NeutralRelations();

        public FactionService()
        {
            //choose players faction at random for now
            Log.LogBaseMessage("Choose players faction here?");

            FactionManager = new FactionsManager();
        }

        public static IFactionService CreateBasicService() => new FactionService();

        public const float FRIENDLY_RELATIONS_LIMIT = 0f;
        public const float ENEMY_RELATIONS_LIMIT = 0f;

        public FactionsManager FactionManager { get => factionManager; private set => factionManager = value; }

        public FactionRelationQueryResult GetFactionReleations(Faction fristFaction, Faction secondFactions)
        {
            var relations = factionManager.GetFactionRelation(fristFaction, secondFactions);

            if (relations > FRIENDLY_RELATIONS_LIMIT)
                return FactionRelationQueryResult.Friendly;
            else if (relations < ENEMY_RELATIONS_LIMIT)
                return FactionRelationQueryResult.Enemy;
            else
                return FactionRelationQueryResult.Neutral;
        }

        public void GetFactionsWithRelations(FactionRelationQueryResult relations, Faction sampleFaction, IFactionCollection factions)
        {
            switch (relations)
            {
                case FactionRelationQueryResult.Enemy:
                    negativeRelationsFinder.SetCollection(factions);
                    factionManager.ExamineRelations(sampleFaction, negativeRelationsFinder);
                    break;
                case FactionRelationQueryResult.Friendly:
                    positiveRelationsFinder.SetCollection(factions);
                    factionManager.ExamineRelations(sampleFaction, positiveRelationsFinder);
                    break;
                default:
                    neutralRelationsFinder.SetCollection(factions);
                    factionManager.ExamineRelations(sampleFaction, neutralRelationsFinder);
                    break;
            }
        }

        public IFactionCollection CreateCollection() => new FactionCollection(FactionManager);

        public Faction GetFactionFromRegion(string region)
        {
            var factions = FactionManager.CreateFactionCollectionAllFactions();

            foreach (var faction in factions)
            {
                if(faction.GetStringValue(FactionData.REGION_KEY, out string value) && region.Equals(value))
                {
                    return faction;
                }
            }

            return Faction.None;
        }
    }
}