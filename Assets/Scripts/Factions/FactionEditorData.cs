using Services;
using System.Collections;
using UnityEngine;
using FrostSnail.Factions;

[CreateAssetMenu(fileName = "FactionEditorData", menuName = "Faction/EditorData")]
public class FactionEditorData : ScriptableObject
{
    [SerializeField]
    private Faction faction;
    [SerializeField]
    private Color color;

    public Faction Faction { get => faction;}
    public Color Color { get => color;}
}
