﻿using Services;
using FrostSnail.Factions;
using System.Collections.Generic;
using System;

namespace FactionServices
{
    public enum FactionRelationQueryResult
    {
        Enemy,
        Neutral,
        Friendly
    }

    public interface IFactionService : IService
    {
        FactionRelationQueryResult GetFactionReleations(Faction firstId, Faction secondId);
        void GetFactionsWithRelations(FactionRelationQueryResult relations, Faction faction, IFactionCollection factions);
        IFactionCollection CreateCollection();
        Faction GetFactionFromRegion(string region);
    }
}