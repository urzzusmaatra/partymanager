using UnityEngine;
using Services.Internal;
using Services;
using FrostSnail.Factions;

namespace FactionServices.Internal
{
    [RequireComponent(typeof(IFactionRelations))]
    [RequireComponent(typeof(IFactionsCreationStrategy))]
    [RequireComponent(typeof(IFactionRelationsCreationStrategy))]
    public class FactionServiceProvider : MonoBehaviour
    {
        private void Start()
        {
            var iFactionService = ServicesConfiguration.GetFactionService();
            var factionService = (FactionService)iFactionService;
            factionService.FactionManager.RunCreationStrategy(GetComponent<IFactionsCreationStrategy>());
            factionService.FactionManager.RunRelationsStrategy(GetComponent<IFactionRelationsCreationStrategy>());
            GameServices.Provide(iFactionService);
        }
    }
}
