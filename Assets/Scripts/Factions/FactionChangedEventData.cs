﻿using System;
using FrostSnail.Events;
using FrostSnail.Factions;

namespace Factions
{
    public class FactionChangedEventData : EventData
    {
        public Faction Faction { get; set; }
    }
}