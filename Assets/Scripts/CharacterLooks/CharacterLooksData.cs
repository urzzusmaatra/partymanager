﻿using Extensions;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.CharacterLooks
{
    [CreateAssetMenu(fileName = "CharacterLooksData", menuName = "Character/Looks/Data", order = 1)]
    public class CharacterLooksData : ScriptableObject
    {
        [SerializeField]
        private List<Sprite> _sprites = null;

        public Sprite GetRandomLooks => _sprites.GetRandom();

    }
}