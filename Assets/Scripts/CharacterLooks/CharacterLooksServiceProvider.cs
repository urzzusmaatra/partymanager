﻿using Services;
using UnityEngine;
using Services.Internal;

namespace Scripts.CharacterLooks
{
    public class CharacterLooksServiceProvider : MonoBehaviour
    {
        [SerializeField]
        private CharacterLooksData _avaliableCharacters = null;

        private void Start()
        {
            
            var service = ServicesConfiguration.GetCharacterLooksService();
            ((CharacterLooksService)service).ProvideData(_avaliableCharacters);
            GameServices.Provide(service);
        }
    }
}