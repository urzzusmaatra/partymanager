﻿using Scripts.CharacterLooks;
using UnityEngine;

namespace Scripts.CharacterLooks
{
    public class CharacterLooksService : ICharacterLooksService
    {
        public CharacterLooksService Service => this;

        private CharacterLooksData _data;

        public static ICharacterLooksService CreateBasicService()
        {
            return new CharacterLooksService();
        }

        public void ProvideData(CharacterLooksData data)
        {
            _data = data;
        }

        public Sprite GetRandomLooks => _data.GetRandomLooks;
    }
}