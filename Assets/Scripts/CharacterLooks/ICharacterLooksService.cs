﻿using Services;
using Services.Internal;
using UnityEngine;

namespace Scripts.CharacterLooks
{
    public interface ICharacterLooksService : IService
    {
        CharacterLooksService Service { get; }

        Sprite GetRandomLooks { get; }
    }
}