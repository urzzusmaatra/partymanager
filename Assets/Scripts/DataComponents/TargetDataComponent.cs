﻿using UnityEngine;
using DecisionMaking.Concretion.Strategy.Data;

public class TargetDataComponent : MonoBehaviour, ITargetDataProvider, ITargetDataReceiver
{
    private bool _useTransform = true;
    private Vector3 _targetPosition;
    private Transform _targetObject;
    private bool _hasTarget = false;

    public Vector3 TargetPosition
    {
        get
        {
            if(this._targetObject == null)//loosing transform can hurt
            {
                SetTargetPosition(this.transform.position);
            }

            return this._useTransform ? this._targetObject.position : this._targetPosition;
        }
    }

    public bool HasTarget => this._hasTarget;

    public void RemoveTarget()
    {
        this._hasTarget = false;
        this._targetPosition = this.transform.position;
        this._targetObject = null;
    }

    public void SetTargetObject(Transform target)
    {
        this._useTransform = true;
        this._targetObject = target;
        this._hasTarget = true;
    }

    public void SetTargetPosition(Vector3 position)
    {
        this._useTransform = false;
        this._targetPosition = position;
        this._hasTarget = true;
    }

    private Color sphereColor = new Color(.8f, .2f, .2f, .4f);
    private Color wireSphereColor = new Color(.8f, .2f, .2f, .9f);
    private void OnDrawGizmos()
    {
        Gizmos.color = sphereColor;
        Gizmos.DrawSphere(TargetPosition, .2f);
        Gizmos.color = wireSphereColor;
        Gizmos.DrawWireSphere(TargetPosition, .22f);
        Gizmos.DrawLine(this.transform.position, TargetPosition);
    }

    [SerializeField]
    private Transform overrideTarget;

    [Sirenix.OdinInspector.Button("Override target")]
    private void OverrideTarget()
    {
        SetTargetObject(overrideTarget);
    }
}
