﻿using UnityEngine;
using DecisionMaking.Concretion.Strategy.Data;

public class ManualDestinationDataComponent : MonoBehaviour, IDestinationDataProvider
{
    [SerializeField]
    private Transform _destinationObject;
    [SerializeField]
    private float _offset;

    //this offset bullshit should be part of the brain, let it decide when and where target is going. It's not update type of thing.
    private Vector3 _offsetedPosition => (this.transform.position - this._destinationObject.transform.position).normalized * _offset;

    public Vector3 DestinationPosition => this._destinationObject != null ? this._destinationObject.position + _offsetedPosition : transform.position;
}