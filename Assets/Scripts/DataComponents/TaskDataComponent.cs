﻿using UnityEngine;
using DecisionMaking.Concretion.Strategy.Data;
using Task;

public class TaskDataComponent : MonoBehaviour, ITaskDataProvider, ITaskDataReceiver
{
    private AITask _task;

    public AITask GetTask => this._task;

    public void SetTask(AITask task) => this._task = task;
}