﻿using DecisionMaking.Concretion.Strategy.Data;
using UnityEngine;

public class ManualTargetDataComponent : MonoBehaviour, ITargetDataProvider
{
    [SerializeField]
    private Transform _target;

    public Vector3 TargetPosition => this._target.position;

    public bool HasTarget => this._target != null;
}
