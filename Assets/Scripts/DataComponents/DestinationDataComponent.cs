﻿using UnityEngine;
using DecisionMaking.Concretion.Strategy.Data;

public class DestinationDataComponent : MonoBehaviour, IDestinationDataProvider, IDestinationDataReceiver
{
    private Vector3 _destination;
    public Vector3 DestinationPosition => overrideDestination ? overrideDestination.transform.position : this._destination;

    [SerializeField]
    private Transform overrideDestination;

    private void Awake()
    {
        this._destination = this.transform.position;
    }

    public void SetDestination(Vector3 destination) => this._destination = destination;

    private Color sphereColor = new Color(.1f, .8f, .1f, .4f);
    private Color wireSphereColor = new Color(.1f, .8f, .1f, .9f);
    private void OnDrawGizmos()
    {
        Gizmos.color = sphereColor;
        //Gizmos.DrawSphere(DestinationPosition, .3f);
        Gizmos.color = wireSphereColor;
        Gizmos.DrawWireSphere(DestinationPosition, .32f);
        Gizmos.DrawLine(this.transform.position, DestinationPosition);
    }
}
