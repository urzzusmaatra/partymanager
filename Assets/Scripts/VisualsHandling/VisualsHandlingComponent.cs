﻿using UnityEngine;
using Utility.Logging;

public class VisualsHandlingComponent : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer _spriteRenderer;
    public void SetVisualsData(VisualsData visualData)
    {
        Log.LogBaseMessage($"Seting visuals to object {visualData}");

        if(_spriteRenderer == null)
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        _spriteRenderer.sprite = visualData.Sprite;
        _spriteRenderer.color = visualData.Color;
        _spriteRenderer.enabled = visualData.EnableRenderer;
    }

    public Sprite CurrentSprite => _spriteRenderer.sprite;
}
