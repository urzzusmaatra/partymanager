﻿using Sirenix.OdinInspector;
using UnityEngine;

[System.Serializable]
public class VisualsDataCreator
{
    [SerializeField]
    private bool enableRenderer = true;
    [SerializeField]
    private Sprite _sprite;
    [SerializeField]
    private bool _useGradient;
    [SerializeField, HideIf("_useGradient")]
    private Color _color;
    [SerializeField, ShowIf("_useGradient")]
    private Gradient _gradient;


    public VisualsData GetData()
    {
        return new VisualsData(_sprite, _color, enableRenderer);
    }

    public VisualsData GetData(float gradientEvaluationValue)
    {
        return new VisualsData(_sprite, _useGradient ? _gradient.Evaluate(gradientEvaluationValue) : _color, enableRenderer);
    }

    public static VisualsData CreateData(Sprite sprite, Color spriteColor, bool enableRenderer = true)
    {
        return new VisualsData(sprite, spriteColor, enableRenderer);
    }
}