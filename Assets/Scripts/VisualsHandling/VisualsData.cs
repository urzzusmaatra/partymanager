﻿using UnityEngine;

public struct VisualsData
{
    public bool EnableRenderer { get; private set; }
    public Sprite Sprite { get; private set; }
    public Color Color { get; private set; }
    public VisualsData(Sprite sprite, Color spriteColor, bool enableRenderer)
    {
        Sprite = sprite;
        Color = spriteColor;
        EnableRenderer = enableRenderer;
    }
}
