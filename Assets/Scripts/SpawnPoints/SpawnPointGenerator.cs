using UnityEngine;
using FrostSnail.Events;
using Map.MapServices.Events;
using Map.MapServices;
using Worlds;
using Services;
using Utility.Logging;
using System.Collections.Generic;
using PartyManager.Assets.Scripts.Utility.Extensions;

namespace SpawnPoints
{
    [RequireComponent(typeof(SpawnPointDataProvider))]
    public class SpawnPointGenerator : MonoBehaviour
    {
        private const int NOISE_POSITION_OFFSET = 10;
        private EventReceiver eventReceiver;
        [SerializeField, UnityEngine.Serialization.FormerlySerializedAs("data")]
        private SpawnPointDataProvider dataProviderComponent;
        private IMapService mapService;
        private IWorldService worldService;
        private bool isMapSet = false;

        private List<GameObject> instantiatedSpawners = new List<GameObject>();
        private void Start()
        {
            if (dataProviderComponent == null)
                TryGetComponent(out dataProviderComponent);

            eventReceiver = EventReceiverFactory.CreateEventReceiver(MapChannel.Channel);
            eventReceiver.RegisterMethod<MapCreatedEvent>(MapCreatedEventReaction);

            GameServices.GetService(GetMapService);
        }

        private void MapCreatedEventReaction(MapCreatedEvent mapEvent)
        {
            isMapSet = true;
            TryGenerateSpawnPoint();
        }

        [Sirenix.OdinInspector.Button("Request spawn point creation")]
        [Sirenix.OdinInspector.DisableInPlayMode()]
        private void RequestCreation()
        {
            if (isMapSet)
            {
                TryGenerateSpawnPoint();
            }
            else
            {
                Debug.LogError("Map not set!");//try through mutable channel
            }
        }

        private bool TryGenerateSpawnPoint()
        {
            if (!isMapSet)
                return false;

            if (dataProviderComponent.SpawnerPrefab == null)
            {
                Log.LogBaseError("No spawner prefab defined i spawn point generator data.", dataProviderComponent);
                return false;
            }

            if (mapService == null)
            {
                Log.LogBaseError("Map service not yet avaliable.", transform);
                return false;
            }

            if (worldService == null)
            {
                Log.LogBaseError("World service not yet avaliable", transform);
                return false;
            }

            ///we are currently assuming positions and factions that need to be spawned
            ///this info should arrive externaly and 
            ///this should only deal with how to spawn spawners and set their data
            var samplePosition = CalculateSamplePosition();
            uint seed = worldService.Seed;
            List<(float, Vector2Int)> possiblePositions = new List<(float, Vector2Int)>();

            for (int i = 0; i < GetSampleSize().x; i++)
            {
                for (int j = 0; j < GetSampleSize().y; j++)
                {
                    Vector2Int position = new Vector2Int(i, j) + samplePosition;
                    positionsToDraw.Add(position);
                    if (IsPositionIncludable(position))
                    {
                        var value = mapService.GetValueFromNoise(seed, position.x, position.y, NOISE_POSITION_OFFSET);
                        possiblePositions.Add((value, position));
                    }
                }
            }

            possiblePositions.Sort((x, y) => { return x.Item1 >= y.Item1 ? -1 : 1; });

            int numberOfSpawners = Mathf.Min(dataProviderComponent.NumberOfSpawners, possiblePositions.Count);
            int spawnersSpawned = 0;

            for (int i = 0; i < numberOfSpawners; i++)
            {
                Vector2Int spawnerPosition = possiblePositions[i].Item2;
                var spawner = Instantiate(dataProviderComponent.SpawnerPrefab);
                instantiatedSpawners.Add(spawner.gameObject);
                spawner.AddActorConstuctionData(dataProviderComponent.GetAdditionalConstructionData);
                spawner.transform.position = spawnerPosition.ConvertVector();
                spawnersSpawned++;
            }

            return spawnersSpawned == dataProviderComponent.NumberOfSpawners;
        }

        private bool GetMapService(IService service)
        {
            if (service is IMapService mapService)
                this.mapService = mapService;
            else if (service is IWorldService worldService)
                this.worldService = worldService;

            return this.mapService != null && worldService != null;
        }

        private Vector2Int GetSampleSize() => dataProviderComponent.SpawnerSampleSize;

        private Vector2Int CalculateSamplePosition()
        {
            return new Vector2Int(dataProviderComponent.SpawnerPosition.x - dataProviderComponent.SpawnerSampleSize.x / 2, dataProviderComponent.SpawnerPosition.y + dataProviderComponent.SpawnerSampleSize.y / 2);
        }

        private bool IsPositionIncludable(Vector2Int position) => mapService.IsTileOnMap(position) && mapService.IsTileWalkable(position);

        private List<Vector2> positionsToDraw = new List<Vector2>();
        private void OnDrawGizmosSelected()
        {
            foreach (var pos in positionsToDraw)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireCube(new Vector3(pos.x, pos.y, 0f), Vector3.one);
            }
        }
    }
}