using Services;
using System.Collections.Generic;
using UnityEngine;
using Items.Service;
using PofyTools;
using PartyManager.Assets.Scripts.ActorEntity;

namespace SpawnPoints
{
    public class SpawnerItemPlugin : MonoBehaviour, IActorConstructionData
    {

        private IItemDistributionService itemDistService;

        private void Awake()
        {
            GameServices.GetService(ReceiveService);
        }

        public void SetupActor(Actor actor)
        {
            var item = itemDistService.GetRandomItem();

            if (item is SocketableItem weapon)
            {
                SocketActionRequest.TryEquipItemToOwner(actor.SocketOwner, weapon.SocketableComponent, weapon.SocketId);
            }
        }

        private bool ReceiveService(IService service)
        {
            if (service is IItemDistributionService itemService)
            {
                itemDistService = itemService;
                return true;
            }

            return false;
        }
    }
}