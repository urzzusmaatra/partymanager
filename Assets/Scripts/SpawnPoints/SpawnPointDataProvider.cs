﻿using UnityEngine;

namespace SpawnPoints
{
    public class SpawnPointDataProvider : MonoBehaviour
    {
        [SerializeField]
        private int numberOfSpawners;
        public int NumberOfSpawners { get => numberOfSpawners; }

        [SerializeField]
        private SpawningComponent spawnerPrefab;
        public SpawningComponent SpawnerPrefab { get => spawnerPrefab; }
        public IActorConstructionData[] GetAdditionalConstructionData { get => GetComponents<IActorConstructionData>(); }

        [SerializeField]
        private Vector2Int spawnerPosition = Vector2Int.zero;
        public Vector2Int SpawnerPosition { get => spawnerPosition; }

        [SerializeField]
        private Vector2Int spawnerSampleSize = Vector2Int.zero;
        public Vector2Int SpawnerSampleSize { get => spawnerSampleSize; }

        [SerializeField]
        private string region;
        public string Region { get => region; }
    }
}