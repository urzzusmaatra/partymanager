﻿using Services;
using FactionServices;
using UnityEngine;
using Factions;
using PartyManager.Assets.Scripts.ActorEntity;

namespace SpawnPoints
{
    public class SpawnerFactionPlugin : MonoBehaviour, IActorConstructionData
    {
        public void SetupActor(Actor actor)
        {
            string region = string.Empty;

            if (TryGetComponent(out SpawnPointDataProvider dataProvider))
            {
                region = dataProvider.Region;
            }
            else
            {
                Utility.Logging.Log.LogBaseError("Empty region provided, unable to set faction for actor.", transform);
                return;
            }

            if (actor.TryGetComponent(out FactionComponent factionComponent))
            {
                factionComponent.SetFaction(region);
            }
        }
    }
}