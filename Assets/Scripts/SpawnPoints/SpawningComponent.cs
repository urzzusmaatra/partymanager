﻿using Services;
using UnityEngine;
using Scripts.Entity;
using Factions;
using Utility.Logging;
using PartyManager.Assets.Scripts.ActorEntity;

public class SpawningComponent : MonoBehaviour
{
    private Actor existingInstance;
    private bool isServiceReady = false;
    private IEntityService entityService;
    [SerializeField, UnityEngine.Serialization.FormerlySerializedAs("_actorConstructionData")]
    private MultipleActorConstructionDatas actorConstructionData;
    private const string ACTOR_NAME_PREFIX = "ACTOR|";
    private void Start()
    {
        GameServices.GetService(ReceiveService);
    }

    private void Update()
    {
        if (isServiceReady)
        {
            if (existingInstance == null)
            {
                existingInstance = entityService.CreateActor(actorConstructionData);
                existingInstance.transform.position = transform.position;// + Random.Range(-1f, 1f) * Vector3.right + Random.value * Vector3.up;
                existingInstance.OnActorRemoved += OnActorDeath;
                existingInstance.gameObject.name = ACTOR_NAME_PREFIX + existingInstance.GetComponent<FactionComponent>().Faction.FactionName.ToString() + existingInstance.transform.GetInstanceID().ToString();
            }
        }
    }

    private void OnActorDeath(Actor actor)
    {
        this.existingInstance.OnActorRemoved -= OnActorDeath;
        existingInstance = null;
    }

    private bool ReceiveService(IService service)
    {
        if (service is IEntityService entityService)
        {
            this.entityService = entityService;
            isServiceReady = true;
            return true;
        }

        return false;
    }

    public void AddActorConstuctionData(IActorConstructionData[] data) => actorConstructionData.AttachAdditionalConstructors(data);
}
