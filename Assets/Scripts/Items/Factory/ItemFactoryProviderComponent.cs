using Services;
using UnityEngine;
using Items.Service;

namespace Items.Factory
{
    public class ItemFactoryProviderComponent : MonoBehaviour
    {
        [SerializeField]
        public ItemCreationDefinition itemCreationDefinition;
        [SerializeField]
        private int weight = 10;

        private void Start()
        {
            GameServices.GetService(ReceiveService);
        }

        private bool ReceiveService(IService service)
        {
            if (service is IItemDistributionService itemService)
            {
                itemService.AddItemDefinition(itemCreationDefinition, weight);
                return true;
            }

            return false;
        }
    }
}