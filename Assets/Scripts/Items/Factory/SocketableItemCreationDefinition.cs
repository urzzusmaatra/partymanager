﻿using UnityEngine;

[CreateAssetMenu(fileName = "SocketableItemCreationDefinition", menuName = "Item/Socketable Creation Definition")]
public class SocketableItemCreationDefinition : ItemCreationDefinition
{
    [SerializeField]
    private string socketId;
    public SocketableItemData CreateWeaponData() => new SocketableItemData(socketId);

    internal override void SetItemData(Item item)
    {
        base.SetItemData(item);
        (item as SocketableItem).ProvideBasicData(CreateWeaponData());
    }
}
