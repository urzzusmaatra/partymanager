using UnityEngine;
using AbilitySpace;
using System.Collections.Generic;
using Utility.Logging;

[CreateAssetMenu(fileName = "ItemCreationDefinition", menuName = "Item/Creation Definition")]
public class ItemCreationDefinition : ScriptableObject
{
    ///this creation definition should have ways of creting all 
    ///data but also a way of providing some input based on which creation 
    ///will happen and somehow determine som of the results
    ///i.e. damage to be between some points and a way to provide a value to determine which value will be
    [SerializeField]
    private VisualsDataCreator _visuals;
    [SerializeField]
    private string _itemName;

    internal virtual void SetItemData(Item weapon)
    {
        weapon.ProvideBasicData(GetItemData);
    }

    [SerializeField]
    private float _itemWeight;
    [SerializeField]
    private List<AbilityData> abiliyData;
    public IReadOnlyList<AbilityData> AbilityData => abiliyData;
    [SerializeField]
    private Item prefab;
    public Item Prefab { get => prefab; }

    public ItemData GetItemData => new ItemData(_itemName, _itemWeight);

    public VisualsData Visuals() => _visuals.GetData();

    public Item CreateItemInstance
    {
        get
        {
            Log.LogBaseMessage($"Creating instance of a item {Prefab}");
            return GameObject.Instantiate(Prefab);
        }
    }
}
