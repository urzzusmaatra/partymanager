﻿using UnityEngine;
using PofyTools;
using AbilitySpace;
using FrostSnail.Events;
using Utility.Logging;
/// <summary>
/// Weapon can be any type, melee with stab, swing or blnut, bash, slice, poke or ranged, long etc etc
/// </summary>

[RequireComponent(typeof(SocketableComponent))]
public class SocketableItem : Item
{
    private ItemData _itemData;
    private SocketableItemData itemData;
    private SocketableComponent _socketableComponent;

    public string SocketId => itemData.SocketId;

    private Transform _owner;
    
    public override bool Initialize()
    {
        if(base.Initialize())
        {
            this._socketableComponent = GetComponent<SocketableComponent>();
            this._socketableComponent.OnSocketableEquiped += OnItemSocketed;
            this._socketableComponent.OnSocketableUnequiped += OnItemUnsocketed;
            return true;
        }
        return false;
    }

    public SocketableComponent SocketableComponent { get => _socketableComponent; set => _socketableComponent = value; }

    protected void OnItemSocketed(IHaveSockets owner)
    {
        this._owner = owner.transform;
    }

    protected void OnItemUnsocketed(IHaveSockets owner)
    {
        this._owner = null;
    }

    public void ProvideBasicData(SocketableItemData data)
    {
        Log.LogBaseMessage($"Providing basic weapon data: {data}");
        itemData = data;
    }

    protected override void OnAbilityProcessResultCallback(AbilityProcessEvent result)
    {
        var itemEvent = EventFactory.GetEvent<ItemAbilityProcessEvent>();
        itemEvent.Item = this;
        itemEvent.Target = result.Target;
        itemEvent.Ability = result.Ability;
        itemEvent.TrySendTo(_owner);
    }
}