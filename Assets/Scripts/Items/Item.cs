﻿using AbilitySpace;
using AbilitySpace.Internal;
using PofyTools;
using UnityEngine;
using FrostSnail.Events;
using Utility.Logging;

[RequireComponent(typeof(AbilityProviderComponent))]
[RequireComponent(typeof(IEventChannel))]
public class Item : MonoBehaviour, IInitializable, IAbilityProvider
{
    private AbilityProviderComponent abilityProviderComponent;

    private ItemData _data;
    [SerializeField]
    protected VisualsHandlingComponent _visualsComponent;

    private EventReceiver eventHandler;
    [SerializeField]
    protected IEventChannel eventComponent;
    
    protected void Awake() 
    {
        Initialize();
    }

    #region Initialization
    private bool isInitialized;
    public bool IsInitialized => this.isInitialized;

    public virtual bool Initialize()
    {
        if (!this.IsInitialized)
        {
            isInitialized = true;
            abilityProviderComponent = GetComponent<AbilityProviderComponent>();
            eventHandler = EventReceiverFactory.CreateEventReceiver();
            eventHandler.RegisterMethod<AbilityProcessEvent>(OnAbilityProcessResultCallback);
            if(eventComponent == null)
            {
                eventComponent = GetComponent<IEventChannel>();
            }
            return true;
        }

        return false;
    }
    #endregion

    public virtual void AddAbility(Ability ability)
    {
        eventHandler.TryRegisterObject(ability.gameObject);
        AbilityProvider.AddAbility(ability);
        ability.transform.SetParent(transform, false);
    }

    public VisualsHandlingComponent VisualsHandler => this._visualsComponent;

    public AbilityProviderComponent AbilityProvider => this.abilityProviderComponent;

    public void ProvideBasicData(ItemData data)
    {
        Log.LogBaseMessage($"Providing basic item data: {data}");
        this._data = data;
        gameObject.name = gameObject.name.Replace("(Clone)", data.itemType);
    }

    protected virtual void OnAbilityProcessResultCallback(AbilityProcessEvent result)
    {
        var itemEvent = EventFactory.GetEvent<ItemAbilityProcessEvent>();
        itemEvent.Item = this;
        itemEvent.Target = result.Target;
        itemEvent.Ability = result.Ability;
        Log.LogBaseError("No owner to send to, this needs to be setup");
        //if this is potion it needs to have an owner, and not to be ownerless
    }
}