﻿[System.Serializable]
public struct SocketableItemData
{
    public string SocketId { get => socketId; }
    private string socketId;

    public SocketableItemData(string socketId) : this()
    {
        this.socketId = socketId;
    }
}
