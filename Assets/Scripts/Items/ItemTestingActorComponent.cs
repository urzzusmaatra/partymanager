using Items.Service;
using PartyManager.Assets.Scripts.ActorEntity;
using PofyTools;
using Services;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Actor))]
public class ItemTestingActorComponent : MonoBehaviour
{
    [Button("Acquire item"), DisableInEditorMode]
    private void AcquireRandomItem()
    {
        GameServices.GetService(ReceiveService);
    }

    private bool ReceiveService(IService service)
    {
        if (service is IItemDistributionService itemsService)
        {
            var actor = GetComponent<Actor>();
            var item = itemsService.GetRandomItem();


            if (item is SocketableItem weapon)
            {
                SocketActionRequest.TryEquipItemToOwner(GetComponent<Actor>().SocketOwner, weapon.SocketableComponent, weapon.SocketId);
            }
            return true;
        }

        return false;
    }
}
