﻿using AbilitySpace;
using FrostSnail.Events;
using System;
using UnityEngine;

public class ItemAbilityProcessEvent : EventData
{
    public IAbility Ability { get; set; }
    public Transform Target { get; set; }
    public Item Item { get; set; }

}

