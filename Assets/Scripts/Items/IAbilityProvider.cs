﻿using AbilitySpace;

public interface IAbilityProvider
{
    AbilityProviderComponent AbilityProvider { get; }
}