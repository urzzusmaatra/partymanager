﻿public interface IProjectileService
{
    bool CreateProjectilePool(Projectile projectile);
    Projectile GetProjectile<T>() where T : Projectile;
}