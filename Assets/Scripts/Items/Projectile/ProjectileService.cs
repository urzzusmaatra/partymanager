﻿using Extensions;
using System.Collections.Generic;
using UnityEngine;
using Utility.Logging;
public class ProjectileService : IProjectileService
{
    private List<Projectile> _prefabs;//switch this with projectile pools

    public ProjectileService()
    {
        this._prefabs = new List<Projectile>();
    }

    public static IProjectileService CreateBasicService() => new ProjectileService();

    public bool CreateProjectilePool(Projectile projectile)
    {
        return this._prefabs.AddOnce(projectile);
    }

    public Projectile GetProjectile<T>() where T : Projectile
    {
        foreach (var prefab in this._prefabs)
        {
            if (prefab is T projectile)
            {
                return GameObject.Instantiate(projectile);
            }
        }

        Log.LogBaseError("Couldn't find projectile of type " + typeof(T));

        return null;
    }
}
