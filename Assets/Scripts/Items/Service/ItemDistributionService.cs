﻿using FrostSnail.Broker;

namespace Items.Service.Internal
{
    /// <summary>
    /// I don't like how this functions. We have little controll over what will be created, and cannot force specific results.
    /// </summary>
    public class ItemDistributionService : IItemDistributionService
    {
        private const float DEFAULT_FACTORY_WEIGHT = 10f;
        private BrokerMarket<ItemCreationDefinition> _itemFactory;
        public ItemDistributionService()
        {
            _itemFactory = new BrokerMarket<ItemCreationDefinition>();
        }

        public static IItemDistributionService CreateBasicService()
        {
            return new ItemDistributionService();
        }

        public Item GetRandomItem()
        {
            return ItemFactory.CreateItem(_itemFactory.GetCommodity());
        }

        public void AddItemDefinition(ItemCreationDefinition creationDefinition, float weight = DEFAULT_FACTORY_WEIGHT)
        {
            _itemFactory.AddCommodity(weight, creationDefinition);
        }
    }

}
