﻿using Services;

namespace Items.Service
{
    public interface IItemDistributionService : IService
    {
        void AddItemDefinition(ItemCreationDefinition factory, float weight = 10);
        Item GetRandomItem();
    }
}