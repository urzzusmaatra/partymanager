﻿using UnityEngine;

namespace ItemService
{
    [System.Serializable]
    public class WeaponDistributionData
    {
        [SerializeField]
        private SocketableItem _weaponPrefab;
        [SerializeField]
        private int _quantity;

        public SocketableItem WeaponPrefab => _weaponPrefab;
        public int Quantity => _quantity;
    }
}