﻿public class ItemFactory
{
    public static Item CreateItem(ItemCreationDefinition definition)
    {
        var item = definition.CreateItemInstance;

        definition.SetItemData(item);
        item.VisualsHandler.SetVisualsData(definition.Visuals());
        foreach (var abilityPrefab in definition.AbilityData)
        {
            item.AddAbility(abilityPrefab.GetAbility);
        }

        return item;
    }
}
