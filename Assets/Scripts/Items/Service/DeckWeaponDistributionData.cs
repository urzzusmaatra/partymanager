﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ItemService
{
    [CreateAssetMenu(fileName = "DeckWeaponDistributionData", menuName = "Item/WeaponDeckDistributionData")]
    public class DeckWeaponDistributionData : ScriptableObject, IEnumerable<WeaponDistributionData>
    {
        [SerializeField]
        private List<WeaponDistributionData> _weaponDistributionData;

        public WeaponDistributionData WeaponDistributionData => WeaponDistributionData;

        public IEnumerator<WeaponDistributionData> GetEnumerator()
        {
            return ((IEnumerable<WeaponDistributionData>)_weaponDistributionData).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<WeaponDistributionData>)_weaponDistributionData).GetEnumerator();
        }
    }
}