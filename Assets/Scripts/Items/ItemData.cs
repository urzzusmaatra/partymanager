﻿[System.Serializable]
public struct ItemData
{
    public readonly string itemType;
    public readonly float weight;

    public ItemData(string itemType, float weight)
    {
        this.itemType = itemType;
        this.weight = weight;
    }
}