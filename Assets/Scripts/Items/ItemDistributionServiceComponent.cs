﻿using UnityEngine;
using Services.Internal;
using Services;

namespace Items
{
    public class ItemDistributionServiceComponent : MonoBehaviour
    {

        private void Awake()
        {
            GameServices.Provide(ServicesConfiguration.GetItemDistributionService());
        }
    }
}