﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;
using AbilitySpace.UI.Internal;

namespace PartyManager.Assets.Scripts.UI.Ability
{
    [CreateAssetMenu(fileName = "AbilityUIRepository", menuName = "Ability/AbilityUIRepository")]
    public class AbilityUIRepository : SerializedScriptableObject
    {
        [SerializeField]
        [DictionaryDrawerSettings(KeyLabel = "Ability Name", ValueLabel = "UI Resources")]
        private Dictionary<string, AbilityUIResources> resources;

        public AbilityUIResources GetAbilityUIResource(string abilityName)
        {
            return resources[abilityName];
        }
    }
}
