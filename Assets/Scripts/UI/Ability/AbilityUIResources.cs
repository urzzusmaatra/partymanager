﻿using UnityEngine;

namespace AbilitySpace.UI.Internal
{
    [System.Serializable]
    public class AbilityUIResources
    {
        [SerializeField]
        private Sprite abilityHUDImage;

        public Sprite AbilityHUDImage { get => abilityHUDImage; }
    }
}
