﻿using UnityEngine;
using HUD;//this is not HUD pool, it's just pool
using FrostSnail.SimpleObjectPool;
namespace Window.Internal
{
    public class WindowModuleType : ObjectPool
    {
        private WindowComponent prefab;
        private Canvas canvas;

        public WindowModuleType(WindowComponent prefab)
        {
            this.prefab = prefab;
        }

        public WindowComponent GetWindow => (WindowComponent)Withdraw();

        protected override IPoolableObject Create()
        {
            var window = GameObject.Instantiate(prefab);
            window.transform.SetParent(canvas.transform, false);
            return (IPoolableObject)window;
        }

        public void SetCanvas(Canvas canvas)
        {
            this.canvas = canvas;
        }
    }
}