using System;
using FrostSnail.SimpleObjectPool;
using UnityEngine;
namespace Window
{
    public class WindowComponent : MonoBehaviour, IPoolableObject
    {
        [SerializeField]
        protected RectTransform windowTransform;
        public RectTransform WindowTransform 
        { 
            get
            {
                if(windowTransform == null)
                {
                    windowTransform = GetComponent<RectTransform>();
                }

                return windowTransform;
            }
        }
        [SerializeField]
        protected WindowContentHolderComponent windowContent;
        [SerializeField]
        protected WindowHeaderComponent windowHeader;

        private void Awake()
        {
            windowHeader.OnColoseWindowCallback += CloseWindowCallbackHandler;
            windowHeader.OnToggleContentVisibility += ToggleContentVisibilityCallbackHandler;
        }

        public void SetContent(WindowContentData data)
        {
            windowContent.SetContent(data);
            windowHeader.SetContent(data);
        }

        public void SetHeader(WindowHeaderData data)
        {
            windowHeader.SetHeader(data);
        }

        public void ToggleContentVisibilityCallbackHandler()
        {
            //windowContent.ToggleContentVisibility();
        }

        public Action<WindowComponent> OnWindowClosed;
        private void CloseWindowCallbackHandler()
        {
            OnWindowClosed?.Invoke(this);
            windowContent.WindowClosingReaction();
            ///return it to pool? why did you make pooled windows? the fuck
            ///oh yeah, in case you are reading this again in dead of night
            ///pooling some windows as we expect a lot of them, like character details, weapon details 'n' such
            ///ofc if this proves unrealistic delete this pooling :D
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }

        public void ResetObject()
        {
        }
    }
}