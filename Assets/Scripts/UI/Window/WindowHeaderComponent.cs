﻿using System;
using TMPro;
using UnityEngine;
namespace Window
{
    public class WindowHeaderComponent : MonoBehaviour
    {
        [SerializeField]
        private RectTransform headerRectTransform;
        [SerializeField]
        private TMP_Text headerText;

        public void SetContent(WindowContentData data)
        {
            headerRectTransform.sizeDelta = new Vector2(data.content.GetSizeDelta.x, headerRectTransform.sizeDelta.y);
        }

        public void SetHeader(WindowHeaderData data)
        {
            headerText.SetText(data.title);
        }

        public Action OnToggleContentVisibility;
        public void ToggleContentVisibility()
        {
            OnToggleContentVisibility?.Invoke();
        }

        public Action OnColoseWindowCallback;
        public void CloseWindow()
        {
            OnColoseWindowCallback?.Invoke();
        }
    }
}