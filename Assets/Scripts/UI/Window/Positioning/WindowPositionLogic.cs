﻿using UnityEngine;
using Utility.Logging;

namespace Window.Positioning
{
    public class WindowPositionLogic
    {
        internal void SetupWindow(RectTransform windowTransform, WindowPositionData data)
        {
            switch (data.type)
            {
                case WindowPositionData.DataType.None:
                    Log.LogBaseError("None data type detected in WindowPositionData. Probably incorrect");
                    break;
                case WindowPositionData.DataType.AnchorPosition:
                    SetupAnchorPosition(windowTransform, data);
                    break;
                case WindowPositionData.DataType.RectTransform:
                    SetupRectTransform(windowTransform, data);
                    break;
                default:
                    Log.LogBaseError($"Type {data.type} is not implemented in WindowPositionData.");
                    break;
            }
        }

        private void SetupRectTransform(RectTransform windowTransform, WindowPositionData data)
        {
            windowTransform.anchorMin = data.anchorTransform.anchorMin;
            windowTransform.anchorMax = data.anchorTransform.anchorMax;
            windowTransform.pivot = data.anchorTransform.pivot;
            windowTransform.anchoredPosition = data.anchorTransform.anchoredPosition;
        }

        private void SetupAnchorPosition(RectTransform windowTransform, WindowPositionData data)
        {
            windowTransform.anchoredPosition = data.anchorPosition;
        }
    }
}