﻿using UnityEngine;

namespace Window.Positioning
{
    public interface IWindowPositioningComponent
    {
        WindowPositionData GetWindowPositionData(Canvas canvas);
    }
}
