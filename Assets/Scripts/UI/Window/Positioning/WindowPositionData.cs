﻿using System;
using UnityEngine;

namespace Window.Positioning
{
    public struct WindowPositionData
    {
        public enum DataType
        {
            None,
            AnchorPosition,
            RectTransform
        }

        public readonly DataType type;
        public readonly Vector2 anchorPosition;
        public readonly RectTransform anchorTransform;

        public WindowPositionData(DataType type, Vector2 anchorPosition, RectTransform anchorTransform)
        {
            this.type = type;
            this.anchorPosition = anchorPosition;
            this.anchorTransform = anchorTransform;
        }

        public static WindowPositionData CreateAnchorPositionData(Vector2 anchorPosition)
        {
            return new WindowPositionData(DataType.AnchorPosition, anchorPosition, null);
        }

        internal static WindowPositionData CreateAnchorTransformData(RectTransform defaultTransform)
        {
            return new WindowPositionData(DataType.RectTransform, Vector2.zero, defaultTransform);
        }

    }
}