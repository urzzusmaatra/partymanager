﻿using UnityEngine;
using Utility.Logging;

namespace Window.Positioning.Internal
{
    public class WindowAnchorTransformPositioning : MonoBehaviour, IWindowPositioningComponent
    {
        [SerializeField]
        private string anchorName;

        public WindowPositionData GetWindowPositionData(Canvas canvas)
        {
            if (string.IsNullOrEmpty(anchorName))
            {
                Log.LogBaseError($"Improper implemented WindowAnchorTrasnformPositioning component on object {transform}.");
                return default;
            }

            var canvasTransform = canvas.transform;
            var defaultObject = canvasTransform.Find(anchorName);

            if(defaultObject == null)
            {
                Log.LogBaseError($"Couldn't find {anchorName} in canvas {canvas}.");
                return default;
            }

            var defaultTransform = defaultObject.GetComponent<RectTransform>();

            return WindowPositionData.CreateAnchorTransformData(defaultTransform);

        }
    }
}
