using System;
using UnityEngine;

namespace Window.Positioning.Internal
{
    public class WindowAnchorPositionPositioning : MonoBehaviour, IWindowPositioningComponent
    {
        [SerializeField]
        private Vector2 defaultPosition;

        public WindowPositionData GetWindowPositionData(Canvas canvas)
        {
            return WindowPositionData.CreateAnchorPositionData(defaultPosition);
        }
    }
}
