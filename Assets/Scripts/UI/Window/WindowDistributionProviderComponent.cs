﻿using System.Collections.Generic;
using UnityEngine;
using Window.Internal;
using Services;

namespace Window
{
    public class WindowDistributionProviderComponent : MonoBehaviour
    {
        [SerializeField]
        private List<WindowModuleTypeConstructor> windowHUDModuleTypeConstructors;

        private void Awake()
        {
            GameServices.GetService(ReceiveService);
        }
        private bool ReceiveService(IService service)
        {
            if (service is IHUDModulesService HUDModulesService)
            {
                HUDModulesService.AddHUDModule(new WindowDistributionModule(windowHUDModuleTypeConstructors));
                return true;
            }
            return false;
        }
    }
}