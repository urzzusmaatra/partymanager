using HUD;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Window
{
    public class DragableWindowComponent : WindowHeaderComponent, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField]
        private RectTransform windowTransform;

        public void OnBeginDrag(PointerEventData eventData)
        {

        }

        public void OnDrag(PointerEventData eventData)
        {
            windowTransform.anchoredPosition += eventData.delta;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
        }
    }
}