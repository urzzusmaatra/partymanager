using Services;
using System.Collections.Generic;
using UnityEngine;
using Window.Internal;

namespace Window
{
    public class WindowDistributionModule : IHUDModule
    {
        private Canvas parentCanvas;
        private Dictionary<int, WindowModuleType> windows;

        public WindowDistributionModule(List<WindowModuleTypeConstructor> windowHUDModuleTypeConstructors)
        {
            windows = new Dictionary<int, WindowModuleType>();
            foreach (var windowConstructors in windowHUDModuleTypeConstructors)
            {
                var constructedPair = windowConstructors.Construct();
                windows.Add(constructedPair.Item1, constructedPair.Item2);
            }
        }

        public WindowComponent GetWindow(int id) => windows[id].GetWindow;

        public void SetCamera(Camera camera) { }

        public void SetCanvas(Canvas canvas)
        {
            parentCanvas = canvas;
            foreach (var windowModules in windows.Values)
            {
                windowModules.SetCanvas(canvas);
            }
        }
    }
}