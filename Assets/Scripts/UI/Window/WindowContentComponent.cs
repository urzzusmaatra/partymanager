using UnityEngine;

namespace Window
{
    [RequireComponent(typeof(RectTransform))]
    public class WindowContentComponent : MonoBehaviour
    {
        [SerializeField]
        private RectTransform rectTransform;

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            HideContentOffScreen();
        }

        public void OnContentHidden()
        {
        }

        public void OnContentShown()
        {
        }
        
        public void RemoveContent()
        {
            rectTransform.SetParent(null);
            HideContentOffScreen();
        }

        private void HideContentOffScreen()
        {
            rectTransform.position = WindowConfiguration.HiddenPosition;
        }

        public void AttachContentToHolder(Transform holdersTransform)
        {
            transform.SetParent(holdersTransform);
            rectTransform.anchoredPosition = Vector2.zero;
        }

        public Vector2 GetSizeDelta => rectTransform.sizeDelta;
    }
}