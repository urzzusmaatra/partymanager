﻿namespace Window
{
    public struct WindowHeaderData
    {
        public readonly string title;

        public WindowHeaderData(string title)
        {
            this.title = title;
        }

        public static WindowHeaderData CreateHeader(string title)
        {
            return new WindowHeaderData(title);
        }
    }
}