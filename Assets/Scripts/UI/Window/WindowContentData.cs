﻿using UnityEngine;
using Window;

namespace Window
{
    public struct WindowContentData
    {
        public readonly WindowContentComponent content;

        private WindowContentData(WindowContentComponent content)
        {
            this.content = content;
        }

        public static WindowContentData CreateContent(RectTransform rectTransform)
        {
            return new WindowContentData(rectTransform.GetComponent<WindowContentComponent>());
        }

        public static WindowContentData CreateContent(WindowContentComponent content)
        {
            return new WindowContentData(content);
        }
    }
}