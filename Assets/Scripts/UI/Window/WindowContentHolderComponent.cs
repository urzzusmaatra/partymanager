using UnityEngine;
using UnityEngine.Serialization;

namespace Window
{
    public class WindowContentHolderComponent : MonoBehaviour
    {
        [SerializeField, FormerlySerializedAs("content")]
        private RectTransform contentHolderTransform;

        private WindowContentComponent content;

        public void SetContent(WindowContentData data)
        {
            if (content != null)
            {
                content.RemoveContent();//this shouldn't be the case, we should know that window is buisy displaying something else
            }
            content = data.content;
            content.AttachContentToHolder(transform);
            contentHolderTransform.sizeDelta = content.GetSizeDelta;
            ShowContent();

        }

        public void ShowContent()
        {
            gameObject.SetActive(true);
            content.OnContentShown();
        }

        public void HideContent()
        {
            gameObject.SetActive(false);//is this the good way of hiding content?
            content.OnContentHidden();
        }

        public void WindowClosingReaction()
        {
            content.RemoveContent();
            HideContent();
        }
    }
}