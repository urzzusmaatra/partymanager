﻿using UnityEngine;
using FrostSnail.Core.Identifier;

namespace Window.Internal
{
    [CreateAssetMenu(fileName = "WindowModuleTypeConstructor", menuName = "Window/Constructor")]
    public class WindowModuleTypeConstructor : ScriptableObject
    {
        [SerializeField]
        private WindowComponent windowPrefab;
        [SerializeField]
        private Identifier windowIdentifier;
        public (int, WindowModuleType) Construct()
        {
            return (windowIdentifier.Id, new WindowModuleType(windowPrefab));
        }
    }
}