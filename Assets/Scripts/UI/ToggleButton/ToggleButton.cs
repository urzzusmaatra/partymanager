using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Window
{
    public class ToggleButton : MonoBehaviour
    {
        [SerializeField]
        private Button onButton, offButton;
        [SerializeField]
        private bool isOnButtonFirst = true;

        public UnityEvent OnButtonClicked;
        public UnityEvent OffButtonClicked;

        public Button.ButtonClickedEvent obvce;
        private void Awake()
        {
            if (isOnButtonFirst)
                OnButtonEvent();
            else
                OffButtonEvent();

            onButton.onClick.AddListener(OnButtonEvent);
            offButton.onClick.AddListener(OffButtonEvent);
        }

        public void OnButtonEvent()
        {
            OnButtonClicked?.Invoke();
            ChangeButtonActivity(onButton, false);
            ChangeButtonActivity(offButton, true);
        }

        public void OffButtonEvent()
        {
            OffButtonClicked?.Invoke();
            ChangeButtonActivity(offButton, false);
            ChangeButtonActivity(onButton, true);
        }

        private void ChangeButtonActivity(Button button, bool isActive)
        {
            button.gameObject.SetActive(isActive);
        }

    }
}