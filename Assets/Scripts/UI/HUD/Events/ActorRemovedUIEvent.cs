﻿using FrostSnail.Events;
using PartyManager.Assets.Scripts.ActorEntity;

namespace PartyManager.Assets.Scripts.UI.HUD.Events
{
    public class ActorRemovedUIEvent : EventData
    {
        public Actor Actor { get; set; }
    }
}
