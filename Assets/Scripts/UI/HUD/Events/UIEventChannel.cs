﻿using UnityEngine;
using FrostSnail.Events;
using FrostSnail.Events.Internal;

namespace PartyManager.Assets.Scripts.UI.HUD.Events
{
    public class UIEventChannel
    {
        private static IEventChannel channel;
        public static IEventChannel Channel
        {
            get
            {
                //take care for this chaching here when reloading scenes etc.
                if (channel == null)
                {
                    var channelObject = new GameObject("UIChannel", typeof(EventChannelComponent));
                    channelObject.TryGetComponent(out channel);
                }

                return channel;
            }
        }
    }
}
