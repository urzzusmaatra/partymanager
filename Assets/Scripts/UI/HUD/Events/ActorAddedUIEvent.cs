﻿using FrostSnail.Events;
using PartyManager.Assets.Scripts.ActorEntity;

namespace PartyManager.Assets.Scripts.UI.HUD.Events
{
    public class ActorAddedUIEvent : EventData
    {
        public Actor Actor { get; set; }
    }
}
