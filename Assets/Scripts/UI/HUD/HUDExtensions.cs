using UnityEngine;

namespace HUD
{
    public static class HUDExtensions
    {
        public static Vector3 ToVector3(this Vector2 v) => new Vector3(v.x, v.y, 0f);
    }
}