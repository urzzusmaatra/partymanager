﻿using UnityEngine;

public class HUDManagedComponentsInstantiator : IHUDManagedComponentProvider
{
    private Canvas canvas;

    public HUDManagedComponentsInstantiator(Canvas canvas)
    {
        SetCanvas(canvas);
    }

    public HUDManagedComponent GetHUDManagedComponent(HUDManagedComponent prefab)
    {
        var instance = GameObject.Instantiate(prefab);
        instance.transform.SetParent(canvas.transform, false);
        return instance;
    }

    public void ReleaseHUDManagedComponent(HUDManagedComponent HUDManagedComponent)
    {
        //some kind of release?
        GameObject.Destroy(HUDManagedComponent.gameObject);
    }

    public void SetCanvas(Canvas canvas)
    {
        this.canvas = canvas;
    }
}
