﻿using UnityEngine;

public interface IHUDManagedComponentProvider
{
    HUDManagedComponent GetHUDManagedComponent(HUDManagedComponent managedComponent);

    void ReleaseHUDManagedComponent(HUDManagedComponent HUDManagedComponent);

    void SetCanvas(Canvas canvas);
}
