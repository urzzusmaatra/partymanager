using UnityEngine;
using FrostSnail.Core.Identifier;

public class HUDManagedComponent : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Object will be instantiated when added to Content Service. This means that object is self sustaining and should not create any error if data is missing.")]
    private bool isAutoStarted = false;
    public bool IsAutoStarted => isAutoStarted;

    [SerializeField]
    [Tooltip("Prevent content service from providing multiple types of this object.")]
    private bool isSingleInstance = false;

    public bool IsSingleInstance => isSingleInstance;

    [SerializeField]
    private Identifier windowType;

    public Identifier WindowType => windowType;

    //this component should be used only in HUDContentService and only resposibility is towards it
    //content component should choose window type and how it will handle closing/hiding
}