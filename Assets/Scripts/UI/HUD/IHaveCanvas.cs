﻿using UnityEngine;

public interface IHaveCanvas
{
    Canvas GetCanvas { get; }
}