using UnityEngine;
using UnityEngine.UI;
using Utility;
using Utility.Logging;

namespace UI.HUD.Bar
{
    [RequireComponent(typeof(RectMask2D))]
    public class MaskedSlider : MonoBehaviour, IValueBar
    {
        [SerializeField]
        private RectMask2D mask;
        private RectTransform maskRect;
        [SerializeField]
        private DirectionProcessor directionData;

        private void Start()
        {
            if(!mask.TryGetComponent(out maskRect))
            {
                Log.LogBaseError("Mask doesnt have it's rect transform!", transform);
            }
        }

        public void SetNormalizedValue(float value)
        {
            if (mask != null)
            {
                //1 - value because visible state of mask is with padding set to 0, so we need to inverse normalized value to have 1 as full slider, and 0 as empty
                mask.padding = this.directionData.Direction4D * maskRect.rect.width * (1f - value);
            }
        }

    }
}