﻿using UnityEngine;
using Utility;

namespace UI.HUD.Bar.Behaviors
{
    public class HUDBarOnChangeAnimation : MonoBehaviour, IHUDBarComponentBehavior
    {
        [SerializeField]
        private MaskedSlider bar;
        [SerializeField]
        private MaskedSlider barFollower;
        [SerializeField]
        private AnimationCurve followerAnimation;
        [SerializeField]
        private float duration;
        [SerializeField]
        private DirectionalEnums.DirectionX barDirection = DirectionalEnums.DirectionX.Right;
        private const int MAX_TASKS = 3;

        private AnimationCurveInterpolatorTask task;
        private float position, destination;
        private void Awake()
        {
            task = new AnimationCurveInterpolatorTask();
            enabled = false;
        }

        public void SetValues(float min, float max, float current, float previous)
        {
            position = current / max - min;
            destination = previous / max - min;
            bar.SetNormalizedValue(Mathf.Clamp01(position));
            enabled = true;
            task.SetValues(duration, followerAnimation);
        }

        private void Update()
        {;
            var teskResult = Mathf.Clamp01(task.Update(Time.deltaTime));
            var updateResult = Mathf.Lerp(position, destination, teskResult);
            barFollower.SetNormalizedValue(updateResult);

            if (task.CurrentStatus == AnimationCurveInterpolatorTask.Status.Idle)
            {
                this.enabled = false;
            }
        }

    }
}