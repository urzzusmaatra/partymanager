using UnityEngine;

namespace UI.HUD.Bar.Behaviors
{
    public class HUDBarSimpleComponent : MonoBehaviour, IHUDBarComponentBehavior
    {
        [SerializeField]
        private RectTransform bar;

        public void SetValues(float min, float max, float current, float previous)
        {
            this.bar.sizeDelta = Vector2.right * Mathf.Clamp01(current / max - min);
        }
    }
}