using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility.Logging;

namespace UI.HUD.Bar
{
    [RequireComponent(typeof(IHUDBarComponentBehavior))]
    public class HUDBarComponent : MonoBehaviour
    {
        private IHUDBarComponentBehavior barBehavior;
        private void Start()
        {
            if (!TryGetComponent(out barBehavior))
            {
                Log.LogBaseError("There is IHUDBarComponentBehavior defined in this bar behavior", transform);
            }
        }

        internal void SetValues(float minAmmount, float maxAmmount, float currentAmmount, float previousAmmount)
        {
            barBehavior.SetValues(minAmmount, maxAmmount, currentAmmount, previousAmmount);
        }

    }
}