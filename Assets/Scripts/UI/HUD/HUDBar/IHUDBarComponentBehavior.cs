﻿namespace UI.HUD.Bar
{
    public interface IHUDBarComponentBehavior
    {
        void SetValues(float min, float max, float current, float previous);
    }
}