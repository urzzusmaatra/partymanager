﻿using Services;
using UnityEngine;

public interface IHUDContentService : IService
{
    void AddManagedPrefab(HUDManagedComponent managedComponent);
}
