﻿
//TODO: This probably should not be in services namespace, or only this needs to be in services namespace and nothing else
//nothing should use Service.Internal expect real internal objects
using UnityEngine;

namespace Services
{
    public interface IHUDModule
    {
        void SetCanvas(Canvas canvas);
        void SetCamera(Camera camera);
    }
}