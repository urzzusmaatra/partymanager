
//TODO: This probably should not be in services namespace, or only this needs to be in services namespace and nothing else
//nothing should use Service.Internal expect real internal objects
using System;
using UnityEngine;

namespace Services
{
    public interface IHUDModulesService : IService
    {
        void AddHUDModule<T>(T newModule) where T : IHUDModule;
        bool TryGetHudModule<T>(out T module) where T : IHUDModule;

        void RegistrateOnHUDModuleAddedCallback(HUDModuleAddedCallback callbackFunc);
        void GetModule(HUDModuleAddedCallback receiveModuleFunc);
    }

    public delegate bool HUDModuleAddedCallback(object HUDModule);

}