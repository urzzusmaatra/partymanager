using UnityEngine;
using Services.Internal;
namespace Services
{
    public class HUDServiceProvider : MonoBehaviour
    {
        private void Awake()
        {
            GameServices.Provide(ServicesConfiguration.GetHudService());
        }
    }
}