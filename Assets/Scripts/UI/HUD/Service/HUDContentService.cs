using CanvasServices;
using Services;
using System.Collections.Generic;
using UnityEngine;
using Window;
using Window.Positioning;
using Utility.Logging;

public class HUDContentService : IHUDContentService, ICanvasChangedListener
{
    private HashSet<HUDManagedComponent> managedPrefabs;

    [SerializeField]
    private List<HUDManagedComponent> managedInstances;

    private ICanvasProvider canvasProvider;
    //TODO: Remove this and create additional component to assit HUDManagedComponent in creating this prefab correctly
    private IHUDModulesService HUDService;
    private WindowDistributionModule windowDistributionModule;
    //
    public static IHUDContentService CreateBasicService() => new HUDContentService();

    private IHUDManagedComponentProvider instanceProvider;

    private WindowPositionLogic windowPositioningLogic;
    public HUDContentService()
    {
        windowPositioningLogic = new WindowPositionLogic();
        IsReady = false;
        managedInstances = new List<HUDManagedComponent>();
        managedPrefabs = new HashSet<HUDManagedComponent>();
        GameServices.GetService(ReceiveCanvasService);
        GameServices.GetService(ReceiveHUDService);
    }

    public bool IsReady { get; private set; }

    public void AddManagedPrefab(HUDManagedComponent managedComponent)
    {
        if(managedPrefabs.Add(managedComponent) && IsReady)
        {
            if(managedComponent.IsAutoStarted)
            {
                CreateContent(managedComponent);
            }
        }
    }

    private void Autostart()
    {
        foreach (var prefab in managedPrefabs)
        {
            if(prefab.IsAutoStarted)
            {
                CreateContent(prefab);
            }
        }
    }

    private void TrySetReady()
    {
        if (canvasProvider != null && HUDService != null && windowDistributionModule != null)
        {
            IsReady = true;
            Autostart();
        }
    }

    //public void GetContentComponent<T>(out T content) where T : IHUDContentComponent
    //{
        ///should i activate content here?
        ///should this mono behaviour be somekind of interface? 
        ///do we need it as a interface? We shouldn't interact with it, 
        ///but should only interact via managed component, 
        ///but we still need way of differentianting

    //}

    private HUDManagedComponent CreateContent(HUDManagedComponent managedComponent)
    {
        //instantiate window, place it in canvas
        WindowComponent window = this.windowDistributionModule.GetWindow(managedComponent.WindowType.Id);
        //instantiate content, also place it in canvas
        HUDManagedComponent instance = instanceProvider.GetHUDManagedComponent(managedComponent);

        window.SetHeader(WindowHeaderData.CreateHeader("!TBD"));

        WindowContentComponent content = instance.GetComponent<WindowContentComponent>();

        if (content == null)
            Log.LogBaseError($"Window {instance} doesnt have WindowContentComponent.");

        window.SetContent(WindowContentData.CreateContent(instance.GetComponent<WindowContentComponent>()));

        if (instance.TryGetComponent(out IWindowPositioningComponent defaultPosition))
        {
            //there is a slight chance this is to early to call window transform, window probably didn't even had awake. Keep that in mind 
            windowPositioningLogic.SetupWindow(window.WindowTransform, defaultPosition.GetWindowPositionData(canvasProvider.Canvas));
        }
        else
        {
            Log.LogBaseError($"HUD instance {instance} is missing default position component, are you sure this is allright?", instance.transform);
        }

        managedInstances.Add(instance);
        return instance;
    }

    private bool ReceiveCanvasService(IService service)
    {
        if (service is ICanvasService canvasService)
        {
            canvasProvider = canvasService.GetCanvasProvider();
            canvasProvider.AddListener(this);
            instanceProvider = new HUDManagedComponentsInstantiator(canvasProvider.Canvas);
            TrySetReady();
            return true;
        }
        return false;
    }

    public void CanvasChanged(Canvas canvas)
    {
        instanceProvider.SetCanvas(canvasProvider.Canvas);
        //thi should be delegated somewhere else, where windows and other stuff is created
        //foreach (var instance in managedInstances)
        //{
        //    instance.transform.SetParent(canvas.transform);
        //}
    }

    private bool ReceiveHUDService(IService service)
    {
        if (service is IHUDModulesService HUDModulesService)
        {
            HUDService = HUDModulesService;
            HUDService.GetModule(ReceiveHUDModule);
            TrySetReady();
            return true;
        }
        return false;
    }

    private bool ReceiveHUDModule(object hudModule)
    {
        if (hudModule is WindowDistributionModule windowDistributionModule)
        {
            this.windowDistributionModule = windowDistributionModule;
            TrySetReady();
            return true;
        }

        return false;
    }
    ///think about why would we have cache of prefabs, collected by HUDManagedComponent but requested by some internal type? Why would we have collection at all,
    ///why don't we add it when we need it. Or why we need to take care of them here at all, why they don't leave in separate universes? Should we have collection of HUD modules to ever be 
    ///presented collected here so we can assign them to correct canvas and update them or access them easier?
}