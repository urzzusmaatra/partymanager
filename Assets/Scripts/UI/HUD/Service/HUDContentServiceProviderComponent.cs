﻿using Services;
using Services.Internal;
using System.Collections.Generic;
using UnityEngine;

public class HUDContentServiceProviderComponent : MonoBehaviour
{
    [SerializeField]
    private List<HUDManagedComponent> HUDContentsPrefabs;
    private void Awake()
    {
        var service = ServicesConfiguration.GetHUDContentService();

        GameServices.Provide(service);

    }

    private void Start()
    {
        var service = GameServices.GetService<IHUDContentService>();
        foreach (var prefab in HUDContentsPrefabs)
        {
            service.AddManagedPrefab(prefab);
        }
    }
}