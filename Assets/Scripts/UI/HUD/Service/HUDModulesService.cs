using System.Collections.Generic;
using UnityEngine;
using HUD;
using CanvasServices;
using System;
using Utility.Logging;

namespace Services.Internal
{
    public class HUDModulesService : IHUDModulesService, ICanvasChangedListener
    {
        private List<IHUDModule> _HUDModulesCollection;
        private ICanvasProvider canvasProvider;
        public bool IsReady { get; private set; }
        public HUDModulesService()
        {
            IsReady = false;
            _HUDModulesCollection = new List<IHUDModule>();
            GameServices.GetService(ReceiveService);
        }

        private bool ReceiveService(IService service)
        {
            if (service is ICanvasService canvasService)
            {
                canvasProvider = canvasService.GetCanvasProvider();
                canvasProvider.AddListener(this);
                SetReady();
                return true;
            }

            return false;
        }

        private void SetReady()
        {
            if (!IsReady)
            {
                IsReady = true;
                foreach (var HUDModule in _HUDModulesCollection)
                {
                    HUDModule.SetCanvas(canvasProvider.Canvas);
                    HUDModule.SetCamera(Camera.main);//same as canvas?
                    ProcessListeners(HUDModule);
                }
            }
        }

        public static IHUDModulesService CreateBasicService() => new HUDModulesService();

        public void AddHUDModule<T>(T newModule) where T : IHUDModule
        {
            var existingModule = GetModule<T>();
            if (existingModule != null)
            {
                Log.LogBaseError($"Ambiguous module {newModule} with existing {existingModule}. Please inspect.");
                return;
            }

            _HUDModulesCollection.Add(newModule);

            if (IsReady)
            {
                //this may not be the case when switching scenes
                newModule.SetCanvas(canvasProvider.Canvas);
                newModule.SetCamera(Camera.main);

                ProcessListeners(newModule);
            }
        }

        private HUDModuleAddedCallback subscriptionList;

        public void RegistrateOnHUDModuleAddedCallback(HUDModuleAddedCallback callbackFunc)
        {
            subscriptionList += callbackFunc;
        }

        private void ProcessListeners(object HUDModule)
        {
            if (subscriptionList == null) return;

            foreach (HUDModuleAddedCallback subscriber in subscriptionList?.GetInvocationList())
            {
                if (subscriber(HUDModule))
                {
                    subscriptionList -= subscriber;
                }
            }
        }

        public bool TryGetHudModule<T>(out T module) where T : IHUDModule
        {
            module = GetModule<T>();
            return module != null;
        }

        private T GetModule<T>() where T : IHUDModule
        {
            foreach (var HUDmodule in _HUDModulesCollection)
            {
                if (HUDmodule is T)
                {
                    return (T)HUDmodule;
                }
            }

            return default(T);
        }

        public void GetModule(HUDModuleAddedCallback receiveModuleFunc)
        {
            foreach (var HUDModules in _HUDModulesCollection)
            {
                if(receiveModuleFunc(HUDModules))
                {
                    return;
                }
            }

            subscriptionList += receiveModuleFunc;
        }

        public void CanvasChanged(Canvas canvas)
        {
            foreach (var HUDModule in _HUDModulesCollection)
            {
                HUDModule.SetCanvas(canvas);
            }
        }
    }
}