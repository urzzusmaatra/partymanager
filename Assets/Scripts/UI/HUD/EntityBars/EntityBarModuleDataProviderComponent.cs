﻿using Services;
using UnityEngine;

namespace PartyManager.Assets.Scripts.UI.HUD.EntityBars
{
    public abstract class EntityBarModuleDataProviderComponent : MonoBehaviour
    {
        public abstract IHUDModule GetModule { get; }
    }
}