﻿using Services;
using UnityEngine;

namespace PartyManager.Assets.Scripts.UI.HUD.EntityBars.HealthBar
{
    public class HealthBarModuleDataProviderComponent : MonoBehaviour
    {
        [SerializeField]
        private EntityBar healthBar;
        [SerializeField]
        private Vector2 offset;

        private void Start()
        {
            GameServices.GetService(ReceiveService);
        }

        private bool ReceiveService(IService service)
        {
            if (service is IHUDModulesService HUDService)
            {
                HUDService.AddHUDModule(new HealthBarHudModule(new HealthBarHUDData(healthBar, offset)));
                return true;
            }

            return false;
        }
    }
}