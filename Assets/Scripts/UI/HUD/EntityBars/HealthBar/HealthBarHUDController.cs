﻿using UnityEngine;
using System.Collections.Generic;
using PartyManager.Assets.Scripts.ActorEntity.Resources;
using PartyManager.Assets.Scripts.EntityResourceComponent;
using PartyManager.Assets.Scripts.Utility.Extensions;

namespace PartyManager.Assets.Scripts.UI.HUD.EntityBars.HealthBar
{
    public class HealthBarHUDController : MonoBehaviour
    {
        private Dictionary<HealthComponent, HealthBarHUDPackage> bars;
        public Vector2 Offset { private get; set; }
        private ResourceData latestResourceData;

        private void Awake()
        {
            bars = new Dictionary<HealthComponent, HealthBarHUDPackage>();
        }

        public void AddHealthObject(Transform subject, Transform bar)
        {
            if (subject.TryGetComponent(out HealthComponent healthComponent))
            {
                if (bar.TryGetComponent(out EntityBar entityBar))
                {
                    bars.Add(healthComponent, new HealthBarHUDPackage(entityBar, healthComponent));

                }
            }
        }

        public void RemoveHealthObject(Transform subject, out EntityBar bar)
        {
            bar = null;
            if (subject.TryGetComponent(out HealthComponent healthComponent))
            {
                bars.Remove(healthComponent, out HealthBarHUDPackage package);
                bar = package.Bar;
                package.Unsubscribe();
            }
        }

        private void LateUpdate()
        {
            foreach (var item in bars.Values)
            {
                item.MoveBar(Offset);
                item.UpdateFill();
            }
        }

        private class HealthBarHUDPackage
        {
            public EntityBar Bar { get; private set; }
            public HealthComponent HealthComponent { get; private set; }
            private bool updateFill = false;
            private ResourceData latestResourceData;
            public HealthBarHUDPackage(EntityBar bar, HealthComponent healthComponent)
            {
                Bar = bar;
                HealthComponent = healthComponent;
                if (HealthComponent != null)
                {
                    HealthComponent.OnResourceChanged += OnResourceChanged;
                }

                updateFill = false;
            }

            ~HealthBarHUDPackage()
            {
                Unsubscribe();
            }

            private void OnResourceChanged(ResourceData data)
            {
                latestResourceData = data;
                updateFill = true;
            }

            public void MoveBar(Vector2 offset)
            {
                Bar.transform.position = HealthComponent.transform.position + offset.ConvertVector();
            }

            public void UpdateFill()
            {
                if(updateFill)
                {
                    Bar.SetNormalizedValue(latestResourceData.currentAmmount / latestResourceData.maxAmmount);
                    updateFill = false;
                }
            }

            public void Unsubscribe()
            {
                if (HealthComponent != null)
                {
                    HealthComponent.OnResourceChanged -= OnResourceChanged;
                }
            }
        }
    }

}