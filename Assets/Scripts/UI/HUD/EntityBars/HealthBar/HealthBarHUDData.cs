﻿using UnityEngine;

namespace PartyManager.Assets.Scripts.UI.HUD.EntityBars.HealthBar
{
    public struct HealthBarHUDData
    {
        public EntityBar prefab;
        public Vector2 offset;

        public HealthBarHUDData(EntityBar prefab, Vector2 offset)
        {
            this.prefab = prefab;
            this.offset = offset;
        }
    }
}