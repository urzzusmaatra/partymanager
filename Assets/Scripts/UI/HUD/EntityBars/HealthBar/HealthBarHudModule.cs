﻿using UnityEngine;
using FrostSnail.SimpleObjectPool;
using FrostSnail.Events;
using Services;
using PartyManager.Assets.Scripts.UI.HUD.Events;
using PartyManager.Assets.Scripts.UI.Internal;

namespace PartyManager.Assets.Scripts.UI.HUD.EntityBars.HealthBar
{
    public class HealthBarHudModule : ObjectPool, IHUDModule
    {
        private EntityBar _prefab;
        private EventReceiver eventReceiver;
        private HealthBarHUDController controller;

        private Camera _camera;
        private Camera GetCamera
        {
            //#TODO get from some service
            get
            {
                if (_camera)
                    _camera = Camera.main;

                return _camera;
            }
        }

        public HealthBarHudModule(HealthBarHUDData data)
        {
            _prefab = data.prefab;
            eventReceiver = EventReceiverFactory.CreateEventReceiver(UIEventChannel.Channel);
            eventReceiver.RegisterMethod<ActorAddedUIEvent>(ActorAddedUIEventHandler);
            eventReceiver.RegisterMethod<ActorRemovedUIEvent>(ActorRemovedUIEventHandler);

            var controllerGameObject = new GameObject("HealthBarHUDController");
            controller = controllerGameObject.AddComponent<HealthBarHUDController>();
            controller.Offset = data.offset;
        }

        ~HealthBarHudModule()
        {
            eventReceiver.UnregisterAll();
        }

        private void ActorAddedUIEventHandler(ActorAddedUIEvent myEvent)
        {
            controller.AddHealthObject(myEvent.Actor.transform, GetEntityBar.transform);
        }

        private void ActorRemovedUIEventHandler(ActorRemovedUIEvent myEvent)
        {
            controller.RemoveHealthObject(myEvent.Actor.transform, out EntityBar bar);
            Deposit(bar);
        }

        protected override IPoolableObject Create()
        {
            var bar = Object.Instantiate(_prefab);
            bar.GetCanvas.worldCamera = GetCamera;
            return bar;
        }

        public void SetCanvas(Canvas canvas) { }

        public void SetCamera(Camera camera) => _camera = camera;

        private EntityBar GetEntityBar => (EntityBar)Withdraw();
    }
}