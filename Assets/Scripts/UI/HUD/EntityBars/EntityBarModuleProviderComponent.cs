﻿using Services;
using UnityEngine;

namespace PartyManager.Assets.Scripts.UI.HUD.EntityBars
{
    public class EntityBarModuleProviderComponent : MonoBehaviour
    {
        [SerializeField]
        private EntityBar _entityBar;

        void Start()
        {
            GameServices.GetService(ReceiveService);
        }

        private bool ReceiveService(IService service)
        {
            if (service is IHUDModulesService HUDService)
            {
                foreach (var component in transform.GetComponents<EntityBarModuleDataProviderComponent>())
                {
                    HUDService.AddHUDModule(component.GetModule);
                }
                return true;
            }

            return false;
        }
    }
}