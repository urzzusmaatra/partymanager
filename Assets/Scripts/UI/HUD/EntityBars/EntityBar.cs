﻿using UnityEngine;
using UnityEngine.UI;
using HUD;
using FrostSnail.SimpleObjectPool;

namespace PartyManager.Assets.Scripts.UI.HUD.EntityBars
{
    public class EntityBar : MonoBehaviour, IValueBar, IHaveCanvas, IPoolableObject
    {
        public Canvas GetCanvas => _canvas;

        [SerializeField]
        private RectTransform _bar;
        [SerializeField]
        private Image _fillImage;
        [SerializeField]
        private Canvas _canvas;
        private const float FULL_BAR = 1F;

        public void SetNormalizedValue(float value)
        {
            _bar.sizeDelta = Vector2.right * Mathf.Clamp01(value);
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }

        public void ResetObject()
        {
            SetNormalizedValue(FULL_BAR);
        }
    }
}