﻿using FrostSnail.Factions;

namespace Assets.Scripts.UI.AbilityHUDSettings
{
    //TODO rename to something more suting, like FactionSelectionDisplayDataContext
    public struct FactionDataContext
    {
        public string PropertyName { get; set; }
        public byte Faction { get; set; }
        public System.Action<Faction> FactionChangeHandler { get; set; }
    }
}