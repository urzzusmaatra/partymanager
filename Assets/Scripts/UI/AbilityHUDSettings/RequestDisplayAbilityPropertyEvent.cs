﻿using AbilitySpace;
using FrostSnail.Events;

namespace Assets.Scripts.UI.AbilityHUDSettings
{
    public abstract class RequestDisplayAbilityPropertyEvent : EventData
    {
        public IAbility Ability {get; set;}
        public string PropertyName { get; set; }
    }
}