﻿using UnityEngine;

namespace Assets.Scripts.UI.AbilityHUDSettings
{
    public class AbilityHUDDisplaySettingsContentProvider : MonoBehaviour
    {
        [SerializeField]
        private AbilityHUDDisplaySettingsContent content;
        public AbilityHUDDisplaySettingsContent GetContent
        {
            get
            {
                if (content == null)
                    content = GetComponent<AbilityHUDDisplaySettingsContent>();

                return content;
            }
        }

    }
}