﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.AbilityHUDSettings
{
    public class AbilityHUDDisplaySettingsContent : MonoBehaviour
    {
        [SerializeField]
        private TMPro.TMP_Text text;
        [SerializeField]
        private Image image;
        internal void SetData(AbilityHUDDisplaySettingsData displaySettingData)
        {
            text.SetText(displaySettingData.AbilityName);
            image.sprite = displaySettingData.HUDImage;
        }
    }
}