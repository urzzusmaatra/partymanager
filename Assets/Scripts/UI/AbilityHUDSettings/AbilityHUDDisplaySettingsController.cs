using System.Collections.Generic;
using UnityEngine;
using FrostSnail.Events;
using AbilitySpace;
using AbilitySpace.Property;
using Utility.Logging;
using PartyManager.Assets.Scripts.UI.Internal;
using PartyManager.Assets.Scripts.UI.Ability;

namespace Assets.Scripts.UI.AbilityHUDSettings
{
    [RequireComponent(typeof(IEventChannel))]
    public class AbilityHUDDisplaySettingsController : HUDManagedComponent
    {
        //this is just a controller, on part of the ability settings layer cake
        private EventReceiver eventHandler;
        private IEventChannel eventComponent;
        private AbilityHUDDisplaySettingsContentProvider contentProvider;
        private IAbility displayedAbility;
        [SerializeField]
        private AbilityUIRepository abilityUIRepositiory;

        private void Awake()
        {
            contentProvider = GetComponent<AbilityHUDDisplaySettingsContentProvider>();
        }

        private void Start()
        {
            if (!TryGetComponent(out eventComponent))
            {
                Log.PushError(UILogChannel.NAME, "No avaliable event component!", transform);
            }
            eventHandler = EventReceiverFactory.CreateEventReceiver();
            eventHandler.RegisterMethod<RequestAbilitySettingDisplayEvent>(RequestAbilitySettingDisplayEventCallback);
            eventHandler.RegisterComponent(UIEventChannelComponent.GetChannel);
        }

        public void RequestAbilitySettingDisplayEventCallback(RequestAbilitySettingDisplayEvent eventData)
        {
            IAbility requestedAbility = eventData.Ability;
            if (displayedAbility != requestedAbility)
            {
                displayedAbility = requestedAbility;
                DisplayAbility(eventData.Ability);
            }
        }

        private void DisplayAbility(IAbility ability)
        {
            //IAbility needs to provide access to properties i.e. faction selection
            GameObject abilityGameObject = ability.GetGameObject;
            AbilityHUDDisplaySettingsData displaySettingData = new AbilityHUDDisplaySettingsData();

            if (abilityUIRepositiory == null)
            {
                UILogChannel.LogError("Cannot display ability. Ability UI Repository is empty!", transform);
                return;
            }

            var abilityUI = abilityUIRepositiory.GetAbilityUIResource(ability.Name);
            if (abilityUI == null)
            {
                UILogChannel.LogError($"Cannot display ability. No ability data in repository for ability {ability.Name}", transform);
                return;
            }
            displaySettingData.HUDImage = abilityUI.AbilityHUDImage;
            displaySettingData.AbilityName = ability.Name;
            contentProvider.GetContent.SetData(displaySettingData);

            foreach (var propertyAcessor in abilityGameObject.GetComponents<AbilityPropertyAccessorComponent>())
            {
                if(propertyAcessor is RangeAbilityPropertyAccessorComponent rangePropertyAccessor)
                {
                    EventFactory.GetEvent(out RequestDisplayRangeAbilityPropertyEvent rangeEvent);
                    rangeEvent.Ability = ability;
                    rangeEvent.PropertyName = rangePropertyAccessor.PropertyName;
                    rangeEvent.Range = rangePropertyAccessor.GetCoverageArea;
                    rangeEvent.RangeChangeHandler = rangePropertyAccessor.SetCoverageArea;
                    rangeEvent.SendTo(eventComponent);
                }
                else if(propertyAcessor is FactionAbilityPropertyAccessorComponent factionPropertyAccessor)
                {
                    EventFactory.GetEvent(out RequestDisplayFactionAbilityPropertyEvent factionEvent);
                    factionEvent.Ability = ability;
                    factionEvent.PropertyName = factionPropertyAccessor.PropertyName;
                    //factionEvent.Faction = factionPropertyAccessor.Faction;
                    factionEvent.FactionChangeHandler = factionPropertyAccessor.ChangeFaction;
                    factionEvent.SendTo(eventComponent);
                }
            }

        }
    }
}