﻿using FrostSnail.Events;

namespace Assets.Scripts.UI.AbilityHUDSettings
{
    public class RequestAbilitySettingDisplayEvent : EventData
    {
        public AbilitySpace.IAbility Ability { get; set; }
    }
}