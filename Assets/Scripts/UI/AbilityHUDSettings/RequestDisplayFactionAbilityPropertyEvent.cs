﻿using FrostSnail.Factions;

namespace Assets.Scripts.UI.AbilityHUDSettings
{
    public class RequestDisplayFactionAbilityPropertyEvent : RequestDisplayAbilityPropertyEvent
    {
        public byte Faction { get; set; }
        public System.Action<Faction> FactionChangeHandler { get; set; }
    }
}