﻿using UnityEngine;
using Window;
using FrostSnail.Coverage;

namespace Assets.Scripts.UI.AbilityHUDSettings
{
    public class AbilityRangeSettingDisplayHUDComponent : MonoBehaviour, IDispayableObjectController
    {
        [SerializeField]
        private TMPro.TMP_Text title;
        [SerializeField]
        private UnityEngine.UI.Slider minSlider;
        [SerializeField]
        private TMPro.TMP_Text minSliderCurrentValueDisplay;
        [SerializeField]
        private UnityEngine.UI.Slider maxSlider;
        [SerializeField]
        private TMPro.TMP_Text maxSliderCurrentValueDisplay;

        private System.Action<float, float> RangeChangeHandler { get; set; }
        private CoverageArea area;

        public void DisplayObject(object objectToDisplay)
        {
            if(objectToDisplay is RangeDataContext rangeData)
            {
                title.SetText(rangeData.PropertyName);

                area = rangeData.Range;

                SetBaseValues(ref minSlider, area);
                minSlider.value = area.CurrentMin;
                minSlider.onValueChanged.AddListener(MinValueChanged);
                minSliderCurrentValueDisplay.SetText(area.CurrentMin.ToString());

                SetBaseValues(ref maxSlider, area);
                maxSlider.value = area.CurrentMax;
                maxSlider.onValueChanged.AddListener(MaxValueChanged);
                maxSliderCurrentValueDisplay.SetText(area.CurrentMax.ToString());

                RangeChangeHandler = rangeData.RangeChangeHandler;
            }
        }

        private void SetBaseValues(ref UnityEngine.UI.Slider slider, CoverageArea area)
        {
            slider.minValue = area.BaseMin;
            slider.maxValue = area.BaseMax;
        }

        private void MinValueChanged(float value)
        {
            area.CurrentMin = value;
            RangeChangeHandler(area.CurrentMin, area.CurrentMax);
            minSliderCurrentValueDisplay.SetText(value.ToString());
        }

        private void MaxValueChanged(float value)
        {
            area.CurrentMax = value;
            RangeChangeHandler(area.CurrentMin, area.CurrentMax);
            maxSliderCurrentValueDisplay.SetText(value.ToString());
        }

        public void RemoveObject()
        {
            RangeChangeHandler = null;
        }

        //hosting allways false to convey a message of allways refreshing
        public bool IsHostingObject() => false;
        public bool IsHostingSpecificObject(object queryObject) => false;

        
    }


}