﻿using FrostSnail.Coverage;

namespace Assets.Scripts.UI.AbilityHUDSettings
{
    public struct RangeDataContext
    {
        public string PropertyName { get; set; }
        public CoverageArea Range { get; set; }
        public System.Action<float, float> RangeChangeHandler { get; set; }
    }


}