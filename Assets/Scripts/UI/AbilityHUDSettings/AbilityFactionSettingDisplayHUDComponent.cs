﻿using FactionServices;
using FrostSnail.Factions;
using Services;
using System.Collections.Generic;
using UnityEngine;
using Window;
using Utility.Logging;

namespace Assets.Scripts.UI.AbilityHUDSettings
{
    public class AbilityFactionSettingDisplayHUDComponent : MonoBehaviour, IDispayableObjectController
    {
        [SerializeField]
        private TMPro.TMP_Text title;
        [SerializeField]
        private TMPro.TMP_Dropdown factionDropdown;
        private System.Action<Faction> valueChangeHandler;
        private byte currentFaction;
        private IFactionService factionService;
        private uint[] factionIds;

        public void DisplayObject(object objectToDisplay)
        {
            if(objectToDisplay is FactionDataContext factionData)
            {
                currentFaction = factionData.Faction;
                title.SetText(factionData.PropertyName);
                valueChangeHandler = factionData.FactionChangeHandler;
                GameServices.GetService(ReceiveService);
                factionDropdown.onValueChanged.AddListener(OnFactionChanged);
            }
        }

        public void RemoveObject()
        {
            factionService = null;
            valueChangeHandler = null;
        }

        //hosting allways false to convey a message of allways refreshing
        public bool IsHostingObject() => false;

        public bool IsHostingSpecificObject(object queryObject) => false;

        private void OnFactionChanged(int newSelection)
        {
            //currentFaction = factionService.GetFactionById(factionIds[newSelection]);//they should be ordered correctly
            //Log.LogBaseWarning("Disabled return value on faction set. don't forget to enable it when you create faction selection UI.");
            ////valueChangeHandler(currentFaction);
        }

        private bool ReceiveService(IService service)
        {
            //if (service is IFactionService factionService)
            //{
            //    this.factionService = factionService;
            //    factionDropdown.AddOptions(new List<string>(factionService.GetFactionOptions()));
            //    factionIds = factionService.GetFactionIds();

            //    //fins faction by id and set that as starting choice in dropdown enumerated in single incremental order unlike factions
            //    int i = 0;
            //    foreach (var factionId in factionIds)
            //    {
            //        if(factionService.CompareFaction(factionService.GetFactionById(factionId), currentFaction))
            //        {

            //            factionDropdown.SetValueWithoutNotify(i);
            //            break;
            //        }
            //        i++;
            //    }
            //    return true;
            //}

            return false;
        }
    }
}