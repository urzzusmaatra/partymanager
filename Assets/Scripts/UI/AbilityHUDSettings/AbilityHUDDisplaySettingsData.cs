﻿using UnityEngine;

namespace Assets.Scripts.UI.AbilityHUDSettings
{
    public struct AbilityHUDDisplaySettingsData
    {
        public Sprite HUDImage;//maybe to encapsulate this?
        public string AbilityName;
    }
}