﻿using FrostSnail.Coverage;

namespace Assets.Scripts.UI.AbilityHUDSettings
{
    public class RequestDisplayRangeAbilityPropertyEvent : RequestDisplayAbilityPropertyEvent
    {
        public CoverageArea Range { get; set; }
        public System.Action<float, float> RangeChangeHandler { get; set; }
    }
}