﻿using UnityEngine;
using FrostSnail.Events;
using Window;
using Window.Internal;
using System.Collections.Generic;
using AbilitySpace;
using FrostSnail.Factions;
using Utility.Logging;

namespace Assets.Scripts.UI.AbilityHUDSettings
{
    [RequireComponent(typeof(IDisplayArrayController))]
    [RequireComponent(typeof(IEventChannel))]
    public class AbilitySettingsContentArray : MonoBehaviour, IDisplayArrayPrefabProvider, IHUDDisplayArrayStrategy
    {
        private EventReceiver eventHandler;
        private IDisplayArrayController displayArrayController;
        private IAbility displayedAbility;
        [SerializeField]
        private DisplayableObjectComponent rangeDisplayPrefab;//myabe not DisplayableObjectComponent but something relatable to this?
        [SerializeField]
        private DisplayableObjectComponent factionDisplayPrefab;
        private DisplayableObjectComponent currentDisplayPrefab;

        private List<IDispayableObjectController> displayedSettingInstances;
        private const int PREDICTED_NUMBER_OF_INSTANCES = 4;
        public void Destroy()
        {
            ReleaseInstances();
        }

        public DisplayableObjectComponent GetPrefab() => currentDisplayPrefab;

        private void Start()
        {
            displayedSettingInstances = new List<IDispayableObjectController>(PREDICTED_NUMBER_OF_INSTANCES);
            EventReceiverFactory.CreateEventHandler(out eventHandler, gameObject);
            displayArrayController = GetComponent<IDisplayArrayController>();

            eventHandler.RegisterMethod<RequestDisplayRangeAbilityPropertyEvent>(RequestDisplayRangeAbilityPropertyEventCallback);
            eventHandler.RegisterMethod<RequestDisplayFactionAbilityPropertyEvent>(RequestDisplayFactionAbilityPropertyEventCallback);
        }

        //implement listeners here
        private void RequestDisplayRangeAbilityPropertyEventCallback(RequestDisplayRangeAbilityPropertyEvent rangeEvent)
        {
            currentDisplayPrefab = rangeDisplayPrefab;

            if (TryChangeDisplayedAbility(rangeEvent.Ability))
            {
                ReleaseInstances();
            }

            var dataContext = new RangeDataContext();
            dataContext.PropertyName = rangeEvent.PropertyName;
            dataContext.Range = rangeEvent.Range;
            Log.LogBaseWarning("Reminder to check if this is copy is legit.");
            dataContext.RangeChangeHandler = new System.Action<float, float>((System.Action<float, float>)rangeEvent.RangeChangeHandler.Clone());

            GetDisplayableObject.DisplayObject(dataContext);
        }

        private void RequestDisplayFactionAbilityPropertyEventCallback(RequestDisplayFactionAbilityPropertyEvent factionEvent)
        {
            currentDisplayPrefab = factionDisplayPrefab;

            if (TryChangeDisplayedAbility(factionEvent.Ability))
            {
                ReleaseInstances();
            }

            var dataContext = new FactionDataContext();
            dataContext.PropertyName = factionEvent.PropertyName;
            dataContext.Faction = factionEvent.Faction;
            Log.LogBaseWarning("Reminder to check if this is copy is legit.");
            dataContext.FactionChangeHandler = new System.Action<Faction>((System.Action<Faction>)factionEvent.FactionChangeHandler);

            GetDisplayableObject.DisplayObject(dataContext);
        }

        private IDispayableObjectController GetDisplayableObject
        {
            get
            {
                IDispayableObjectController displayableObject = displayArrayController.GetDisplayableObject();
                displayedSettingInstances.Add(displayableObject);
                return displayableObject;
            }
        }

        private bool TryChangeDisplayedAbility(IAbility ability)
        {
            if(displayedAbility != ability)
            {
                displayedAbility = ability;
                return true;
            }
            return false;
        }

        private void ReleaseInstances()
        {
            if (displayedSettingInstances != null)
            {
                foreach (var instance in displayedSettingInstances)
                {
                    displayArrayController.ReturnDisplayableObject(instance);
                }
                displayedSettingInstances.Clear();
            }
        }
    }

}