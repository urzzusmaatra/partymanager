﻿using LiveSelection;
using LiveSelection.Service;
using Services;
using UnityEngine;
using UnityEngine.UI;
using Window;
using Utility.Logging;
using PartyManager.Assets.Scripts.ActorEntity;

public class ActorDisplayHUDComponent : MonoBehaviour, IDispayableObjectController
{
    [SerializeField]
    private Image actorImage;

    private Actor actor;
    private SelectableComponent selectableComponentOnActor;

    private ISelectableObjectService selectableService;

    private void Start()
    {
        GameServices.GetService(ReceiveService);
    }

    private void AttachActor(Actor actor)
    {
        this.actor = actor;
        var visuals = actor.GetComponent<VisualsHandlingComponent>();
        actorImage.sprite = visuals.CurrentSprite;

        selectableComponentOnActor = actor.GetComponentInChildren<SelectableComponent>();
    }

    private bool IsHostingActor(Actor actor) => this.actor == actor;

    public void OnEntityButtonPressed()
    {
        if(selectableComponentOnActor == null)
        {
            Log.LogBaseError($"NULL selectable service on ActorDisplayHUDComponent {this}");
        }
        selectableService.RequestSelection(selectableComponentOnActor);
    }

    private bool ReceiveService(IService service)
    {
        if (service is ISelectableObjectService selectableService)
        {
            this.selectableService = selectableService;
            return true;
        }
        return false;
    }

    public void DisplayObject(object objectToDisplay)
    {
        if(objectToDisplay is Actor actor)
        {
            AttachActor(actor);
        }
    }

    public void RemoveObject()
    {
        selectableComponentOnActor = null;
        actor = null;

    }

    public bool IsHostingObject()
    {
        return actor != null;
    }

    public bool IsHostingSpecificObject(object queryObject)
    {
        if(IsHostingObject())
        {
            if(queryObject is Actor actor)
            {
                return IsHostingActor(actor);
            }
        }

        return false;
    }
}