﻿using UnityEngine;
using Window;

namespace PartyManager.Assets.Scripts.UI.ActorHUDDisplayArray
{
    [CreateAssetMenu(fileName = "ActorsHeaderHUDArrayDisplayStrategyProvider", menuName = "Actor/HeaderHUDStrategyProvider")]
    public class ActorsHeaderHUDArrayDisplayStrategyProvider : HUDDisplayArrayStrategyProvider
    {
        public override IHUDDisplayArrayStrategy GetStrategy(IDisplayArrayController displayArrayController)
        {
            return new ActorsHeaderHUDArrayDisplayStrategy(displayArrayController);
        }
    }
}