﻿using System.Collections.Generic;
using Services;
using Window;
using FactionServices;
using FrostSnail.Factions;
using Utility.Logging;
using FrostSnail.Events;
using PartyManager.Assets.Scripts.EventChannels;
using PartyManager.Assets.Scripts.Entity.Events;
using PartyManager.Assets.Scripts.ActorEntity;

namespace PartyManager.Assets.Scripts.UI.ActorHUDDisplayArray
{
    public class ActorsHeaderHUDArrayDisplayStrategy : IHUDDisplayArrayStrategy
    {
        private IFactionService factionService;
        private Faction displayFaction;
        private List<IDispayableObjectController> entityList;
        private IDisplayArrayController displayArrayController;
        private EventReceiver eventReceiver;

        public ActorsHeaderHUDArrayDisplayStrategy(IDisplayArrayController displayArrayController)
        {
            GameServices.GetService(ReceiveFactionService);
            entityList = new List<IDispayableObjectController>();
            this.displayArrayController = displayArrayController;

            eventReceiver = EventReceiverFactory.CreateEventReceiver(GameplayEventChannel.Channel);
            eventReceiver.RegisterMethod<ActorAddedEvent>(ActorAddedEventHandler);
            eventReceiver.RegisterMethod<ActorRemovedEvent>(ActorRemovedEventHandler);
        }

        public void Destroy()
        {
            eventReceiver.UnregisterComponent(GameplayEventChannel.Channel);
        }

        private void ActorAddedEventHandler(ActorAddedEvent myEvent)
        {
            Actor actor = myEvent.Actor;

            if (actor.GetFaction != displayFaction)
                return;

            foreach (var entityComponent in entityList)
            {
                if (entityComponent.IsHostingSpecificObject(actor))
                {
                    Log.LogBaseError($"Adding actor:{actor} which is allread displayed in {this}.");
                    return;
                }
            }

            IDispayableObjectController displayableObject = displayArrayController.GetDisplayableObject();
            displayableObject.DisplayObject(actor);
            entityList.Add(displayableObject);
        }

        private void ActorRemovedEventHandler(ActorRemovedEvent myEvent)
        {
            Actor actor = myEvent.Actor;

            for (int i = entityList.Count - 1; i >= 0; i--)
            {
                var entityComponent = entityList[i];
                if (entityComponent.IsHostingSpecificObject(actor))
                {
                    displayArrayController.ReturnDisplayableObject(entityComponent);
                    entityList.RemoveAt(i);
                    break;
                }
            }
        }

        private bool ReceiveFactionService(IService service)
        {
            if (service is IFactionService factionService)
            {
                this.factionService = factionService;
                //this.displayFaction = this.factionService.PlayersFaction;
                return true;
            }
            return false;
        }

    }
}