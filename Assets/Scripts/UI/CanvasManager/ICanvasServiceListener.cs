﻿using UnityEngine;

namespace CanvasServices.Internal
{
    public interface ICanvasServiceListener
    {
        void ChangeCanvas(Canvas canvas);
    }
}