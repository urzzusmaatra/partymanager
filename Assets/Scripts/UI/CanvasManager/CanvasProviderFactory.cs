﻿using UnityEngine;

namespace CanvasServices.Internal
{
    public static class CanvasProviderFactory
    {
        public static CanvasProvider CreateBasicCanvasProvider(Canvas canvas)
        {
            return new CanvasProvider(canvas);
        }
    }
}