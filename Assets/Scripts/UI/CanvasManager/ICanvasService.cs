using CanvasServices.Internal;
using Services;
using UnityEngine;

namespace CanvasServices
{
    public interface ICanvasService : IService
    {
        public Canvas GetCanvas();
        CanvasProvider GetCanvasProvider();
    }
}