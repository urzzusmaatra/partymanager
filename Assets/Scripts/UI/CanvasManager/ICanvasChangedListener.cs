﻿using UnityEngine;

namespace CanvasServices
{
    public interface ICanvasChangedListener
    {
        void CanvasChanged(Canvas canvas);
    }
}
