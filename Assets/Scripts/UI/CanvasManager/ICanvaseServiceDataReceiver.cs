﻿using UnityEngine;

namespace CanvasServices.Internal
{
    public interface ICanvaseServiceDataReceiver
    {
        public void SetCanvas(Canvas canvas);
    }
}