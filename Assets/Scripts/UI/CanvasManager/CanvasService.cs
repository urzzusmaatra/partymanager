using CanvasServices;
using CanvasServices.Internal;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace CanvasServices.Internal
{
    public class CanvasService : ICanvasService, ICanvaseServiceDataReceiver
    {
        private Canvas canvas;
        private List<ICanvasServiceListener> listeners;
        public static ICanvasService CreateBasicService() => new CanvasService();

        public CanvasService()
        {
            listeners = new List<ICanvasServiceListener>();
        }

        public Canvas GetCanvas() => canvas;

        public void SetCanvas(Canvas canvas)
        {
            if(this.canvas != canvas && this.canvas != null)
            {
                foreach (var listener in listeners)
                {
                    listener.ChangeCanvas(canvas);
                }
            }
            this.canvas = canvas;
        }

        public CanvasProvider GetCanvasProvider() => CanvasProviderFactory.CreateBasicCanvasProvider(canvas);
    }
}