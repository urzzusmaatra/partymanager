using System;
using System.Collections.Generic;
using UnityEngine;

namespace CanvasServices.Internal
{
    public class CanvasProvider : ICanvasProvider, ICanvasServiceListener
    {
        private Canvas canvas;
        private List<ICanvasChangedListener> listeners;
        public CanvasProvider(Canvas canvas)
        {
            ChangeCanvas(canvas);
            listeners = new List<ICanvasChangedListener>();        
        }

        ~CanvasProvider()
        {

        }

        public Canvas Canvas => canvas;

        public void AddListener(ICanvasChangedListener listener) => listeners.Add(listener);

        public void RemoveListener(ICanvasChangedListener listener) => listeners.Remove(listener);

        public void ChangeCanvas(Canvas canvas)
        {
            if(this.canvas != canvas && this.canvas != null)
            {
                foreach (var listener in listeners)
                {
                    listener.CanvasChanged(canvas);
                }
            }

            this.canvas = canvas;
        }

    }
}