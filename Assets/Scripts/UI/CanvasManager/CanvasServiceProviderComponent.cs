using Services;
using Services.Internal;
using UnityEngine;

namespace CanvasServices.Internal
{
    public class CanvasServiceProviderComponent : MonoBehaviour
    {
        [SerializeField]
        private Canvas canvas;
        private void Awake()
        {
            var service = ServicesConfiguration.GetCanvasService();
            ((ICanvaseServiceDataReceiver)service).SetCanvas(canvas);
            GameServices.Provide(service);
        }
    }
}