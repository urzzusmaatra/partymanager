﻿using UnityEngine;

namespace CanvasServices
{
    public interface ICanvasProvider
    {
        Canvas Canvas { get; }
        void AddListener(ICanvasChangedListener listener);
        void RemoveListener(ICanvasChangedListener listener);
    }
}
