﻿using UnityEngine;
using Window.Internal;

namespace Window
{
    public interface IDisplayArrayObjectProvider
    {
        DisplayableObjectComponent GetDisplayableArrayObject(Canvas parentCanvas);

        void ReturnObject(DisplayableObjectComponent displayableArrayObject);
    }
}