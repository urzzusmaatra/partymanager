﻿using UnityEngine;

namespace Window
{
    public interface IDispayableObjectController
    {
        void DisplayObject(object objectToDisplay);
        void RemoveObject();

        bool IsHostingObject();

        bool IsHostingSpecificObject(object queryObject);
    }
}