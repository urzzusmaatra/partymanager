﻿using System.Collections.Generic;
using UnityEngine;
namespace Window
{
    using Internal;
    [CreateAssetMenu(fileName = "MiddleOutArangementStrategy", menuName = "Window/Arrangement Strategy/Middle Out")]
    public class MiddleOutArangementStrategy : BaseArrangementStrategy
    {
        [SerializeField]
        private Vector2 orderDirection = Vector2.left;

        public override void ArrangeObjects(DisplayArrayComponent parent, List<DisplayableObjectComponent> children)
        {
            var childrenCount = children.Count;
            var startingMultiplier = (childrenCount - 1) * .5f;
            for (int i = 0; i < childrenCount; i++)
            {
                var child = children[i];
                var displacement = MultiplyVectors(child.RectTransform.rect.size, orderDirection);
                var startPos = MultiplyVectors(child.RectTransform.rect.size * startingMultiplier, orderDirection);
                child.RectTransform.SetParent(parent.GetComponent<RectTransform>());
                //from middle
                child.RectTransform.anchoredPosition = startPos + (displacement * i * -1f);
            }
        }

        private Vector2 MultiplyVectors(Vector2 a, Vector2 b)
        {
            return new Vector2(a.x * b.x, a.y * b.y);
        }
    }
}