using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Window.Internal
{
    public abstract class BaseArrangementStrategy : ScriptableObject
    {
        public abstract void ArrangeObjects(DisplayArrayComponent parent, List<DisplayableObjectComponent> children);
    }
}