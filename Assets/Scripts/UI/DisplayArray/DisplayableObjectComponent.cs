﻿using System;
using UnityEngine;

namespace Window.Internal
{
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(IDispayableObjectController))]
    public class DisplayableObjectComponent : MonoBehaviour
    {
        public RectTransform RectTransform => rectTransform;

        private RectTransform rectTransform;
        public IDispayableObjectController DispayableObjectController { get; private set; }

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            DispayableObjectController = GetComponent<IDispayableObjectController>();
        }
    }
}