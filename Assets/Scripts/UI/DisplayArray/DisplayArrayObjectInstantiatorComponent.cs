﻿using UnityEngine;

namespace Window
{
    using Internal;

    public class DisplayArrayObjectInstantiatorComponent : DisplayArrayObjectInstantiator
    {
        [SerializeField]
        private DisplayableObjectComponent prefab;

        protected override DisplayableObjectComponent GetPrefab() => prefab;
    }
}