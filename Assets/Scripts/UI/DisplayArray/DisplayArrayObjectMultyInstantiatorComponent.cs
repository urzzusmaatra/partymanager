﻿using UnityEngine;

namespace Window
{
    using Internal;

    [RequireComponent(typeof(IDisplayArrayPrefabProvider))]
    public class DisplayArrayObjectMultyInstantiatorComponent : DisplayArrayObjectInstantiator
    {
        private IDisplayArrayPrefabProvider prefabProvider;

        private void Awake()
        {
            prefabProvider = GetComponent<IDisplayArrayPrefabProvider>();
        }
        protected override DisplayableObjectComponent GetPrefab() => prefabProvider.GetPrefab();
    }
}