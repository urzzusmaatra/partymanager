using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Window.Internal
{
    [RequireComponent(typeof(IDisplayArrayObjectProvider))]
    public class DisplayArrayComponent : MonoBehaviour
    {
        private IDisplayArrayObjectProvider provider;
        private List<DisplayableObjectComponent> displayableArray;
        [SerializeField, FormerlySerializedAs("strategy")]
        private BaseArrangementStrategy displayArrangementStrategy;

        private void Awake()
        {
            provider = GetComponent<IDisplayArrayObjectProvider>();
            displayableArray = new List<DisplayableObjectComponent>();
        }

        public DisplayableObjectComponent AddNew(Canvas parentCanvas)
        {
            var displayable = provider.GetDisplayableArrayObject(parentCanvas);
            displayableArray.Add(displayable);
            displayArrangementStrategy.ArrangeObjects(this, displayableArray);
            return displayable;
        }

        public void PopObject(DisplayableObjectComponent displayableArrayObject)
        {
            displayableArray.Remove(displayableArrayObject);
            displayArrangementStrategy.ArrangeObjects(this, displayableArray);
            provider.ReturnObject(displayableArrayObject);
        }
    }
}