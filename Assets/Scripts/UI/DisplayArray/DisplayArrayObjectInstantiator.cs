﻿using UnityEngine;

namespace Window.Internal
{
    public abstract class DisplayArrayObjectInstantiator : MonoBehaviour, IDisplayArrayObjectProvider
    {
        protected abstract DisplayableObjectComponent GetPrefab();
        public DisplayableObjectComponent GetDisplayableArrayObject(Canvas parentCanvas)
        {
            return Instantiate(GetPrefab(), parentCanvas.transform);
        }

        public void ReturnObject(DisplayableObjectComponent displayableArrayObject)
        {
            Destroy(displayableArrayObject.gameObject);
        }
    }
}