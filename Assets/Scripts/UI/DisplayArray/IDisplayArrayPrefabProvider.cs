﻿namespace Window
{
    using Internal;

    public interface IDisplayArrayPrefabProvider
    {
        DisplayableObjectComponent GetPrefab();
    }
}