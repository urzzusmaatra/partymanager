using LiveSelection.Service;
using LiveSelection;
using Services;
using System.Collections.Generic;
using UnityEngine;
using Window;

namespace UI.EntityDisplay
{
    [RequireComponent(typeof(WindowContentComponent))]
    public class EntityDetailsHUDDisplayComponent : MonoBehaviour, ISelectableObjectListener
    {
        private object displayedObject = null;
        private ISelectableObjectService selectableObjectService;
        [SerializeField]
        private List<EntityDetailDisplayComponent> entityDisplayDetailsCollection;
        [SerializeField]
        private WindowContentComponent window;
        private void Start()
        {
            GameServices.GetService(ReceiveSelectableService);
        }

        private bool ReceiveSelectableService(object service)
        {
            if (service is ISelectableObjectService selectableObjectService)
            {
                this.selectableObjectService = selectableObjectService;
                this.selectableObjectService.AddListener(this);
                return true;
            }
            return false;
        }

        private void OnDestroy()
        {
            if (this.selectableObjectService != null)
            {
                this.selectableObjectService.RemoveListener(this);
            }
        }

        public void OnSelectedObjectChanged(SelectableComponent previous, SelectableComponent current)
        {
            bool displayAccepted = false;
            foreach (var display in entityDisplayDetailsCollection)
            {
                display.Hide();
                if (!displayAccepted)
                {
                    displayAccepted = display.TryHandleSelectable(current);

                    if (displayAccepted)
                        display.Show();
                }

            }
        }
    }
}