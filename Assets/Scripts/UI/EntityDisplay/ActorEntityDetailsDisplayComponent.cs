﻿using UnityEngine;
using LiveSelection;
using TMPro;
using UI.HUD.Bar;
using PartyManager.Assets.Scripts.ActorEntity;
using PartyManager.Assets.Scripts.EntityResourceComponent;

namespace UI.EntityDisplay
{
    public class ActorEntityDetailsDisplayComponent : EntityDetailDisplayComponent
    {
        private Actor currentlyDisplayedActor;
        [SerializeField]
        private TextMeshProUGUI actorName;
        [SerializeField]
        private HUDBarComponent HealthHUDBar;

        public override bool TryHandleSelectable(SelectableComponent currentSelected)
        {
            if(!currentSelected)
            {
                Hide();
                return false;
            }

            if (currentSelected.RootObject.TryGetComponent(out Actor actor))
            {
                if (currentlyDisplayedActor != actor)
                {
                    SetActorsName(actor);
                    SetActorsHealth(actor);
                    currentlyDisplayedActor = actor;
                }
                return true;
            }

            return false;
        }

        private void SetActorsName(Actor actor)
        {
            actorName.text = actor.transform.name;
        }

        private void SetActorsHealth(Actor actor)
        {
            if (currentlyDisplayedActor != null)
                currentlyDisplayedActor.HealthComponent.OnResourceChanged -= ChangeBarData;
            actor.HealthComponent.OnResourceChanged += ChangeBarData;

            ChangeBarData(actor.HealthComponent.Data);
        }

        private void ChangeBarData(ResourceData data)
        {
            HealthHUDBar.SetValues(data.minAmmount, data.maxAmmount,data.currentAmmount, data.previousAmmount);
        }

        public override void Show()
        {
            gameObject.SetActive(true);
        }

        public override void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}