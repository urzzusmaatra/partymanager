﻿using LiveSelection;
using UnityEngine;
namespace UI.EntityDisplay
{
    public abstract class EntityDetailDisplayComponent : MonoBehaviour
    {
        public abstract void Hide();
        public abstract void Show();
        public abstract bool TryHandleSelectable(SelectableComponent currentSelected);
    }
}