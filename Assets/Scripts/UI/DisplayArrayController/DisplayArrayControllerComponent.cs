﻿using UnityEngine;
using UnityEngine.Serialization;
using Services;
using CanvasServices;
using Utility.Logging;

namespace Window.Internal
{
    public class DisplayArrayControllerComponent : MonoBehaviour, IDisplayArrayController
    {
        [SerializeField]
        private HUDDisplayArrayStrategyProvider strategyProvider;
        private IHUDDisplayArrayStrategy strategy;

        [SerializeField, FormerlySerializedAs("display")]
        private DisplayArrayComponent displayObjectPool;

        private ICanvasProvider canvasProvider;

        private void Awake()
        {
            strategy = strategyProvider != null ? strategyProvider.GetStrategy(this) : GetComponent<IHUDDisplayArrayStrategy>();

            if (strategy == null)
            {
                Log.LogBaseError($"Controller is missing its strategy {transform}", transform);
            }

            GameServices.GetService(ReceiveCanvasService);

        }

        private bool ReceiveCanvasService(IService service)
        {
            if (service is ICanvasService canvasService)
            {
                canvasProvider = canvasService.GetCanvasProvider();
                return true;
            }

            return false;
        }

        private void OnDestroy()
        {
            if (strategy != null)
                strategy.Destroy();
        }
        public IDispayableObjectController GetDisplayableObject() => displayObjectPool.AddNew(canvasProvider.Canvas).DispayableObjectController;

        public void ReturnDisplayableObject(IDispayableObjectController displayableObject)
        {
            displayObjectPool.PopObject((displayableObject as MonoBehaviour).GetComponent<DisplayableObjectComponent>());
        }

    }
}