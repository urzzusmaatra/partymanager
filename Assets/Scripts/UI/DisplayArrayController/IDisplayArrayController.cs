﻿using UnityEngine;
/// <summary>
/// Turn this into Array display controller component
/// Let it accept scriptable object that contains relevant information regarding actors (other should have relevant information regarding abilities)
/// and like that make it modular with a possibility to create only scriptable objects that return specific ways of handling displayable objects
/// </summary>

namespace Window
{
    public interface IDisplayArrayController
    {
        IDispayableObjectController GetDisplayableObject();
        void ReturnDisplayableObject(IDispayableObjectController displayableObject);
    }
}