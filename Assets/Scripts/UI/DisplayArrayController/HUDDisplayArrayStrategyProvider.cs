﻿using UnityEngine;
namespace Window
{
    public abstract class HUDDisplayArrayStrategyProvider : ScriptableObject
    {
        public abstract IHUDDisplayArrayStrategy GetStrategy(IDisplayArrayController displayArrayController);
    }
}