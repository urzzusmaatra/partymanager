using AbilitySpace;
using UnityEngine;
using UnityEngine.UI;
using Window;
using FrostSnail.Events;
using Assets.Scripts.UI;
using Assets.Scripts.UI.AbilityHUDSettings;
using PartyManager.Assets.Scripts.UI.Ability;
using PartyManager.Assets.Scripts.UI.Internal;

public class AbilityDisplayHUDComponent : MonoBehaviour, IDispayableObjectController
{
    [SerializeField]
    private Image abilityImageObject;
    [SerializeField]
    private Image cooldownImageObject;

    private IAbility hostingAbility;

    //like this for now
    [SerializeField]
    private AbilityUIRepository abilityUIRepo;

    public void DisplayObject(object objectToDisplay)
    {
        if (objectToDisplay is IAbility ability)
        {
            SetAbility(ability);
        }
    }

    private void SetAbility(IAbility ability)
    {
        if (hostingAbility != ability)
        {
            hostingAbility = ability;
            DisplayAbility();
        }
    }

    private void DisplayAbility()
    {
        if (abilityUIRepo == null)
        {
            UILogChannel.LogError("Cannot display ability. Ability UI Repository is empty!", transform);
            return;
        }

        var abilityUI = abilityUIRepo.GetAbilityUIResource(hostingAbility.Name);
        if (abilityUI == null)
        {
            UILogChannel.LogError($"Cannot display ability. No ability data in repository for ability {hostingAbility.Name}", transform);
            return;
        }

        abilityImageObject.sprite = abilityUI.AbilityHUDImage;
        cooldownImageObject.sprite = abilityUI.AbilityHUDImage;
    }

    public bool IsHostingObject() => hostingAbility != null;

    public bool IsHostingSpecificObject(object queryObject)
    {
        if(IsHostingObject() && queryObject is IAbility ability)
        {
            return hostingAbility == ability;
        }
        return false;
    }

    public void RemoveObject()
    {
        RemoveAbility();
    }

    private void RemoveAbility()
    {
        hostingAbility = null;
        abilityImageObject = null;
    }

    private bool IsDisplayed => hostingAbility != null;

    private void Update()
    {
        if(IsDisplayed)
        {
            cooldownImageObject.fillAmount = hostingAbility.CooldownProgressNormalized;
        }
    }

    public void RequestAbilitySettingDisplay()
    {
        EventFactory.GetEvent(out RequestAbilitySettingDisplayEvent hudEvent);
        hudEvent.Ability = hostingAbility;
        hudEvent.SendTo(UIEventChannelComponent.GetChannel, transform);
    }
}
