﻿using Window;
using UnityEngine;

[CreateAssetMenu(fileName = "AbilityHUDArrayDisplayStrategyProvider", menuName = "Ability/HUDDisplayArrayStrategyProvider")]
public class AbilityHUDDistplayStrategyProvider : HUDDisplayArrayStrategyProvider
{
    public override IHUDDisplayArrayStrategy GetStrategy(IDisplayArrayController displayArrayController)
    {
        return new AbilityHUDArrayDisplayStrategy(displayArrayController);
    }
}