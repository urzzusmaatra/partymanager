using AbilitySpace;
using LiveSelection;
using LiveSelection.Service;
using Services;
using Window;
using System.Collections.Generic;
using PartyManager.Assets.Scripts.ActorEntity;

public class AbilityHUDArrayDisplayStrategy : IHUDDisplayArrayStrategy, ISelectableObjectListener
{
    private ISelectableObjectService selectableService;
    private Actor displayedActor;
    private IReadonlyAbilityCollection abilityCollection;
    private IDisplayArrayController displayArrayController;
    private List<IDispayableObjectController> displayedAbilities;

    public AbilityHUDArrayDisplayStrategy(IDisplayArrayController displayArrayController)
    {
        this.displayArrayController = displayArrayController;
        displayedAbilities = new List<IDispayableObjectController>();
        GameServices.GetService(ReceiveSelectableService);
    }

    public void Destroy()
    {
        if(this.selectableService != null)
        {
            this.selectableService.RemoveListener(this);
        }

        CleanAbilities();

        if(abilityCollection != null)
        {
            abilityCollection.RemoveAbilitiesChangedListener(OnActorsAbilitiesChanged);
            abilityCollection = null;
        }

        displayedActor = null;
    }

    public void OnSelectedObjectChanged(SelectableComponent previous, SelectableComponent current)
    {
        if(current != null && current.RootObject.TryGetComponent(out Actor currentActor))
        {
            SelectedObjectChanged(currentActor);
        }
    }

    private void SelectedObjectChanged(Actor currentlySelected)
    {
        if(currentlySelected != displayedActor)
        {
            CleanAbilities();
            SelectActor(currentlySelected);
        }
    }

    private void SelectActor(Actor actor)
    {
        displayedActor = actor;

        abilityCollection = displayedActor.GetComponent<IReadonlyAbilityCollection>();
        abilityCollection.AddAbilitiesChangedListener(OnActorsAbilitiesChanged);

        DisplayAbilities();
    }

    private void DisplayAbilities()
    {
        if (abilityCollection.HasAbilities)
        {
            foreach (var ability in abilityCollection.GetAllAbilities)
            {
                IDispayableObjectController displayableObject = displayArrayController.GetDisplayableObject();
                displayableObject.DisplayObject(ability.WrappedObject);
                displayedAbilities.Add(displayableObject);
            }
        }
    }

    private void CleanAbilities()
    {
        foreach (var displayedAbility in displayedAbilities)
        {
            displayArrayController.ReturnDisplayableObject(displayedAbility);
        }
        displayedAbilities.Clear();
    }

    private bool ReceiveSelectableService(IService service)
    {
        if (service is ISelectableObjectService selectableService)
        {
            this.selectableService = selectableService;
            this.selectableService.AddListener(this);
            return true;
        }
        return false;
    }

    private void OnActorsAbilitiesChanged()
    {
        CleanAbilities();
        DisplayAbilities();
    }
}
