﻿using Utility.Logging;
using Utility.Logging.Internal;

namespace PartyManager.Assets.Scripts.UI.Internal
{
    public class UILogChannelHandler : ChannelHandler
    {
        public override string ChannelName => UILogChannel.NAME;
    }


    public static class UILogChannel
    {
        public const string NAME = "UIChannel";

        public static void LogError(string message, object obj = null)
        {
            Log.PushError(NAME, message, obj);
        }

        public static void LogWarning(string message, object obj = null)
        {
            Log.PushMessage(NAME, message, obj);
        }

        public static void LogMessage(string message, object obj = null)
        {
            Log.PushMessage(NAME, message, obj);
        }
    }
}
