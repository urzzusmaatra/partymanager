using Utility.Logging;
using FrostSnail.Events.Internal;

namespace Assets.Scripts.UI
{
    public class UIEventChannelComponent : EventChannelComponent
    {
        private static EventChannelComponent _channel;
        public static EventChannelComponent GetChannel
        {
            get
            {
                if (_channel == null)
                {
                    _channel = FindObjectOfType<UIEventChannelComponent>();

                    if (_channel == null)
                    {
                        Log.LogBaseError("There is no channel of type UIEventChannelComponent.");
                    }
                }

                return _channel;
            }
        }
    }
}