﻿using System;
using UnityEngine;

namespace PartyManager.Assets.Scripts.EntityResourceComponent
{
    public abstract class ResourceComponent : MonoBehaviour
    {
        public abstract float CurrentAmmount { get; }
        public abstract bool IsAtMinimum { get; }
        public abstract float CurrentPercent { get; }
        public abstract float MinAmmount { get; }
        public abstract float MaxAmmount { get; }

        public ResourceData Data => ResourceData.CreateBaseData(this);
        public virtual void AddAmmount(float ammount)
        {
            OnResourceChanged?.Invoke(ResourceData.CreateChangedData(this, Mathf.Clamp(CurrentAmmount - ammount, MinAmmount, MaxAmmount)));
        }

        public virtual void RemoveAmmount(float ammount)
        {
            if (IsAtMinimum)
                OnResourceDepleted?.Invoke();
            OnResourceChanged?.Invoke(ResourceData.CreateChangedData(this, Mathf.Clamp(CurrentAmmount + ammount, MinAmmount, MaxAmmount)));
        }


        private bool _isInitialized = false;
        protected virtual bool Initialize()
        {
            if (!_isInitialized)
            {
                _isInitialized = true;
                return true;
            }

            return false;
        }

        private void Awake()
        {
            Initialize();
        }

        public Action OnResourceDepleted;
        /// <summary>
        /// current ammount, current percent, changed ammount
        /// </summary>
        public Action<ResourceData> OnResourceChanged;

    }
}