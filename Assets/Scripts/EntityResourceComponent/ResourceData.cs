﻿namespace PartyManager.Assets.Scripts.EntityResourceComponent
{
    public struct ResourceData
    {
        public readonly float minAmmount;
        public readonly float maxAmmount;
        public readonly float currentAmmount;
        public readonly float previousAmmount;

        public ResourceData(float minAmmount, float maxAmmount, float currentAmmount, float previousAmmount)
        {
            this.minAmmount = minAmmount;
            this.maxAmmount = maxAmmount;
            this.currentAmmount = currentAmmount;
            this.previousAmmount = previousAmmount;
        }

        public static ResourceData CreateBaseData(ResourceComponent resource)
        {
            return new ResourceData(resource.MinAmmount, resource.MaxAmmount, resource.CurrentAmmount, resource.CurrentAmmount);
        }

        public static ResourceData CreateChangedData(ResourceComponent resource, float prevousAmmount)
        {
            return new ResourceData(resource.MinAmmount, resource.MaxAmmount, resource.CurrentAmmount, prevousAmmount);
        }
    }
}