﻿using Services;
using UnityEngine;

namespace LiveSelection.Service
{
    /// <summary>
    /// This "moves" cursor for selections component
    /// And investigates if something is selectable on specific inputs
    /// </summary>
    public class SelectableCursorComponent : MonoBehaviour
    {
        private ISelectableObjectService selectableService;

        private void Start()
        {
            selectableService = GameServices.GetService<ISelectableObjectService>();
        }
        private void Update()
        {
            if (Input.GetButtonDown("Fire1"))//this should be delt with by input manager, and somehow acquired from outside services
            {
                Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                RaycastHit2D hitData = Physics2D.Raycast(new Vector2(worldPosition.x, worldPosition.y), Vector2.zero, 0, SelectableServiceConfiguration.SELECTABLE_OBJECT_LAYER_MASK);
                if (hitData)
                {
                    var hitObject = hitData.transform.gameObject;
                    if(hitObject.TryGetComponent(out SelectableComponent selectableComponent))
                    {
                        selectableComponent.Select();//multiple objects?
                    }
                }
            }
        }
    }
}