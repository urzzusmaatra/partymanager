﻿namespace LiveSelection.Service
{
    public class SelectableServiceConfiguration
    {
        public const int SELECTABLE_OBJECT_LAYER_MASK = 1 << 11;
    }
}