using Services;
using System;
using UnityEngine;
using LiveSelection.Service;
namespace LiveSelection
{
    public class SelectableComponent : MonoBehaviour, ISelectableObjectListener
    {
        private ISelectableObjectService selectableService;
        [SerializeField]
        private Transform rootObject;
        public Transform RootObject { get => rootObject; }
        private void Awake()
        {
            GameServices.GetService(ReceiveService);
        }

        private bool ReceiveService(IService service)
        {
            if (service is ISelectableObjectService selectableObjectService)
            {
                this.selectableService = selectableObjectService;
                this.selectableService.AddListener(this);
                return true;
            }

            return false;
        }

        public void Select()
        {
            OnSelection();
        }

        private void OnSelection()
        {
            selectableService.RequestSelection(this);
        }

        public void OnSelectedObjectChanged(SelectableComponent previous, SelectableComponent current)
        {
            if (previous == this)
            {
                OnSelectionRevoked();
            }
            else if (current == this)
            {
                OnSelectionAproved();
            }
        }

        public Action OnSelectionAprovedEvent;
        private void OnSelectionAproved()
        {
            OnSelectionAprovedEvent?.Invoke();
        }

        public Action OnSelectionRevokedEvent;
        private void OnSelectionRevoked()
        {
            OnSelectionRevokedEvent?.Invoke();
        }
    }
}