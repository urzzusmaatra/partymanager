﻿namespace LiveSelection
{
    public interface ISelectableObjectListener
    {
        void OnSelectedObjectChanged(SelectableComponent previous, SelectableComponent current);
    }
}