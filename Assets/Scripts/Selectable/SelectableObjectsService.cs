﻿using System.Collections.Generic;
using UnityEngine;

namespace LiveSelection.Service.Internal
{
    public class SelectableObjectsService : ISelectableObjectService
    {
        public SelectableComponent CurrentSelected { get; private set; }

        private HashSet<ISelectableObjectListener> listeners;
        private SelectableCursorComponent cursor;
        private SelectableObjectsService()
        {
            listeners = new HashSet<ISelectableObjectListener>();
            cursor = new GameObject("SelectableCursorObject", typeof(SelectableCursorComponent)).GetComponent<SelectableCursorComponent>();
        }

        public static ISelectableObjectService CreateBasicService() => new SelectableObjectsService();

        public void AddListener(ISelectableObjectListener listener)
        {
            listeners.Add(listener);
        }

        public void RemoveListener(ISelectableObjectListener listener)
        {
            listeners.Remove(listener);
        }

        public void RequestSelection(SelectableComponent selectedObject)
        {
            if (CurrentSelected != selectedObject)
            {
                foreach (var listener in listeners)
                {
                    listener.OnSelectedObjectChanged(CurrentSelected, selectedObject);
                }

                CurrentSelected = selectedObject;
            }
        }

        public void ClearSelection()
        {
            foreach (var listener in listeners)
            {
                listener.OnSelectedObjectChanged(CurrentSelected, null);
            }

            CurrentSelected = null;
        }
    }
}
