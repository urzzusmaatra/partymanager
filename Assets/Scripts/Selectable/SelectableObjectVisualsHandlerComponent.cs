using LiveSelection;
using UnityEngine;

public class SelectableObjectVisualsHandlerComponent : MonoBehaviour
{
    [SerializeField]
    private SelectableComponent selectable;
    [SerializeField]
    private VisualsHandlingComponent visualsHandler;
    [SerializeField]
    private VisualsDataCreator unselectedData;
    [SerializeField]
    private VisualsDataCreator selectedData;
    private void Start()
    {
        selectable.OnSelectionAprovedEvent += OnSelectionApprovedCallback;
        selectable.OnSelectionRevokedEvent += OnSelectionRevokedCallback;
    }

    private void OnSelectionApprovedCallback()
    {
        visualsHandler.SetVisualsData(selectedData.GetData());
    }

    private void OnSelectionRevokedCallback()
    {
        visualsHandler.SetVisualsData(unselectedData.GetData());
    }

    private void OnDestroy()
    {
        selectable.OnSelectionAprovedEvent -= OnSelectionApprovedCallback;
        selectable.OnSelectionRevokedEvent -= OnSelectionRevokedCallback;
    }
}
