﻿using Services;
using Services.Internal;
using UnityEngine;

namespace LiveSelection.Service
{
    public class SelectableObjectServiceProviderComponent : MonoBehaviour
    {
        private void Awake()
        {
            GameServices.Provide(ServicesConfiguration.GetSelectableObjectService());
        }

    }
}