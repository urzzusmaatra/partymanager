﻿using Services;

namespace LiveSelection.Service
{
    public interface ISelectableObjectService : IService
    {
        void RequestSelection(SelectableComponent selectedObject);
        void AddListener(ISelectableObjectListener listener);
        void RemoveListener(ISelectableObjectListener listener);
        void ClearSelection();

        SelectableComponent CurrentSelected { get; }

    }
}