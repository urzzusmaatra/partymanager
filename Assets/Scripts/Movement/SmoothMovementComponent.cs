﻿using UnityEngine;

namespace ActorBehaviour
{
    public class SmoothMovementComponent : MovementComponent
    {
        private const float MIN_SPEED = 0f;
        private float _currentMovementSpeed = 0f;
        [SerializeField]
        private float _acceleration = 0f;
        [SerializeField]
        private AnimationCurve _decelerationCurve;
        [SerializeField]
        private float _decelerationDistance = 0f;

        public override float CurrentMovementSpeed => this._currentMovementSpeed;

        public override void Move()
        {
            var vector = DestinationPosition - transform.position;
            var frameDistance = _movementSpeed * Time.deltaTime;
            var sqrMagnitude = vector.sqrMagnitude;

            if (sqrMagnitude > frameDistance * frameDistance)
            {
                if (sqrMagnitude >= _decelerationDistance * _decelerationDistance)
                {
                    Accelerate();
                }
                else
                {
                    Decelerate(sqrMagnitude);
                }

                var direction = vector.normalized;
                transform.position += direction * _currentMovementSpeed * Time.deltaTime;
            }
            else
            {
                transform.position = DestinationPosition;
            }
        }

        private void Accelerate()
        {
            if (this._currentMovementSpeed == this._movementSpeed) return;

            this._currentMovementSpeed += _acceleration * Time.deltaTime;

            if(this._currentMovementSpeed > this._movementSpeed)
            {
                this._currentMovementSpeed = this._movementSpeed;
            }
        }

        private void Decelerate(float sqrDistance)
        {
            if(sqrDistance == 0f)
            {
                this._currentMovementSpeed = 0f;
                return;
            }

            this._currentMovementSpeed = this._decelerationCurve.Evaluate(sqrDistance / (_decelerationDistance * _decelerationDistance)) * this._movementSpeed;
        }
    }
}