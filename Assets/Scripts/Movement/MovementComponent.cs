﻿using Extensions;
using UnityEngine;

namespace ActorBehaviour
{
    public abstract class MovementComponent : MonoBehaviour
    {
        [SerializeField]
        protected float _movementSpeed = 1f;//move this out of here to some config and make a way of calculating it
        public Vector3 DestinationPosition { protected get; set; }

        public abstract float CurrentMovementSpeed { get; }

        public abstract void Move();

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, (transform.position + transform.position.NormalizedDirection(DestinationPosition)));
        }

    }
}