﻿using Extensions;
using UnityEngine;

namespace ActorBehaviour
{
    public class BasicMovementComponent : MovementComponent
    {
        public override float CurrentMovementSpeed => this._movementSpeed;

        public override void Move()
        {
            var vector = DestinationPosition - transform.position;
            var frameDistance = _movementSpeed * Time.deltaTime;

            if (vector.sqrMagnitude > frameDistance * frameDistance)
            {
                var direction = vector.normalized;
                transform.position += direction * _movementSpeed * Time.deltaTime;
            }
            else
            {
                transform.position = DestinationPosition;
            }
        }
    }
}