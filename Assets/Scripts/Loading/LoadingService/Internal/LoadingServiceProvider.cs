using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Services;
using Services.Internal;

public class LoadingServiceProvider : MonoBehaviour
{
    void Start()
    {
        GameServices.Provide(ServicesConfiguration.GetLoadingService());
    }
}