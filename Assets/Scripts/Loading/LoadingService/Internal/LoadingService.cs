using System;
using System.Collections.Generic;
using Services;
using Scripts.CharacterLooks;
using FactionServices;
using Scripts.Entity;
using Map.MapServices;

namespace Loading.LoadingService.Internal
{
    public class LoadingService : ILoadingService
    {
        private bool isServiceLoadingDone = false;
        private Dictionary<Type, bool> servicesLoading;

        public LoadingService()
        {
            servicesLoading = new Dictionary<Type, bool>();
            servicesLoading.Add(typeof(ILoadingService), false);
            servicesLoading.Add(typeof(ICharacterLooksService), false);
            servicesLoading.Add(typeof(ITaskService), false);
            servicesLoading.Add(typeof(IFactionService), false);
            servicesLoading.Add(typeof(IEntityService), false);
            servicesLoading.Add(typeof(IMapService), false);

            GameServices.GetService(GetService);
        }

        public bool IsServiceLoadingDone => isServiceLoadingDone;

        internal static ILoadingService CreateBasicService() => new LoadingService();

        private bool GetService(IService service)
        {
            foreach (var serviceType in service.GetType().GetInterfaces())
            {
                if (IsServiceOnList(serviceType, out Type key))
                {
                    SetServiceLoaded(key);
                    break;
                }
            }

            if (IsLoadingDone())
            {
                if (!isServiceLoadingDone)
                {
                    isServiceLoadingDone = true;
                    onServicesLoaded?.Invoke();
                }
                return true;
            }

            return false;
        }

        private bool IsServiceOnList(Type type, out Type key)
        {
            key = default(Type);
            foreach (var serviceLoading in servicesLoading)
            {
                if (serviceLoading.Key == type)
                {
                    key = serviceLoading.Key;
                    return true;
                }
            }
            return false;
        }

        private void SetServiceLoaded(Type key)
        {
            servicesLoading[key] = true;
        }

        private bool IsLoadingDone()
        {
            foreach (var servicesLoadingState in servicesLoading)
            {
                if (!servicesLoadingState.Value)
                {
                    return false;
                }
            }

            return true;
        }

        public Action onServicesLoaded;
        public void OnServicesLoadedListener(Action serviceLoadedAction)
        {
            if(isServiceLoadingDone)
            {
                serviceLoadedAction();
            }
            else
            {
                onServicesLoaded += serviceLoadedAction;
            }
        }
    }
}