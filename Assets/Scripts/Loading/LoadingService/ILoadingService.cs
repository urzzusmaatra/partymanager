using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Services;
using System;

namespace Loading.LoadingService
{
    public interface ILoadingService : IService
    {
        bool IsServiceLoadingDone { get; }

        void OnServicesLoadedListener(Action serviceLoadedAction);
    }
}