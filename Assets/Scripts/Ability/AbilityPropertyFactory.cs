﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using AbilitySpace.Property;
using Utility.Logging;

namespace AbilitySpace
{
    public static class AbilityPropertyFactory
    {
        public static AbilityProperty GetProperty(string type)
        {
            if (ValidateAbilityProperty(type))
            {
                if (type.Equals("FactionAbilityProperty"))
                {
                    //return new FactionAbilityProperty(type);
                    return null;
                }
                else if (type.Equals("TargetRangeAbilityProperty"))
                {
                    return new TargetRangeAbilityProperty(type);
                }
                else if (type.Equals("TargetHealthRangeAbilityProperty"))
                {
                    return new TargetHealthRangeAbilityProperty(type);
                }

                throw new NotImplementedException("Property of type " + type + " is not yet implemented");
            }

            return null;
        }
        //do not cache this in editor, we are not looking for a preformance here, and it can lead to errors when additional properties are created
#if !UNITY_EDITOR
        private static List<Type> definedProperties;
#endif
        public static bool ValidateAbilityProperty(string property)
        {
#if UNITY_EDITOR
            List <Type> definedProperties = new List<Type>(typeof(AbilityProperty).Assembly.GetTypes().Where(t => t.BaseType == typeof(AbilityProperty)));
#else
            if (definedProperties == null)
            {
                definedProperties = new List<Type>(typeof(AbilityProperty).Assembly.GetTypes().Where(t => t.BaseType == typeof(AbilityProperty)));
            }
#endif
            if (!definedProperties.Contains(Type.GetType(typeof(AbilityProperty).Namespace + "." + property)))
            {
                Log.LogBaseError("Property " + property + " is not defined whithin ability property!");
                PrintDefinedAbilityProperties();
                return false;
            }

            return true;
        }

        public static void PrintDefinedAbilityProperties()
        {
#if UNITY_EDITOR
            List<Type> definedProperties = new List<Type>(typeof(AbilityProperty).Assembly.GetTypes().Where(t => t.BaseType == typeof(AbilityProperty)));
#else
            if (definedProperties == null)
            {
                definedProperties = new List<Type>(typeof(AbilityProperty).Assembly.GetTypes().Where(t => t.BaseType == typeof(AbilityProperty)));
            }
#endif
            var properties = "DefinedProperties: ";
            var separator = ", ";

            foreach (var property in definedProperties)
            {
                properties += property.ToString().Replace(typeof(AbilityProperty)+".",string.Empty) + separator;
            }

            properties.Remove(properties.Length - separator.Length, separator.Length);

            Log.LogBaseMessage(properties);
        }

    }
}