using Utility.Logging.Internal;

public class AbilityLogChannelHandler : ChannelHandler
{
    public override string ChannelName => AbilityLogChannel.NAME;
}

public class AbilityLogChannel
{
    public const string NAME = "AbilityChannel";
}