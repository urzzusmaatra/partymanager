﻿using UnityEngine;
using System.Collections.Generic;

namespace AbilitySpace
{
    using Internal;
    using Processors.Interactions;
    public static class AbilityFactory
    {
        public static Ability CreateAbilityFromData(AbilityData data)
        {
            var ability = GetAbilityInstance(data.AbilityPrefab);

            ability.SetInteractionProcessors(data.AbilityInteractionProcessor);

            ability.SetExecutionProcessors(data.AbilityExecutionProcessors);

            ability.SetProperties(data.Properties);

            //ability.SetUIResources(data.AbilityUIResources);

            ability.SetBaseData(data.AbilityName, data.BaseCooldown, data.ExecutionRange);

            return ability;
        }

        private static Ability GetAbilityInstance(Ability prefab)
        {
            return GameObject.Instantiate(prefab);//pool this eventualy
        }

        private static void SetInteractionProcessors(this Ability ability, InteractionProcessor interactionProcessor)
        {
            var interactionComponent = ability.GetComponent<AbilityInteractionResolver>();
            interactionComponent.SetProcessor(interactionProcessor);
        }

        private static void SetExecutionProcessors(this Ability ability, IReadOnlyList<AbilityExecutionProcessor> processors)
        {
            var executionComponent = ability.GetComponent<AbilityExecutionHandlingComponent>();
            foreach (var processorPrefab in processors)
            {
                var processor = GameObject.Instantiate(processorPrefab);
                executionComponent.AddActivator(processor);
            }
        }
    }

}