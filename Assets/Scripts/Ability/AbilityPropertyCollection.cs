﻿using System.Collections.Generic;
using AbilitySpace.Property;
using UnityEngine;

namespace AbilitySpace.Internal
{
    public class AbilityPropertyCollection
    {
        private List<AbilityProperty> properties;
        private Ability ability;
        private Transform owner;

        public AbilityPropertyCollection(List<AbilityPropertyData> data, Ability ability)
        {
            this.ability = ability;
            properties = new List<AbilityProperty>(data.Count);

            foreach (var abilityPropertyDefinition in data)
            {
                var abilityProperty = abilityPropertyDefinition.ConstructPropertiesOnObject(ability.gameObject);
                properties.Add(abilityProperty);
            }

        }

        public void SetOwner(Transform owner)
        {
            this.owner = owner;

            foreach (var property in properties)
            {
                property.SetupOwner(owner);
            }
        }

        internal void RemoveOwner()
        {
            this.owner = null;

            foreach (var property in properties)
            {
                property.RemoveOwner();
            }
        }

        internal bool IsTargetValid(Transform target)
        {
            foreach (var property in properties)
            {
                if(!property.IsObjectValid(target))
                {
                    return false;
                }
            }

            return true;
        }
    }
}