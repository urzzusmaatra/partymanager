﻿using System.Collections.Generic;
using UnityEngine;
using Extensions;
using System;

namespace AbilitySpace.Internal
{
    public class AbilityCollectionComponent : MonoBehaviour, IAbilityCollector, IReadonlyAbilityCollection
    {
        private List<PriorityObject<Ability>> _abilityCollection;

        private PriorityObject<Ability> _tempWrapper;
        private Action OnAbilitiesChanged;

        private void Awake()
        {
            this._abilityCollection = new List<PriorityObject<Ability>>();
            this._tempWrapper = new PriorityObject<Ability>(null);
        }


        public bool ContainsAbility(Ability ability)
        {
            return this._abilityCollection.Contains(ability);
        }

        public void ProvideAbilities(AbilityProviderComponent abilityProvider)
        {
            bool abilityAdded = false;
            foreach (var ability in abilityProvider.Abilities)
            {
                if(AddTargetAbility(ability))
                {
                    abilityAdded = true;
                }
            }

            if (abilityAdded)
            {
                OnAbilitiesChanged?.Invoke();
            }
        }

        private bool AddTargetAbility(Ability targetAbility)
        {
            if (this._abilityCollection.AddOnce(targetAbility))
            {
                targetAbility.AbilityOwnerAssigned(transform);
                return true;
            }

            return false;
        }

        public void RemoveTargetAbility(Ability targetAbility)
        {
            if (this._abilityCollection.Remove(targetAbility))
            {
                targetAbility.AbilityRemoved();
                OnAbilitiesChanged?.Invoke();
            }
        }

        public void AddAbilitiesChangedListener(Action onAbilitiesChanged)
        {
            OnAbilitiesChanged += onAbilitiesChanged;
        }

        public void RemoveAbilitiesChangedListener(Action onAbilitiesChanged)
        {
            OnAbilitiesChanged -= onAbilitiesChanged;
        }

        public IReadOnlyCollection<PriorityObject<Ability>> GetAllAbilities => this._abilityCollection;

        public bool HasAbilities => _abilityCollection.Count > 0;
    }
}