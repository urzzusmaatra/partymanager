﻿using Utility.Logging;
using Utility.Logging.Drawing;
using UnityEngine;

namespace AbilitySpace.Processors.Interactions
{
    public class HealTargetProcessor : InteractionProcessor
    {
        private HealInteractionData healInteractionData;
        private DebugDrawLine lineDrawer = new DebugDrawLine();
        private DebugDrawWieredSphere sphereDrawer = new DebugDrawWieredSphere();
        public HealTargetProcessor(HealInteractionData healInteractionData)
        {
            this.healInteractionData = healInteractionData;
        }

        public override void ProcessTarget(InteractionData data)
        {
            IDamagable damagable = data.target.GetComponentInChildren<IDamagable>();
            if (damagable != null)
            {
                damagable.ProcessInteraction(HealthInteractionData.CreateBasicHeal(healInteractionData.Ammount));

                DrawLineToTarget(data);
                DrawCircleOnTarget(data);
            }
            else
            {
                Log.PushMessage(AbilityLogChannel.NAME, $"<b>Target {data.target} provided doesn't contain IDamagable type. IDamagable type expected to process damage.</b>");
            }
        }

        private void DrawLineToTarget(InteractionData data)
        {
            lineDrawer.Set(data.target.transform.position, data.attacker.transform.position, Color.cyan, 1f);
            Log.Draw(AbilityLogChannel.NAME, lineDrawer);
        }

        private void DrawCircleOnTarget(InteractionData data)
        {
            sphereDrawer.Set(data.target.transform.position, .3f, Color.cyan, 1f);
            Log.Draw(AbilityLogChannel.NAME, sphereDrawer);
        }
    }
}