﻿using UnityEngine;

namespace AbilitySpace.Processors.Interactions
{
    [CreateAssetMenu(fileName = "HealTargetProcessorStrategyProvider", menuName = "Ability/Processors/HealTargetProcessorStrategyProvider")]
    public class HealTargetInteractionStrategyProvider : InteractionStrategyProvider
    {
        [SerializeField]
        private HealInteractionData healInteractionData;
        public override InteractionProcessor GetStrategy => new HealTargetProcessor(healInteractionData);
    }
}