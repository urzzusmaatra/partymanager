﻿using UnityEngine;

namespace AbilitySpace.Processors.Interactions
{
    [System.Serializable]
    public class HealInteractionData
    {
        [SerializeField]
        private float ammount;
        public float Ammount { get => ammount; }
    }
}