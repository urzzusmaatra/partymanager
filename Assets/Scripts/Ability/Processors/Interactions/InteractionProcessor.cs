﻿using UnityEngine;
using AbilitySpace;

namespace AbilitySpace.Processors.Interactions
{
    public abstract class InteractionProcessor : ScriptableObject
    {
        public abstract void ProcessTarget(InteractionData data);
    }
}