﻿using UnityEngine;

namespace AbilitySpace.Processors.Interactions
{
    public abstract class InteractionStrategyProvider : ScriptableObject
    {
        public abstract InteractionProcessor GetStrategy { get; }
    }
}