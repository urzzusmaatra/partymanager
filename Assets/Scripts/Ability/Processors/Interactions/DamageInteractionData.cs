﻿using UnityEngine;

namespace AbilitySpace.Processors.Interactions
{
    [System.Serializable]
    public class DamageInteractionData
    {
        [SerializeField]
        private float baseDamage;
        public float BaseDamage { get => baseDamage; }
    }
}