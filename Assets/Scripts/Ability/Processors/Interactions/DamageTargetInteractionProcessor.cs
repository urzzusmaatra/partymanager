﻿using UnityEngine;
using Utility.Logging;

namespace AbilitySpace.Processors.Interactions
{
    public class DamageTargetInteractionProcessor : InteractionProcessor
    {
        private DamageInteractionData damageInteractionData;

        public DamageTargetInteractionProcessor(DamageInteractionData damageInteractionData)
        {
            this.damageInteractionData = damageInteractionData;
        }

        public override void ProcessTarget(InteractionData data)
        {
            if (data.target.TryGetComponent(out IDamagable damagable))
            {
                damagable.ProcessInteraction(HealthInteractionData.CreateBasicDamage(damageInteractionData.BaseDamage));
            }
            else
            {
                Log.LogBaseError($"Target {data.target} provided doesn't contain IDamagable type. IDamagable type expected to process damage.");
            }
        }
    }
}