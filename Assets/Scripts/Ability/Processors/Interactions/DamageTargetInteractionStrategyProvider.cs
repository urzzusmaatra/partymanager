﻿using UnityEngine;

namespace AbilitySpace.Processors.Interactions
{
    [CreateAssetMenu(fileName = "DamageTargetProcessorStrategyProvider", menuName = "Ability/Processors/DamageTargetProcessorStrategyProvider")]
    public class DamageTargetInteractionStrategyProvider : InteractionStrategyProvider
    {
        [SerializeField]
        private DamageInteractionData damageInteractionData;
        public override InteractionProcessor GetStrategy => new DamageTargetInteractionProcessor(damageInteractionData);
    }
}