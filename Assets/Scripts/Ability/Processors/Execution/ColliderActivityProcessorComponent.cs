﻿using AbilitySpace;
using UnityEngine;
using PofyTools;
using System;
using Utility.Logging;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class ColliderActivityProcessorComponent : AbilityExecutionProcessor
{
    [SerializeField]
    private Rigidbody2D _rigidBody;
    [SerializeField]
    private Collider2D _attackCollider;
    private Plane attackPlane;
    private bool deactivate = false;
    private void Awake()
    {
        Deactivate();
        Log.LogBaseMessage("Don't forget to change collider hits with direct target hits from ability data.");
    }

    private void Start()
    {
        attackPlane = new Plane(Vector3.forward, _attackCollider.transform.position.z);
    }

    public override void Process(Transform activator, Transform target)
    {
        Log.LogBaseMessage($"[ColliderActivityProcessorComponent:Process] {this}");
        this._rigidBody.WakeUp();
        RotateTowardsTarget(target.position);
        this._attackCollider.enabled = true;
        enabled = true;
        deactivate = false;
    }

    public void Deactivate()
    {
        Log.LogBaseMessage($"[ColliderActivityProcessorComponent:Deactivate] {this}");
        this._rigidBody.Sleep();
        this._attackCollider.enabled = false;
        enabled = false;
    }

    private void FixedUpdate()
    {
        if(deactivate)
        { 
            Deactivate();
        }
        deactivate = true;//one frame delay to process all trigger enters
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Log.LogBaseMessage($"Attack Weapon:{this} Time:{Time.time} Target:{collision.transform}");
        InvokeProcessorResultCallback(collision.transform);
    }

    public override void Shutdown()
    {
        Log.LogBaseMessage($"[ColliderActivityProcessorComponent:Shutdown] {this}");

        Deactivate();
        base.Shutdown();
    }

    private void RotateTowardsTarget(Vector3 targetPosition)
    {
        var attackObject = _attackCollider.transform;
        var attackPosition = attackPlane.ClosestPointOnPlane(targetPosition);
        var angle = Vector3.Angle(attackObject.right, targetPosition - attackObject.position);

        attackObject.RotateAround(attackObject.position, Vector3.forward, angle);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.right + transform.position);
    }
}
