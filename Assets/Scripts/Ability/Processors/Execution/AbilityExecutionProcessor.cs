﻿using System;
using UnityEngine;

namespace AbilitySpace
{
    using Internal;
    public abstract class AbilityExecutionProcessor : MonoBehaviour
    {
        public Action<AbilityExecutionResult> processResult;
        public abstract void Process(Transform activator, Transform target);
        public void RegisterProcessResultCallback(Action<AbilityExecutionResult> action) => processResult += action;
        public virtual void Shutdown() { processResult = null; }
        protected void InvokeProcessorResultCallback(Transform target) => processResult?.Invoke(new AbilityExecutionResult(target));

    }
}