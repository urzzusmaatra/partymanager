﻿using AbilitySpace;
using UnityEngine;

public class TargetProcessorComponent : AbilityExecutionProcessor
{
    public override void Process(Transform activator, Transform target)
    {
        //check if target in line of sight
        InvokeProcessorResultCallback(target);
    }
}