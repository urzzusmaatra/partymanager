﻿using Extensions;
using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace AbilitySpace
{
    using Internal;
    public class AbilityExecutionHandlingComponent : MonoBehaviour
    {
        private List<AbilityExecutionProcessor> _activators;
        private Action<AbilityExecutionResult> resultCallback;

        public void AddActivator(AbilityExecutionProcessor activator)
        {
            if (_activators == null)
            {
                _activators = new List<AbilityExecutionProcessor>();
            }

            _activators.Add(activator);
            activator.RegisterProcessResultCallback(ProcessResultCallback);

            if(activator is MonoBehaviour activatorObject)
            {
                activatorObject.transform.parent = transform;
            }
        }

        [Button("Activate")]
        public void Process(Transform activator, Transform target)
        {
            foreach (var abilityActivator in _activators)
            {
                abilityActivator.Process(activator, target);
            }
        }

        public void RegisterProcessResultCallback(Action<AbilityExecutionResult> action)
        {
            resultCallback += action;
        }

        private void ProcessResultCallback(AbilityExecutionResult result)
        {
            resultCallback?.Invoke(result);
        }

        public void Shutdown()
        {
            resultCallback = null;
            foreach (var activator in _activators)
            {
                activator.Shutdown();
            }

            _activators.Clear();
        }
    }

    
}