﻿using FrostSnail.Events;
using System;
using UnityEngine;
namespace AbilitySpace
{
    using Internal;
    public class AbilityProcessEvent : EventData
    {
        public Ability Ability { get; set; }
        public Transform Target { get; set; }

    }
}

