﻿using UnityEngine;

namespace AbilitySpace.Internal
{
    public struct AbilityExecutionResult
    {
        public readonly Transform target;

        public AbilityExecutionResult(Transform target)
        {
            this.target = target;
        }
    }
}