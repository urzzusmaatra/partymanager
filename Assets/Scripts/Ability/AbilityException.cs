﻿using System;
namespace AbilitySpace
{
    using Internal;
    public class AbilityException : Exception
    {
        public AbilityException(string message) : base(message)
        {
        }

        public AbilityException(string message, Ability ability) : base(message)
        {
            message = ability.Name + ": " + message;
        }
    }
}