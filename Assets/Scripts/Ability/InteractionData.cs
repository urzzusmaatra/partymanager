﻿using UnityEngine;

namespace AbilitySpace.Processors.Interactions
{
    using AbilitySpace.Internal;
    using PartyManager.Assets.Scripts.ActorEntity;

    public struct InteractionData
    {
        public readonly Ability ability;
        public readonly Actor attacker;
        public readonly Transform target;
        public readonly Item item;

        public InteractionData(Actor attacker, Ability ability, Item item ,Transform target)
        {
            this.attacker = attacker;
            this.ability = ability;
            this.target = target;
            this.item = item;
        }
    }
}