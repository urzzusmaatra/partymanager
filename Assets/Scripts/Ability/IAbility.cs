﻿using AbilitySpace.UI;
using System;
using UnityEngine;

namespace AbilitySpace
{
    public interface IAbility
    {
        string Name { get; }
        float CooldownDuration { get; }
        float CooldownProgress { get; }
        float CooldownProgressNormalized { get; }
        float AbilityRange { get; }
        GameObject GetGameObject { get; }

        bool IsActivatable();
        bool IsDistanceInRange(float v);
    }

    public abstract class AbilityEvent : FrostSnail.Events.EventData
    {
        public IAbility Ability { get; set; }

    }

    public class AbilityOwnerAssignedEvent : AbilityEvent
    {
        public Transform Owner { get; set; }
    }

    public class AbilityActivatedEvent : AbilityEvent 
    {
        public Transform Owner { get; set; }
        public Transform Target { get; set; }
    }
    public class AbilityRemovedEvent : AbilityEvent 
    {
    }
}