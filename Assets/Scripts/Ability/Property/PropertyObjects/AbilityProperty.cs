﻿using UnityEngine;
using System.Collections.Generic;

namespace AbilitySpace.Property
{
    //construct ability with properies that decide if action is doable - only yes/no answers
    public abstract class AbilityProperty
    {
        private string propertyName;
        public bool IsInitialized { get; private set; }
        public string PropertyName { get => this.propertyName; }

        public AbilityProperty(string propertyName)
        {
            this.propertyName = propertyName;
            this._propertyAccessorComponents = new List<AbilityPropertyAccessorComponent>();
        }

        public abstract bool IsObjectValid(Transform actor);

        protected virtual bool Initialize()
        {
            if (!IsInitialized)
            {
                AppendPropertiesToComponents();
                IsInitialized = true;
                return true;
            }

            return false;
        }

        private List<AbilityPropertyAccessorComponent> _propertyAccessorComponents;

        internal void AppendDataToComponent(AbilityPropertyAccessorComponent propertyComponent)
        {
            if (IsInitialized)
            {
                propertyComponent.AppendProperty(this);
            }
            else
            {
                this._propertyAccessorComponents.Add(propertyComponent);
            }
        }

        private void AppendPropertiesToComponents()
        {
            for (int i = this._propertyAccessorComponents.Count - 1; i >= 0; i--)
            {
                var component = this._propertyAccessorComponents[i];
                component.AppendProperty(this);
                this._propertyAccessorComponents.RemoveAt(i);
            }
        }

        public abstract void SetupOwner(Transform owner);

        public abstract void RemoveOwner();
    }
}