﻿using PofyTools;
using UnityEngine;
using FrostSnail.Coverage;

namespace AbilitySpace.Property
{
    public abstract class RangeAbilityProperty : AbilityProperty
    {
        protected CoverageArea coverage;
        protected Transform owner; 
        public RangeAbilityProperty(string propertyName, CoverageArea coverage) : base(propertyName) 
        {
            this.coverage = coverage;
        }

        public CoverageArea Coverage => coverage;

        public void SetCurrentRange(float min, float max)
        {
            coverage.CurrentMin = min;
            coverage.CurrentMax = max;
        }

        public override void SetupOwner(Transform owner)
        {
            this.owner = owner;
        }

        public override void RemoveOwner()
        {
            this.owner = null;
        }
    }
}