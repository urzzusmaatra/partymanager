﻿using UnityEngine;
using PofyTools;
using FrostSnail.Coverage;
using Utility.Logging;
using PartyManager.Assets.Scripts.ActorEntity.Resources;

namespace AbilitySpace.Property
{
    public class TargetHealthRangeAbilityProperty : RangeAbilityProperty
    {
        private const float MIN_RANGE = 0f;
        private const float MAX_RANGE = 1f;

        public TargetHealthRangeAbilityProperty(string propertyName) : base(propertyName, CoverageArea.FullCoverage(MIN_RANGE, MAX_RANGE)) 
        {
            Initialize();
        }

        private string LogPrefix => $"<b>Property:{PropertyName} - owner:{owner} - </b>";
        public override bool IsObjectValid(Transform target)
        {
            if(coverage.BaseMax == float.MaxValue)
            {
                Log.LogBaseMessage(LogPrefix + $" Target:{target} - is valid, property range is max allowed");
                return true;
            }

            if (target.TryGetComponent(out HealthComponent healthComponent))
            {
                bool result = coverage.Contains(healthComponent.CurrentPercent);
                Log.LogBaseMessage(LogPrefix + $" Target:{target} - is {result}, range is {coverage} and current health percent is {healthComponent.CurrentPercent}");
                return result;
            }

            Log.LogBaseMessage(LogPrefix + $" Target:{target} - is invalid, all checks failed");
            return false;
        }
    }
}