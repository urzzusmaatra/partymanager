﻿using UnityEngine;
using PofyTools;
using FrostSnail.Coverage;
using Utility.Logging;

namespace AbilitySpace.Property
{
    public class TargetRangeAbilityProperty : RangeAbilityProperty
    {
        private const float MIN_RANGE = 0f;
        private const float MAX_RANGE = 100f;

        public TargetRangeAbilityProperty(string propertyName) : base(propertyName, CoverageArea.FullCoverage(MIN_RANGE, MAX_RANGE)) 
        {
            Initialize();
        }
        private string LogPrefix => $"<b>Property:{PropertyName} - owner:{owner} - </b>";
        public override bool IsObjectValid(Transform target)
        {
            var sqrMagnitude = Vector3.SqrMagnitude(owner.transform.position - target.transform.position);
            var result = coverage.SqrContains(sqrMagnitude);
            Log.LogBaseMessage(LogPrefix + $" Target:{target} - is {result}, current range {coverage} and sqr magnitude {sqrMagnitude}");
            return result;
        }
    }
}