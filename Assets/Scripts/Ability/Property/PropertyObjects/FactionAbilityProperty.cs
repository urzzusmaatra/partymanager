﻿using UnityEngine;
using Services;
using AbilitySpace.Property;
using DecisionMaking.Concretion.Strategy.Data;
using FrostSnail.Events;
using FrostSnail.Factions;
using System.Collections.Generic;
using FactionServices;
using Factions;
using Utility.Logging;
using FrostSnail.Events.Internal;

//TODO add namespace here
public class FactionAbilityProperty : AbilityProperty
{
    private IFactionService factionService;
    private IFactionProvider factionProvider;
    private IFactionCollection targetingFactions;
    private bool targetFactionOverride = false;
    private Transform owner;
    private EventReceiver eventHandler;
    /// <summary>
    /// this property still needs to cover changing of the faction, which means that it needs to define faction realtions result and if not enough to additionaly filter factions
    /// we will cover enemy factions for now until better implementation arrives
    /// </summary>
    /// <param name="propertyName"></param>

    public FactionAbilityProperty(string propertyName) : base(propertyName)
    {
        GameServices.GetService(ReceiveService);

        eventHandler = EventReceiverFactory.CreateEventReceiver();
        eventHandler.RegisterMethod<FactionChangedEventData>(FactionChangedCallback);
    }

    private bool ReceiveService(IService service)
    {
        if (service is IFactionService factionService)
        {
            this.factionService = factionService;
            targetingFactions = factionService.CreateCollection();
            Initialize();
            
            return true;
        }
        return false;
    }

    public IFactionCollection TargetingFactions => targetingFactions;

    private string LogPrefix => $"<b>Property:{PropertyName} - owner:{owner} - </b>";

    public override bool IsObjectValid(Transform target)
    {
#if UNITY_EDITOR
        //this will enable everything to be a target, and allow to have missing faction service
        if (factionService == null)
        {
            Log.LogBaseWarning("Scene is missing faction services. Everything is a target.");
            Log.LogBaseMessage(LogPrefix + $" Target:{target} - is valid, no faction service ");
            return true;
        }
#endif
        if(targetingFactions.AllFactions)
        {
            Log.LogBaseMessage(LogPrefix + $" Target:{target} - is valid, owner targets all ");
            return true;
        }

        if(target.TryGetComponent(out IFactionProvider factionProvider))
        {
            var result = targetingFactions.ContainsFaction(factionProvider.Faction);
            Log.LogBaseMessage(LogPrefix + $" Target:{target} - is {result}, comparing factions {factionProvider.Faction} == {targetingFactions} ");
            return result;
        }

        Log.LogBaseMessage(LogPrefix + $" Target:{target} - is invalid, all checks failed");
        return false;
    }

    public override void SetupOwner(Transform owner)
    {
        UnregisterFromOwnerEvents();

        if (owner.TryGetComponent(out IFactionProvider factionProvider))
        {
            this.factionProvider = factionProvider;
            TargetEnemyRelatedFactions(this.factionProvider.Faction);
            this.owner = owner;
            RegisterToOwnerEvents();
            return;
        }

        throw new System.Exception("Faction ability should be suplied with Actor to get faction!");
    }

    public override void RemoveOwner()
    {
        factionProvider = null;
        targetingFactions.ClearPack();
    }

    private void TargetEnemyRelatedFactions(Faction faction)
    {
        if (targetFactionOverride)
            return;

        if (factionService == null || factionProvider == null)
        {
            targetingFactions = null;//this might not be usefull info
        }
        else
        {
            factionService.GetFactionsWithRelations(FactionRelationQueryResult.Enemy, faction, targetingFactions);
        }

    }

    public void SetTargetFactions(IFactionCollection targetingFactions)
    {
        targetFactionOverride = true;
        this.targetingFactions = targetingFactions;
    }

    private void RegisterToOwnerEvents()
    {
        if(owner.TryGetComponent(out EventChannelComponent eventComponent))
        {
            eventHandler.RegisterComponent(eventComponent);
        }
    }

    private void UnregisterFromOwnerEvents()
    {
        if(owner != null && owner.TryGetComponent(out EventChannelComponent eventComponent))
        {
            eventHandler.UnregisterComponent(eventComponent);
        }
    }

    private void FactionChangedCallback(FactionChangedEventData data)
    {
        if (data.Sender == owner)
        {
            TargetEnemyRelatedFactions(data.Faction);
        }
    }

}
