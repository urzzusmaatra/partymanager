﻿using PofyTools;
using Sirenix.OdinInspector;
using UnityEngine;
using FrostSnail.Coverage;
using Utility.Logging;

namespace AbilitySpace.Property
{
    public class RangeAbilityPropertyAccessorComponent : AbilityPropertyAccessorComponent
    {
        [SerializeField]
        private CoverageArea coverageArea;
        private RangeAbilityProperty property;

        public override void AppendProperty(AbilityProperty property)
        {
            if (property != null && property is RangeAbilityProperty rangeProperty)
            {
                this.property = rangeProperty;
                this.coverageArea = this.property.Coverage;
            }
            else
            {
                Log.LogBaseError($"Wrong type of property supplied to {this}", transform);
            }

        }

        internal void SetCoverageArea(float min, float max)
        {
            property.SetCurrentRange(min, max);
            this.coverageArea = this.property.Coverage;
        }

        internal CoverageArea GetCoverageArea => property.Coverage;

        public override string PropertyName => property.PropertyName;
    }
}