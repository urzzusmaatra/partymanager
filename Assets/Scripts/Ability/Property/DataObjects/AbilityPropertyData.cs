﻿using System;
using UnityEngine;

namespace AbilitySpace.Property
{
    public abstract class AbilityPropertyData : ScriptableObject
    {
        public abstract string Property { get; }

        public abstract AbilityProperty ConstructPropertiesOnObject(GameObject propertyOwner);

    }
}