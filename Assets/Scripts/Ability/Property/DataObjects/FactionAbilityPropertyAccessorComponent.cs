﻿using Services;
using Sirenix.OdinInspector;
using UnityEngine;
using System;
using FactionServices;
using FrostSnail.Factions;
using Utility.Logging;

namespace AbilitySpace.Property
{
    public class FactionAbilityPropertyAccessorComponent : AbilityPropertyAccessorComponent
    {
#if UNITY_EDITOR
        [SerializeField]
        private IFactionCollection targetFactions;
#endif
        private FactionAbilityProperty property;

        public override void AppendProperty(AbilityProperty property)
        {
            if (property != null && property is FactionAbilityProperty factionProperty)
            {
                this.property = factionProperty;
                this.targetFactions = this.property.TargetingFactions;
            }
            else
            {
                Log.LogBaseError($"Wroing type of property supplied to {this}", transform);
            }
        }

        public override string PropertyName => property.PropertyName;

        internal void ChangeFaction(Faction faction)
        {
            //this.property.SetTargetFactions(faction);
            //targetFactions = faction;
        }
#if UNITY_EDITOR
        private void Update()
        {
            if (property != null)
            {
                targetFactions = property.TargetingFactions;
            }
        }
#endif
    }
}