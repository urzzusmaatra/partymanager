﻿using UnityEngine;

namespace AbilitySpace.Property
{
    public abstract class AbilityPropertyAccessorComponent : MonoBehaviour
    {
        public abstract void AppendProperty(AbilityProperty property);
        public abstract string PropertyName { get; }

    }
}