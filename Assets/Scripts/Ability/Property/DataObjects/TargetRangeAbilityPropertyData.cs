﻿using UnityEngine;

namespace AbilitySpace.Property
{
    [CreateAssetMenu(fileName = "TargetRangeAbilityPropertyData", menuName = "Ability/PropertyData/TargetRange")]
    public class TargetRangeAbilityPropertyData : AbilityPropertyData
    {
        public override string Property => "TargetRangeAbilityProperty";

        public override AbilityProperty ConstructPropertiesOnObject(GameObject propertyOwner)
        {
            var accessor = propertyOwner.AddComponent<RangeAbilityPropertyAccessorComponent>();
            var property = new TargetRangeAbilityProperty(Property);
            accessor.AppendProperty(property);
            return property;
        }
    }
}