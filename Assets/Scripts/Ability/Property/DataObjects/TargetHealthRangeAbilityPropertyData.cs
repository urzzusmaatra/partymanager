﻿using UnityEngine;

namespace AbilitySpace.Property
{
    [CreateAssetMenu(fileName = "TargetHealthRangeAbilityPropertyData", menuName = "Ability/PropertyData/TargetHealth")]
    public class TargetHealthRangeAbilityPropertyData : AbilityPropertyData
    {
        public override string Property => "TargetHealthRangeAbilityProperty";

        public override AbilityProperty ConstructPropertiesOnObject(GameObject propertyOwner)
        {
            var accessor = propertyOwner.AddComponent<RangeAbilityPropertyAccessorComponent>();
            var property = new TargetHealthRangeAbilityProperty(Property);
            accessor.AppendProperty(property);
            return property;
        }
    }
}