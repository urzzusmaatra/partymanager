﻿using Services;
using UnityEngine;
using AbilitySpace.Property;
using System.Net;

[CreateAssetMenu(fileName = "FactionAbilityPropertyData", menuName = "Ability/PropertyData/Faction")]
public class FactionAbilityPropertyData : AbilityPropertyData
{
    public override string Property => "FactionAbilityProperty";

    public override AbilityProperty ConstructPropertiesOnObject(GameObject propertyOwner)
    {
        var accessor = propertyOwner.AddComponent<FactionAbilityPropertyAccessorComponent>();
        var property = new FactionAbilityProperty(Property);
        accessor.AppendProperty(property);
        return property;
    }
}
