using UnityEngine;
using Utility.Logging;

namespace AbilitySpace.Processors.Interactions
{
    using AbilitySpace.Internal;
    using PartyManager.Assets.Scripts.ActorEntity;

    public class AbilityInteractionResolver : MonoBehaviour
    {
        private InteractionProcessor targetProcessor;

        public void SetProcessor(InteractionProcessor targetProcessor)
        {
            if (this.targetProcessor == null)
            {
                this.targetProcessor = targetProcessor;
            }
            else
            {
                Log.LogBaseError("Trying to set new target processor when allready set.", transform);
            }
        }

        public void ProcessInteraction(InteractionData data)
        {
            targetProcessor.ProcessTarget(data);
        }

        public static void ResolveInteraction(Actor attacker, IAbility ability, Item item, Transform target)
        {
            var abilityObject = (ability as MonoBehaviour).gameObject;
            if (abilityObject != null && abilityObject.TryGetComponent(out AbilityInteractionResolver interactionResolver))
            {
                interactionResolver.ProcessInteraction(new InteractionData(attacker, ability as Ability, item, target));
            }
        }
    }
}