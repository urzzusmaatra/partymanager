﻿using System.Collections.Generic;
using PofyTools;
using UnityEngine;
using FrostSnail.Events;
using AbilitySpace.UI;
using AbilitySpace.Property;
using FrostSnail.Events.Internal;

namespace AbilitySpace.Internal
{
    public class Ability : MonoBehaviour, IAbility
    {
        private string ability;
        public string Name => this.ability;

        private AbilityPropertyCollection abilityProperties;

        private float baseCooldown;
        private Timer cooldownTimer;

        private Range executionRange;

        [SerializeField]
        private AbilityExecutionHandlingComponent abilityExecutionProcessor;

        public System.Action<AbilityProcessEvent> OnAbilityProcessResultCallback;

        private EventChannelComponent eventBroadcaster;

        private void Awake()
        {
            eventBroadcaster = GetComponent<EventChannelComponent>();
            if(abilityExecutionProcessor == null)
            {
                abilityExecutionProcessor = GetComponent<AbilityExecutionHandlingComponent>();
            }
            abilityExecutionProcessor.RegisterProcessResultCallback(AbilityProcessResultCallback);
        }

        public void SetBaseData(string name, float cooldown, Range range)
        {
            ability = name;

            baseCooldown = cooldown;
            cooldownTimer = new Timer("CooldownTimer", this.baseCooldown);
            cooldownTimer.SetReady();

            executionRange = range;
        }

        public void SetProperties(List<AbilityPropertyData> properties)
        {
            abilityProperties = new AbilityPropertyCollection(properties, this);
        }

        internal bool IsTargetValid(Transform target)
        {
            return abilityProperties.IsTargetValid(target);
        }

        public bool IsDistanceInRange(float distance)
        {
            return executionRange.Contains(distance);
        }
        public void AbilityOwnerAssigned(Transform owner)
        {
            abilityProperties.SetOwner(owner);
            var abilityEvent = EventFactory.GetEvent<AbilityOwnerAssignedEvent>();
            abilityEvent.Ability = this;
            abilityEvent.Owner = owner;
            eventBroadcaster.SendEvent(abilityEvent);
        }

        internal void Activate(Transform activator, Transform target)
        {
            cooldownTimer.ResetTimer();
            abilityExecutionProcessor.Process(activator, target);

            var abilityEvent = EventFactory.GetEvent<AbilityActivatedEvent>();
            abilityEvent.Ability = this;
            abilityEvent.Owner = activator;
            abilityEvent.Target = target;
            eventBroadcaster.SendEvent(abilityEvent);
        }

        public void AbilityRemoved()
        {
            abilityProperties.RemoveOwner();
            OnAbilityProcessResultCallback = null;

            var abilityEvent = EventFactory.GetEvent<AbilityRemovedEvent>();
            abilityEvent.Ability = this;
            eventBroadcaster.SendEvent(abilityEvent);
        }

        public bool IsOnCooldown => !this.cooldownTimer.IsReady;

        public float CooldownDuration => this.baseCooldown;

        public float CooldownProgress => this.cooldownTimer.TimeLeft;

        public float CooldownProgressNormalized => this.cooldownTimer.NormalizedTime;

        public Range AbilityRange => executionRange;

        public GameObject GetGameObject => gameObject;

        float IAbility.AbilityRange => throw new System.NotImplementedException();

        public bool IsActivatable() => !IsOnCooldown;

        private void AbilityProcessResultCallback(AbilityExecutionResult result)
        {
            var processResult = EventFactory.GetEvent<AbilityProcessEvent>();
            processResult.Ability = this;
            processResult.Target = result.target;
            eventBroadcaster.SendEvent(processResult);
        }
    }

}