﻿using System.Collections.Generic;

public class PriorityObject<T> where T : class
{
    public T WrappedObject { get; private set; }
    public int Priority { get; set; }

    public PriorityObject(T wrappedObject) : this(wrappedObject, 0) { }
    public PriorityObject(T wrappedObject, int priority)
    {
        WrappedObject = wrappedObject;
        Priority = priority;
    }

    public static explicit operator T(PriorityObject<T> value) => value.WrappedObject;
    public static implicit operator PriorityObject<T>(T value) => new PriorityObject<T>(value);//#techdebt: this may not be the most optimal way for heavy searches
    public override bool Equals(object obj)
    {
        if(obj is T value)
        {
            return WrappedObject.Equals(value);
        }
        return false;
    }

    public override int GetHashCode()
    {
        return WrappedObject.GetHashCode();
    }
}