﻿using AbilitySpace.Internal;

namespace AbilitySpace
{
    public interface IAbilityCollector
    {
        void ProvideAbilities(AbilityProviderComponent abilityProvider);
        void RemoveTargetAbility(Ability ability);
    }
}