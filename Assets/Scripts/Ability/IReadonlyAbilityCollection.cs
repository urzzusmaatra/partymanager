﻿using System.Collections.Generic;
using System;

namespace AbilitySpace
{
    using Internal;
    public interface IReadonlyAbilityCollection
    {
        IReadOnlyCollection<PriorityObject<Ability>> GetAllAbilities { get; }
        bool HasAbilities { get; }

        void AddAbilitiesChangedListener(Action onAbilitiesChanged);
        void RemoveAbilitiesChangedListener(Action onAbilitiesChanged);
    }
}