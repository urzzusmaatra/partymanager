﻿using UnityEngine;
using System.Collections.Generic;

namespace AbilitySpace
{
    using Internal;
    public class AbilityProviderComponent : MonoBehaviour
    {
        private List<Ability> _abilities = new List<Ability>();
        public IReadOnlyCollection<Ability> Abilities => _abilities.AsReadOnly();

        public void AddAbility(in Ability ability)
        {
            this._abilities.Add(ability);
        }

    }
}