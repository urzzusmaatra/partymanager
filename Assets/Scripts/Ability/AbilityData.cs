﻿using PofyTools;
using System.Collections.Generic;
using UnityEngine;

namespace AbilitySpace
{
    using Property;
    using UI;
    using UI.Internal;
    using Internal;
    using Processors.Interactions;

    [CreateAssetMenu(fileName = "AbilityData", menuName = "Ability/AbilityData")]
    public class AbilityData : ScriptableObject
    {
        [SerializeField]
        private string abilityName = string.Empty;
        [SerializeField]
        private float baseCooldown = 0f;
        [SerializeField]
        private Range executionRange = new Range(1f);
        [SerializeField]
        private List<AbilityPropertyData> properties = null;
        [SerializeField]
        private Ability abilityPrefab;
        public Ability AbilityPrefab { get => abilityPrefab; }
        [SerializeField]
        private List<AbilityExecutionProcessor> abilityProcessors;
        public IReadOnlyList<AbilityExecutionProcessor> AbilityExecutionProcessors { get => abilityProcessors; }
        [SerializeField]
        private InteractionStrategyProvider interactionStrategy;
        public InteractionProcessor AbilityInteractionProcessor { get => interactionStrategy.GetStrategy; }

        public string AbilityName { get => abilityName; }
        public float BaseCooldown { get => baseCooldown; }
        public Range ExecutionRange { get => executionRange; }

        public List<AbilityPropertyData> Properties { get => properties; }

        public Ability GetAbility => AbilityFactory.CreateAbilityFromData(this);

        
        
        
    }
}