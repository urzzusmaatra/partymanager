using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

namespace Utility.Tests
{
    public class AnimationCurveInterpolatorTaskTests
    {
        private AnimationCurve linearZeroToOneCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

        [Test]
        public void InterpolateCurve()
        {
            var task = new AnimationCurveInterpolatorTask();

            Assert.IsTrue(task.CurrentStatus == AnimationCurveInterpolatorTask.Status.Idle);

            task.SetValues(1f, linearZeroToOneCurve);

            Assert.IsTrue(task.CurrentStatus == AnimationCurveInterpolatorTask.Status.AwaitingExecution);

            var step = .098f;
            var numberOfSteps = Mathf.FloorToInt(1f / step);
            
            for (int i = 0; i < numberOfSteps; i++)
            {
                task.Update(step);

                if (i < numberOfSteps)
                    Assert.IsTrue(task.CurrentStatus == AnimationCurveInterpolatorTask.Status.Running, $"Step:{step} Iteration:{i}");
                else
                    Assert.IsTrue(task.CurrentStatus == AnimationCurveInterpolatorTask.Status.Idle, $"Step:{step} Iteration:{i}");
            }

        }
    }
}