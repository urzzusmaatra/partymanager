using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO: Create better namespace for this
namespace Utility
{
    public static class DirectionalEnums
    {
        public enum DirectionX : int
        {
            Left = -1,
            Right = 1
        }

        public enum DirectionY : int
        {
            Down = -1,
            Up = 1
        }

        public enum DirectionXY : int
        {
            None,
            Left,
            Right,
            Down,
            Up
        }

        public static Vector2 GetVector(DirectionX dir) => new Vector2((int)dir, 0);
        public static Vector2 GetVector(DirectionY dir) => new Vector2(0, (int)dir);
        public static Vector2 GetVector(DirectionXY dirx)
        {
            switch (dirx)
            {
                case DirectionXY.Left:
                    return Vector2.left;
                case DirectionXY.Right:
                    return Vector2.right;
                case DirectionXY.Up:
                    return Vector2.up;
                case DirectionXY.Down:
                    return Vector2.down;
                default:
                    return Vector2.zero;
            }
        }

        public static Vector4 GetVector4(DirectionXY dirx)
        {
            switch (dirx)
            {
                case DirectionXY.Left:
                    return new Vector4(1f, 0f);
                case DirectionXY.Right:
                    return new Vector4(0f, 0f, 1f);
                case DirectionXY.Up:
                    return new Vector4(0f, 1f);
                case DirectionXY.Down:
                    return new Vector4(0f, 0f, 0f, 1f);
                default:
                    return new Vector4(0f, 0f);
            }
        }
    }

    [System.Serializable]
    public class DirectionProcessor
    {
        [SerializeField]
        private DirectionalEnums.DirectionXY direction;

        public Vector2 Direction2D => DirectionalEnums.GetVector(direction);

        public Vector4 Direction4D => DirectionalEnums.GetVector4(direction);
    }
}
    