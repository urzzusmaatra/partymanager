using UnityEngine;
using FrostSnail.Events;
using Worlds.Events;
using Map.MapServices.Events;
using Sirenix.OdinInspector;

namespace Utility.Internal
{
    public class GameHelper : MonoBehaviour
    {

        [HideInEditorMode, Button("Regen World")]
        private void ReGenerateWorld()
        {
            EventFactory.GetEvent(out RequestSeedRegenerationEvent seedEvent);
            seedEvent.SendTo(WorldChannel.Channel, transform);

            EventFactory.GetEvent(out RequestMapCreationEvent mapCreationEvent);
            mapCreationEvent.SendTo(MapChannel.Channel, transform);
        }
    }
}