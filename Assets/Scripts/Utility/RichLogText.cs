using UnityEngine;

namespace Utility
{
    [System.Serializable]
    public class RichLogText
    {
        [SerializeField] private Color textColor;
        [SerializeField] private bool color = false;
        [SerializeField] private bool bold = false;
        [SerializeField] private bool italic = false;

        public virtual string EnrichString(string message)
        {
            if (bold)
                BoldIt(ref message);

            if (italic)
                SkewIt(ref message);

            if (color)
                ColorIt(ref message);

            return message;
        }

        private void BoldIt(ref string message)
        {
            message = $"<b>{message}</b>";
        }

        private void SkewIt(ref string message)
        {
            message = $"<i>{message}</i>";
        }

        private void ColorIt(ref string message)
        {
            message = $"<color=#{ColorUtility.ToHtmlStringRGB(textColor)}>{message}</color>";
        }
    }
}