﻿using UnityEngine;
using FrostSnail.Core.DataUtility;

namespace Utility.Data
{
    public class DataSerializer : ISerializationProvider
    {
        public void OverwriteObject<T>(string data, ref T obj)
        {
            JsonUtility.FromJsonOverwrite(data, obj);
        }

        public string SerializeObject<T>(T obj)
        {
            return JsonUtility.ToJson(obj);
        }
    }
}