﻿using UnityEngine;

namespace Utility
{
    [System.Serializable]
    public class StaticRichLogText
    {
        [SerializeField]
        private RichLogText enrichment;
        [SerializeField]
        private string message;
        private string fullMessage;

        public string GetMessage
        {
            get
            {
                if (string.IsNullOrEmpty(message))
                    return null;

                if(string.IsNullOrEmpty(fullMessage))
                {
                    fullMessage = enrichment.EnrichString(message);
                }

                return fullMessage;
            }
        }
    }
}