using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO:Create better namespace for this
namespace Utility
{
    public class AnimationCurveInterpolatorTask
    {
        public enum Status
        {
            Idle,
            Running,
            AwaitingExecution
        }

        private float duration;
        private float remainingDuration;
        private AnimationCurve followerAnimation;
        public Status CurrentStatus { get; private set; }

        public AnimationCurveInterpolatorTask()
        {
            this.duration = 0f;
            remainingDuration = 0f;
            CurrentStatus = Status.Idle;
        }

        public void SetValues(float duration, AnimationCurve followerAnimation)
        {
            this.duration = duration;
            remainingDuration = duration;
            this.followerAnimation = followerAnimation;
            CurrentStatus = Status.AwaitingExecution;
        }

        public float Update(float deltaTime)
        {
            if (CurrentStatus == Status.AwaitingExecution)
                CurrentStatus = Status.Running;

            remainingDuration -= deltaTime;
            if (remainingDuration <= 0f)
            {
                remainingDuration = 0f;
                CurrentStatus = Status.Idle;
            }
            return followerAnimation.Evaluate(remainingDuration / duration);

        }
    }
}