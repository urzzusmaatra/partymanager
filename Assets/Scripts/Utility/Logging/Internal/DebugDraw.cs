using FrostSnail.Core.MessageLogger;
using UnityEngine;
using PofyTools;

namespace Utility.Logging.Internal
{
    public abstract class DebugDraw : IDrawableInstruction
    {
        public bool IsDone => timer.IsReady;
        protected Timer timer;

        protected DebugDraw()
        {
            this.timer = new Timer("Duration", 0f);
        }

        public void Draw()
        {
            var prevColor = Gizmos.color;
            DrawGizmo();
            Gizmos.color = prevColor;
        }

        protected abstract void DrawGizmo();
    }

}