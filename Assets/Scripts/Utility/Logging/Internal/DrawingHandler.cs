﻿using FrostSnail.Core.MessageLogger;
using System.Collections.Generic;

namespace Utility.Logging.Internal
{
    public class DrawingHandler : BaseMessageHandler
    {
        private HashSet<DebugDraw> drawStack;

        private void Awake()
        {
            drawStack = new HashSet<DebugDraw>();
        }

        public void Draw(IDrawableInstruction instruction)
        {
            if (!IsMuted)
            {
                if (instruction is DebugDraw drawable)
                {
                    drawStack.Add(drawable);
                }
                else
                {
                    Log.LogBaseError($"Expected DebugDraw type, got {instruction.GetType()}");
                }
            }
        }

        private void OnDrawGizmos()
        {
            if (drawStack == null)
                return;

            foreach (var drawer in drawStack)
            {
                drawer.Draw();
            }

            drawStack.RemoveWhere(x => { return x.IsDone;});

        }
    }
}