﻿using UnityEngine;
using FrostSnail.Core.MessageLogger;

namespace Utility.Logging.Internal
{
    [RequireComponent(typeof(DrawingHandler))]
    public class LoggingWarningHandler : BaseMessageHandler, ILoggerOutputStream
    {
        private DrawingHandler drawingHandler;

        private void Awake()
        {
            drawingHandler = GetComponent<DrawingHandler>();
        }

        public void Draw(IDrawableInstruction instruction)
        {
            drawingHandler.Draw(instruction);
        }
        public void LogMessage(string message, object obj = null)
        {
            if (!IsMuted)
                Debug.LogWarning(message, (obj as Object));
        }
    }
}