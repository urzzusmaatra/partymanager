﻿using UnityEngine;
using FrostSnail.Core.MessageLogger;
using Utility.Logging;

namespace Utility.Logging.Internal
{
    [RequireComponent(typeof(DrawingHandler))]
    public class LoggingErrorHandler : BaseMessageHandler, ILoggerOutputStream
    {
        private DrawingHandler drawingHandler;

        private void Awake()
        {
            drawingHandler = GetComponent<DrawingHandler>();
        }

        public void Draw(IDrawableInstruction instruction)
        {
            drawingHandler.Draw(instruction);
        }
        public void LogMessage(string message, object obj = null)
        {
            if (!IsMuted)
                Debug.LogError(message, (obj as Object));
        }
    }
}