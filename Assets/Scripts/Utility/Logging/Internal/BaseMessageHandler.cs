﻿using UnityEngine;

namespace Utility.Logging.Internal
{
    public abstract class BaseMessageHandler : MonoBehaviour
    {
        [SerializeField]
        private bool isMuted = false;
        public bool IsMuted { get => isMuted; set => isMuted = value; }

    }
}