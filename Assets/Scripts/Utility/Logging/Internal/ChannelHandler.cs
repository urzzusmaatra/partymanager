﻿using UnityEngine;
using FrostSnail.Core.MessageLogger;

namespace Utility.Logging.Internal
{
    public abstract class ChannelHandler : MonoBehaviour
    {
        public abstract string ChannelName { get; }

        [SerializeField]
        private StaticRichLogText prefix;
        [SerializeField]
        private StaticRichLogText sufix;

        private Channel channel;
        public Channel Channel
        {
            get
            {
                if(channel == null)
                {
                    channel = Messenger.CreateChannel(ChannelName, prefix.GetMessage, sufix.GetMessage);
                }

                return channel;
            }
        }

        [SerializeField]
        private bool mute = false;

        private void Update()
        {
            if (ShouldToggleMute())
            {
                ToggleMute();
            }
        }

        private bool ShouldToggleMute() => Channel.Muted != mute;

        private void ToggleMute()
        {
            if (mute)
            {
                Channel.Mute();
            }
            else
            {
                Channel.Unmute();
            }
        }
    }
}