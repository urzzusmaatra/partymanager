﻿namespace Utility.Logging.Internal
{
    public class BaseChannelHandler : ChannelHandler
    {
        public const string CHANNEL_NAME_BASE = "BaseChannel";
        public override string ChannelName => CHANNEL_NAME_BASE;
    }
}