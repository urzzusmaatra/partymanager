using UnityEngine;
using FrostSnail.Core.MessageLogger;
using System.Collections.Generic;

namespace Utility.Logging.Internal
{
    [ExecuteAlways]
    [RequireComponent(typeof(ILoggerOutputStream))]
    public class LoggingComponent : MonoBehaviour
    {
        private Messenger logMessageStream;
        private Messenger logWarningStream;
        private Messenger logErrorStream;

        private List<ChannelHandler> channelHandlers;

        private void Awake()
        {
            logMessageStream = new Messenger(GetComponent<LoggingMessageHandler>());
            logWarningStream = new Messenger(GetComponent<LoggingWarningHandler>());
            logErrorStream = new Messenger(GetComponent<LoggingErrorHandler>());

            HashSet<string> channelNames = new HashSet<string>();
            channelHandlers = new List<ChannelHandler>(GetComponents<ChannelHandler>());
            foreach (var channelHandler in channelHandlers)
            {
                if (channelNames.Add(channelHandler.ChannelName))
                {
                    var channel = channelHandler.Channel;
                    logMessageStream.AddChannel(channel);
                    logWarningStream.AddChannel(channel);
                    logErrorStream.AddChannel(channel);
                }
            }
        }

        public void Log(string message, Channel channel, LogType type, object obj = null)
        {
            switch (type)
            {
                case LogType.Message:
                    if (logMessageStream != null && logMessageStream.IsValid())
                    {
                        logMessageStream.LogMessage(message, channel, obj);
                    }
                    else
                    {
                        Debug.Log(message, (obj as Object));
                    }
                    break;
                case LogType.Warning:
                    if (logWarningStream != null && logWarningStream.IsValid())
                    {
                        logWarningStream.LogMessage(message, channel, obj);
                    }
                    else
                    {
                        Debug.LogWarning(message, (obj as Object));
                    }
                    break;
                case LogType.Error:
                    if (logErrorStream != null && logErrorStream.IsValid())
                    {
                        logErrorStream.LogMessage(message, channel, obj);
                    }
                    else
                    {
                        Debug.LogError(message, (obj as Object));
                    }
                    break;
                default:
                    break;
            }
        }

        public void Draw(DebugDraw drawable,Channel channel)
        {
            if (logMessageStream != null && logMessageStream.IsValid())
            {
                logMessageStream.Draw(drawable, channel);
            }
        }

        public ChannelHandler GetChannelHandler(string name)
        {
            return channelHandlers.Find(x => x.ChannelName == name);
        }

        public enum LogType : uint
        {
            Message = 0,
            Warning = 1,
            Error = 2
        }
    }

    public struct MessageBuffer
    {

        public string message;
        public Channel channel;
        public object obj;
    }
}