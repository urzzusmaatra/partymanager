﻿using UnityEngine;
using Utility.Logging.Internal;

namespace Utility.Logging
{
    public static class Log
    {
        private static LoggingComponent logger;

        private static LoggingComponent Logger
        {
            get
            {
                if (logger == null)
                    logger = GameObject.FindObjectOfType<LoggingComponent>();

                return logger;
            }
        }

        private static void CreateLog(string message, ChannelHandler channelHandler, LoggingComponent.LogType type, object obj = null)
        {
            if (channelHandler != null)
            {
                Log.Logger.Log(message, channelHandler.Channel, type, obj);
            }
        }

        internal static void Draw(string channelName, DebugDraw drawer)
        {
            if (TryGetChannelHandler(channelName, out ChannelHandler channelHandler))
            {
                Log.Logger.Draw(drawer, channelHandler.Channel);
            }
        }

        public static void Push(string channelName, string message, LoggingComponent.LogType type, object obj = null)
        {
            if(TryGetChannelHandler(channelName, out ChannelHandler channelHandler))
            {
                CreateLog(message, channelHandler, type, obj);
            }
        }

        public static void PushMessage(string channelName, string message, object obj = null) => Push(channelName, message, LoggingComponent.LogType.Message, obj);
        public static void PushWarning(string channelName, string message, object obj = null) => Push(channelName, message, LoggingComponent.LogType.Warning, obj);
        public static void PushError(string channelName, string message, object obj = null) => Push(channelName, message, LoggingComponent.LogType.Error, obj);
        

        public static void LogBaseMessage(string message, object obj = null) => CreateLog(message, BaseChannel, LoggingComponent.LogType.Message, obj);
        public static void LogBaseWarning(string message, object obj = null) => CreateLog(message, BaseChannel, LoggingComponent.LogType.Warning, obj);
        public static void LogBaseError(string message, object obj = null) => CreateLog(message, BaseChannel, LoggingComponent.LogType.Error, obj);

        private static bool TryGetChannelHandler(string channelName, out ChannelHandler channelHandler)
        {
            channelHandler = default(ChannelHandler);
            if (Logger != null)
            {
                channelHandler = Logger.GetChannelHandler(channelName);
                if(channelHandler != default(ChannelHandler))
                {
                    return true;
                }
            }

            return false;
        }

        private static ChannelHandler baseChannel;
        private static ChannelHandler BaseChannel
        {
            get
            {
                if (Logger != null)
                {
                    if (baseChannel == null)
                    {
                         TryGetChannelHandler(BaseChannelHandler.CHANNEL_NAME_BASE, out baseChannel);
                    }
                    
                    return baseChannel;
                }
                return null;
            }
        }
    }
}