﻿using UnityEngine;
using Utility.Logging.Internal;

namespace Utility.Logging.Drawing
{
    public class DebugDrawWieredSphere : DebugDraw
    {
        private Color gizmoColor;
        private Vector3 center;
        private float radius;

        public void Set(Vector3 center, float radius, Color gizmoColor, float duration = 0f)
        {
            timer.SetTimer(duration);
            this.center = center;
            this.radius = radius;
            this.gizmoColor = gizmoColor;
        }

        public void ReSet() => timer.ResetTimer();

        protected override void DrawGizmo()
        {
            Gizmos.color = gizmoColor;
            Gizmos.DrawWireSphere(center, radius);
        }
    }
}