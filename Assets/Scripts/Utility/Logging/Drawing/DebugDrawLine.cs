using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility.Logging.Internal;

namespace Utility.Logging.Drawing
{
    public class DebugDrawLine : DebugDraw
    {
        private Color gizmoColor;
        private Vector3 from, to;

        public void Set(Vector3 from, Vector3 to, Color gizmoColor, float duration = 0f)
        {
            timer.SetTimer(duration);
            this.from = from;
            this.to = to;
            this.gizmoColor = gizmoColor;
        }

        public void ReSet() => timer.ResetTimer();

        protected override void DrawGizmo()
        {
            Gizmos.color = gizmoColor;
            Gizmos.DrawLine(from, to);
        }
    }
}