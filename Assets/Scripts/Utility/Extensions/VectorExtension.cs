﻿using UnityEngine;

namespace PartyManager.Assets.Scripts.Utility.Extensions
{
    public static class VectorExtension
    {
        public static Vector3 GetVector(float x, float y, float z = 0f) => new Vector3(x, y, z);
        public static Vector3 ConvertVector(this Vector2Int vctr) => GetVector(vctr.x, vctr.y);
        public static Vector3 ConvertVector(this Vector2 vctr) => GetVector(vctr.x, vctr.y);
    }
}
