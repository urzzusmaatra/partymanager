using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Map.GenerationStrategies.EasingFunctions;

namespace Utility.Debuging
{
    public class DrawEasingFunction : MonoBehaviour
    {
        [SerializeField]
        private Vector3 start;
        [SerializeField]
        private Vector3 direction;
        [SerializeField]
        private int samples = 10;
        [SerializeField]
        private Color color;
        [SerializeField]
        private EasingFunction easingFunction;
        [SerializeField, Sirenix.OdinInspector.ReadOnly]
        private float calculationDuration = 0f;

        private List<System.Tuple<Vector3, Vector3>> points;

        [Sirenix.OdinInspector.Button("Draw")]
        private void Calculate()
        {
            System.Diagnostics.Stopwatch stopwatch = null;
            stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            points = new List<System.Tuple<Vector3, Vector3>>(samples);

            for (int i = 0; i < samples; i++)
            {
                var step = (1f / samples) * i;
                var value = easingFunction.GetValue(0f, 1f, i, samples);
                Vector3 start = new Vector3(step, 0f) + this.start;
                Vector3 end = new Vector3(step, value) + this.start;
                points.Add(new System.Tuple<Vector3, Vector3>(start, end));
                
            }
            stopwatch.Stop();
            calculationDuration = stopwatch.ElapsedTicks;
        }

        private void OnDrawGizmos()
        {
            if(points != null)
            {
                var gizmoColor = Gizmos.color;
                Gizmos.color = color;

                foreach (var point in points)
                {
                    Gizmos.DrawLine(point.Item1, point.Item2);
                }

                Gizmos.color = gizmoColor;
            }
        }
    }
}