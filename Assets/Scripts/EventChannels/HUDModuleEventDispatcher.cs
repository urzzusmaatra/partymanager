﻿using UnityEngine;
using FrostSnail.Events;
using PartyManager.Assets.Scripts.Entity.Events;
using PartyManager.Assets.Scripts.UI.HUD.Events;

namespace PartyManager.Assets.Scripts.EventChannels
{
    /// <summary>
    /// Dispatches gameplay events to UI. Decoupling service mostly
    /// Should be moved to some other decoupling system to make gameplay and UI truly independant
    /// But currently it keeps both separated at least in their own namespaces
    /// </summary>
    public class HUDModuleEventDispatcher : MonoBehaviour
    {
        private EventReceiver eventHandler;

        private void Start()
        {
            eventHandler = EventReceiverFactory.CreateEventReceiver(GameplayEventChannel.Channel);
            eventHandler.RegisterMethod<ActorAddedEvent>(OnActorAddedEventHandler);
            eventHandler.RegisterMethod<ActorRemovedEvent>(OnActorRemovedEventHandler);
        }

        private void OnDestroy()
        {
            eventHandler.UnregisterAll();
        }

        private void OnActorAddedEventHandler(ActorAddedEvent myEvent)
        {
            EventFactory.GetEvent(out ActorAddedUIEvent newEvent);
            newEvent.Actor = myEvent.Actor;
            newEvent.SendTo(UIEventChannel.Channel);
        }

        private void OnActorRemovedEventHandler(ActorRemovedEvent myEvent)
        {
            EventFactory.GetEvent(out ActorRemovedUIEvent newEvent);
            newEvent.Actor = myEvent.Actor;
            newEvent.SendTo(UIEventChannel.Channel);
        }
    }
}
