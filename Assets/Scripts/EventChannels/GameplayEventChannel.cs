﻿using UnityEngine;
using FrostSnail.Events;
using FrostSnail.Events.Internal;

namespace PartyManager.Assets.Scripts.EventChannels
{
    public class GameplayEventChannel
    {
        private static IEventChannel channel;
        public static IEventChannel Channel
        {
            get
            {
                //take care for this chaching here when reloading scenes etc.
                if (channel == null)
                {
                    var channelObject = new GameObject("GameplayEventChannel", typeof(EventChannelComponent));
                    channelObject.TryGetComponent(out channel);
                }

                return channel;
            }
        }
    }
}
