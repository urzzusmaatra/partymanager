﻿using System.Collections.Generic;
using QumNaming;
using Services;
namespace Services.Internal
{
    public class NamingService : INamingService
    {
        private Qum _godfather;

        public NamingService(Qum godfather)
        {
            _godfather = godfather;
        }

        public Qum Service => this._godfather;

        public static NamingService CreateBasicService(List<NamesCollectionObject> collection)
        {
            Qum godfather = null;

            foreach (var data in collection)
            {
                if(godfather == null)
                {
                    godfather = new Qum(data.GetCollection, false, 50);
                }
                else
                {
                    godfather.AddStructure(data.GetCollection);
                }
            }

            return new NamingService(godfather);
            
        }
    }
}