﻿using QumNaming;

namespace Services
{
    public interface INamingService
    {
        Qum Service { get; }
    }
}