﻿using System.Collections;
using System.Collections.Generic;
using Utility;
using FrostSnail.InputResolving;
/**
* Input requirements
* Different contexts with customizable actions within
* Cotexts should be divided in UI and World (with possible Base)
* Those contexts must be devided within players (I, II)
* Each of the contexts must be divided into actions with customisable key (Action name (with ID) and input key)
*  
*/
namespace Services
{
    public class InputService : IInputService
    {
        private List<InputContextControllerAccessor> inputCollectionAccessors;
        public InputService()
        {
            this.inputCollectionAccessors = new List<InputContextControllerAccessor>();
            LoadOrCreatePlayer(InputObjectID.Controlls_Player1);
        }

        private void LoadOrCreatePlayer(InputObjectID player)
        {
            ///these creators need to be loaded from somewhere, but i think it's safer to be hardcoded, code cannot be lost, but files can
            ///but code needs to be maintained, when "default" changes this should reflect that
            var playerOneInputAccessor = new InputContextControllerAccessor(player.ToString());
            if (!playerOneInputAccessor.IsLoaded)
            {
                playerOneInputAccessor.AddInputToContext(INPUT_CONTEXT_SHIP, "Point", UnityEngine.KeyCode.Mouse0);
                playerOneInputAccessor.AddInputToContext(INPUT_CONTEXT_SHIP, "Move", UnityEngine.KeyCode.Mouse2);
                playerOneInputAccessor.AddInputToContext(INPUT_CONTEXT_SHIP, "Fire Cannon One", UnityEngine.KeyCode.Q);
                playerOneInputAccessor.SaveData();
            }
            inputCollectionAccessors.Add(playerOneInputAccessor);
        }

        public static IInputService CreateBasicService()
        {
            return new InputService();
        }

        public InputService Service => this;

        public InputContextControllerAccessor GetInputControllerAccessor(in string controllerId)
        {
            foreach (var controller in this.inputCollectionAccessors)
            {
                if(controller.CheckId(controllerId))
                {
                    return controller;
                }
            }

            return null;
        }

        public enum InputObjectID
        {
            Controlls_Player1,
            Controlls_Player2
        }

        public const string INPUT_CONTEXT_SHIP = "Ship Controlls";
        public const string INPUT_CONTEXT_UI = "UI Controlls";
        //To be implemented
        public const string ACTION_COLLECTION_PLAYER1_CONTROLLER = "Player1_controller";
        public const string ACTION_COLLECTION_PLAYER2_CONTROLLER = "Player2_controller";
        public const string ACTION_COLLECTION_UI = "UI";
    }

}
