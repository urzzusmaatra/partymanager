using CanvasServices;
using CanvasServices.Internal;
using LiveSelection.Service;
using LiveSelection.Service.Internal;
using FXServices;
using Scripts.Entity;
using FactionServices.Internal;
using FactionServices;
using Scripts.CharacterLooks;
using Loading.LoadingService;
using Loading.LoadingService.Internal;
using Map.MapServices;
using Map.MapServices.Internal;
using Worlds;
using Worlds.Internal;
using Items.Service;
using Items.Service.Internal;
using PartyManager.Assets.Scripts.ActorEntity;

namespace Services.Internal
{
    public static class ServicesConfiguration
    {
        //this way we can switch any service fast within game configuration and not affect rest of the code
        public static IHUDModulesService GetHudService() => HUDModulesService.CreateBasicService();

        public static IHUDContentService GetHUDContentService() => HUDContentService.CreateBasicService();

        public static ICanvasService GetCanvasService() => CanvasService.CreateBasicService();

        public static IEntityService GetEntityService(Actor prefab) => EntityService.CreateBasicService(prefab);//try to delegate this parameter providing somewhere else

        public static IFactionService GetFactionService() => FactionService.CreateBasicService();

        public static IItemDistributionService GetItemDistributionService() => ItemDistributionService.CreateBasicService();

        public static IPathfindingService GetPathfindingService() => PathfindingService.CreateBasicService();

        public static ITaskService GetTaskService() => TaskService.CreateBasicService();

        public static ISelectableObjectService GetSelectableObjectService() => SelectableObjectsService.CreateBasicService();

        public static IFXService GetFXService() => FXService.CreateBasicService();

        public static IMapService GetMapService() => MapService.CreateBasicService();

        public static ICharacterLooksService GetCharacterLooksService() => CharacterLooksService.CreateBasicService();

        public static ILoadingService GetLoadingService() => LoadingService.CreateBasicService();

        public static IWorldService GetWorldService() => WorldService.CreateBasicService();

    }
}