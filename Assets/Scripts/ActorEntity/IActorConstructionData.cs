﻿using PartyManager.Assets.Scripts.ActorEntity;

public interface IActorConstructionData
{
    void SetupActor(Actor actor);
}