using PartyManager.Assets.Scripts.EntityResourceComponent;

public class InfiniteResourceComponent : ResourceComponent
{
    public override float CurrentAmmount => MaxAmmount;

    public override bool IsAtMinimum => false;

    public override float CurrentPercent => 1f;

    public override float MinAmmount => 0f;

    public override float MaxAmmount => float.PositiveInfinity;
}
