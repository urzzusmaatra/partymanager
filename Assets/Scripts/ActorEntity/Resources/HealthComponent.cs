﻿using PartyManager.Assets.Scripts.EntityResourceComponent;
using PofyTools;
using UnityEngine;

namespace PartyManager.Assets.Scripts.ActorEntity.Resources
{
    public class HealthComponent : ResourceComponent
    {
        [SerializeField]
        private float _maxHealth = 0f;

        private Range _health;
        protected override bool Initialize()
        {
            if (base.Initialize())
            {
                _health = new Range(0f, _maxHealth, _maxHealth);
                return true;
            }

            return false;
        }

        public override float CurrentAmmount => _health.Current;

        public override bool IsAtMinimum => _health.AtMin;

        public override float CurrentPercent => _health.CurrentPercentage;

        public override float MinAmmount => _health.min;

        public override float MaxAmmount => _health.max;

        public override void AddAmmount(float ammount)
        {
            _health.Current += ammount;
            base.AddAmmount(ammount);
        }

        public override void RemoveAmmount(float ammount)
        {
            _health.Current -= ammount;
            base.RemoveAmmount(ammount);
        }
    }
}