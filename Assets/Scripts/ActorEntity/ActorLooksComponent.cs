﻿using Services;
using UnityEngine;
using Scripts.CharacterLooks;

[RequireComponent(typeof(VisualsHandlingComponent))]
public class ActorLooksComponent : MonoBehaviour
{
    [SerializeField]
    private VisualsHandlingComponent _visualsHandlingComponent;

    private void Start()
    {
        if(_visualsHandlingComponent == null)
            _visualsHandlingComponent = GetComponent<VisualsHandlingComponent>();

        var characterLooksService = GameServices.GetService<ICharacterLooksService>();
        if(characterLooksService != null)
        {
            ProvideLooks(characterLooksService);
        }
        else
        {
            GameServices.OnServiceAdded += OnServiceAdded;
        }
    }

    private void OnServiceAdded(object service)
    {
        if(service is ICharacterLooksService characterLooksService)
        {
            ProvideLooks(characterLooksService);
            GameServices.OnServiceAdded -= OnServiceAdded;
        }
    }

    private void ProvideLooks(ICharacterLooksService characterLooksService)
    {
        var color = Color.white;
        this._visualsHandlingComponent.SetVisualsData(VisualsDataCreator.CreateData(characterLooksService.GetRandomLooks, color));
    }
}
