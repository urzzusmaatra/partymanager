﻿using AbilitySpace;
using AbilitySpace.Processors.Interactions;
using CubeEngine.Storage;
using PofyTools;
using Services;
using UnityEngine;
using Factions;
using FrostSnail.Events;
using Scripts.CharacterLooks;
using Scripts.Entity;
using FrostSnail.Factions;
using Utility.Logging;
using FrostSnail.Events.Internal;
using PartyManager.Assets.Scripts.EntityResourceComponent;

namespace PartyManager.Assets.Scripts.ActorEntity
{
    [RequireComponent(typeof(FactionComponent))]
    [RequireComponent(typeof(SocketOwnerComponent))]
    [RequireComponent(typeof(IAbilityCollector))]
    [RequireComponent(typeof(EventChannelComponent))]
    public class Actor : MonoBehaviour, IInventoryOwner
    {
        [SerializeField]
        public ResourceComponent _health;

        public float GetMaxWeight => _maxWeight;

        private FactionComponent _faction;
        private SocketOwnerComponent _socketsComponent;
        private Inventory _inventory;//why isn't this a component?
        [SerializeField]
        private float _maxWeight = 100f;
        private IAbilityCollector _abilityCollection;
        private IEntityService _entityService;
        private ICharacterLooksService characterLooksService;
        [SerializeField]
        private EventReceiver eventHandler;
        private EventChannelComponent eventComponent;
        private void Awake()
        {
            GameServices.GetService(ReceiveEntitiyService);
            GameServices.GetService(ReceiveCharacterLooksService);

            if (_health == null)
                _health = GetComponent<ResourceComponent>();
            _health.OnResourceDepleted += Die;

            _faction = GetComponent<FactionComponent>();

            _abilityCollection = GetComponent<IAbilityCollector>();

            _socketsComponent = GetComponent<SocketOwnerComponent>();
            _socketsComponent.OnItemSocketed += OnItemSocketed;
            _socketsComponent.OnItemUnsocketed += OnItemUnsocketed;

            _inventory = new Inventory(this);

            eventHandler = EventReceiverFactory.CreateEventReceiver();
            eventComponent = GetComponent<EventChannelComponent>();
            eventHandler.RegisterMethod<ItemAbilityProcessEvent>(ItemAbilityProcessCallback);
            eventHandler.RegisterComponent(eventComponent);

            //this should be optional
            var damageProcessor = GetComponentInChildren<DamagableComponent>();
            if (damageProcessor != null)
            {
                eventHandler.TryRegisterObject(damageProcessor.gameObject);
                eventHandler.RegisterMethod<DamagableInteractionEvent>(DamagableInteractionEventCallback);
            }
        }

        private void Start()
        {
            _entityService.IntroduceActor(this);//introducing actor in start so we gaing space for initializing all components before introducing actor to the game systems
        }

        private void ProcessDamage(HealthInteractionData data)
        {
            switch (data.interactionType)
            {
                case HealthInteractionData.InteractionType.Heal:
                    HealthComponent.AddAmmount(data.healthAmmount);
                    break;
                case HealthInteractionData.InteractionType.Damage:
                    HealthComponent.RemoveAmmount(data.healthAmmount);
                    break;
                default:
                    Log.LogBaseError($"Interaction type of {data.interactionType} not supported");
                    break;
            }
        }

        private void DamagableInteractionEventCallback(DamagableInteractionEvent eventData)
        {
            ProcessDamage(eventData.HealthInteractionData);
        }

        private void Die()
        {
            OnActorRemoved?.Invoke(this);

            _entityService?.RemoveActor(this);

            gameObject.SetActive(false);

            Destroy(gameObject, .1f);
        }

        public ResourceComponent HealthComponent => _health;

        public Faction GetFaction => _faction.Faction;

        public IHaveSockets SocketOwner => _socketsComponent;

        public System.Action<Actor> OnActorRemoved;

        private void OnItemSocketed(ICanBeSocketed item)
        {
            if (item.transform.TryGetComponent(out Item soceketedItem))
            {
                _abilityCollection.ProvideAbilities(soceketedItem.AbilityProvider);
            }

            if (item.transform.TryGetComponent(out IStoragable storagableItem))
            {
                _inventory.TryAddItem(storagableItem, 1);
            }
        }

        private void OnItemUnsocketed(ICanBeSocketed item)
        {
            if (item is Item soceketedItem)
            {
                foreach (var ability in soceketedItem.AbilityProvider.Abilities)
                {
                    _abilityCollection.RemoveTargetAbility(ability);
                }
            }

            if (item.transform.TryGetComponent(out IStoragable storagableItem))
            {
                _inventory.TryTakeItem(storagableItem, 1);
            }
        }

        private bool ReceiveEntitiyService(IService service)
        {
            if (service is IEntityService entityService)
            {
                _entityService = entityService;
                return true;
            }

            return false;
        }

        private bool ReceiveCharacterLooksService(IService service)
        {
            if (service is ICharacterLooksService characterService)
            {
                characterLooksService = characterService;
                GetComponent<VisualsHandlingComponent>().SetVisualsData(VisualsDataCreator.CreateData(characterLooksService.GetRandomLooks, Color.white));
                return true;
            }

            return false;
        }

        private void ItemAbilityProcessCallback(ItemAbilityProcessEvent itemAbilityProcess)
        {
            //there is probably no need to go this far, weapon can deal with this, it knows the owner
            //lets keep like this until next refactor
            AbilityInteractionResolver.ResolveInteraction(this, itemAbilityProcess.Ability, itemAbilityProcess.Item, itemAbilityProcess.Target);
        }
    }
}