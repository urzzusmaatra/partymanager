﻿using PartyManager.Assets.Scripts.ActorEntity;
using UnityEngine;

public abstract class ActorConstructionData : ScriptableObject, IActorConstructionData
{
    public abstract void SetupActor(Actor actor);
}
