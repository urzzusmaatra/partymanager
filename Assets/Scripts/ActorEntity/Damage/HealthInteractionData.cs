﻿public struct HealthInteractionData
{
    public enum InteractionType
    {
        Heal,
        Damage
    }

    public readonly float healthAmmount;
    public readonly InteractionType interactionType;
    public HealthInteractionData(float damageAmmount, InteractionType interactionType)
    {
        this.healthAmmount = damageAmmount;
        this.interactionType = interactionType;
    }

    public static HealthInteractionData CreateBasicDamage(float damageAmmount)
    {
        return new HealthInteractionData(damageAmmount, InteractionType.Damage);
    }

    public static HealthInteractionData CreateBasicHeal(float healAmmount)
    {
        return new HealthInteractionData(healAmmount, InteractionType.Heal);
    }
}
