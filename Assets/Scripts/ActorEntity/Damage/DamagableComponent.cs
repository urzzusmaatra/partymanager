﻿using FrostSnail.Events;
using FrostSnail.Events.Internal;
using System;
using UnityEngine;
using Utility.Logging;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(EventChannelComponent))]
public class DamagableComponent : MonoBehaviour, IDamagable
{
    private EventChannelComponent eventComponent;

    private void Awake()
    {
        eventComponent = GetComponent<EventChannelComponent>();
    }

    public void ProcessInteraction(HealthInteractionData data)
    {
        var damagableEvent = EventFactory.GetEvent<DamagableInteractionEvent>();
        damagableEvent.HealthInteractionData = data;
        eventComponent.SendEvent(damagableEvent);
    }

    private void OnValidate()
    {
        var collider = GetComponent<Collider2D>();

        if(!collider.isTrigger)
        {
            Log.LogBaseWarning("Setting collider to be trigger", transform);
        }

        if(gameObject.layer != LayerData.DAMAGABLE_LAYER)
        {
            Log.LogBaseWarning("Damaging collider not in damagable layer!", transform);
        }
    }

    [Sirenix.OdinInspector.Button]
    private void givemesomedamage()
    {
        ProcessInteraction(new HealthInteractionData(26f, HealthInteractionData.InteractionType.Damage));
    }
}