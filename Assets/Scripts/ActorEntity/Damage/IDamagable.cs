﻿using System.Collections;
using System.Collections.Generic;

public interface IDamagable
{
    void ProcessInteraction(HealthInteractionData data);
}
