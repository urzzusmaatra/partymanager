﻿using FrostSnail.Events;
using System;

public class DamagableInteractionEvent : EventData
{
    public HealthInteractionData HealthInteractionData { get; set; }

}