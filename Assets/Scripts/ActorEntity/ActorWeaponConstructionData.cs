﻿using PofyTools;
using Services;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using FrostSnail.Broker;
using Items.Service;
using PartyManager.Assets.Scripts.ActorEntity;

[CreateAssetMenu(fileName = "ActorWeaponConstructionData", menuName = "Actor/Weapon Construction")]
public class ActorWeaponConstructionData : ActorConstructionData
{
    [SerializeField]
    [FormerlySerializedAs("startingWeapons")]
    private List<ItemCreationDefinition> items;
    [SerializeField]
    private SerializedBrokerMarket<ItemCreationDefinition> itemsMarketData;//this should come from some single data source, not to be repeated multiple times
    private BrokerMarket<ItemCreationDefinition> itemsMarket;

    public override void SetupActor(Actor actor)
    {
        var itemService = GameServices.GetService<IItemDistributionService>();
        Assert.IsNotNull(itemService, "Could not get IItemDistributionService from game services");

        if (itemsMarket == null)
            itemsMarket = itemsMarketData.GetBrokerMarket();

        var itemDefinition = itemsMarket.GetCommodity();
        var itemInstance = (SocketableItem)ItemFactory.CreateItem(itemDefinition);
        SocketActionRequest.TryEquipItemToOwner(actor.SocketOwner, itemInstance.SocketableComponent, itemInstance.SocketId);
        //foreach (var weaponData in items)
        //{
        //    var weapon = (SocketableItem)ItemFactory.CreateItem(weaponData);
        //    SocketActionRequest.TryEquipItemToOwner(actor.SocketOwner, weapon.SocketableComponent, weapon.SocketId);
        //}
    }
}
