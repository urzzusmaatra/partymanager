﻿using PartyManager.Assets.Scripts.ActorEntity;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MultipleActorConstructionDatas : IActorConstructionData
{
    [SerializeField]
    private List<ActorConstructionData> dataCollection;
    private IActorConstructionData[] constructors;
    public void SetupActor(Actor actor)
    {
        if (constructors != null)
        {
            foreach (var constructor in constructors)
            {
                constructor.SetupActor(actor);
            }
        }

        foreach (var data in dataCollection)
        {
            data.SetupActor(actor);
        }
    }

    public void AttachAdditionalConstructors(IActorConstructionData[] constructors) => this.constructors = constructors;
}