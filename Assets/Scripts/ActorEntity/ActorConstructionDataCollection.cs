﻿using PartyManager.Assets.Scripts.ActorEntity;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ActorConstructionCollection", menuName = "Actor/Construction Collection")]
public class ActorConstructionDataCollection : ActorConstructionData
{
    [SerializeField]
    private List<ActorConstructionData> dataCollection;

    public override void SetupActor(Actor actor)
    {
        foreach (var data in dataCollection)
        {
            data.SetupActor(actor);
        }
    }
}
