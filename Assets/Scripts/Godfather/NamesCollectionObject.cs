﻿using UnityEngine;

namespace QumNaming
{
    [CreateAssetMenu(fileName = "GodfatherNamesCollection", menuName = "Godfather/NamesCollection")]
    public class NamesCollectionObject : ScriptableObject
    {
        [SerializeField]
        private float _chanse = 1f;
        [SerializeField]
        private string[] _names = null;

        public NamesCollection GetCollection => new NamesCollection(_names, _chanse);
    }
}
