﻿using System.Collections.Generic;
using UnityEngine;

namespace QumNaming
{
    public class Qum
    {
        private int _namingStructureCount;
        private List<NamesCollection> namingStructure;
        private ICollection<string> _usedNames;
        private bool _allowRepeating;
        private int _repeatBufferSize;

        public Qum(NamesCollection startingStructure, bool allowRepeating = false, int repeatBufferSize = -1)
        {
            this.namingStructure = new List<NamesCollection>() { startingStructure };
            this._namingStructureCount = 1;
            if (repeatBufferSize == -1)
                this._usedNames = new HashSet<string>();
            else
                this._usedNames = new SortedSet<string>();

            this._allowRepeating = allowRepeating;
            this._repeatBufferSize = repeatBufferSize;
        }

        public void AddStructure(NamesCollection newStructure)
        {
            this.namingStructure.Add(newStructure);
            this._namingStructureCount++;
        }

        public string GetSeededName(int seed)
        {
            var initialState = Random.state;
            Random.InitState(seed);
            var name = GetName;
            Random.state = initialState;
            return name;
        }

        public string GetName
        {
            get
            {
                var newName = string.Empty;

                do
                {
                    for (int i = 0; i < this._namingStructureCount; i++)
                    {
                        var content = namingStructure[i];

                        if (content.Chance)
                        {
                            newName += content.RandomName;
                        }
                    }
                }
                while (IsDuplicate(newName));

                CollectNewName(newName);

                return newName;
            }
        }

        #region Repeating handling
        private bool IsDuplicate(string name)
        {
            if (this._allowRepeating)
                return false;

            if (this._usedNames.Contains(name))
                return true;

            return false;
        }

        private void CollectNewName(string name)
        {
            if (this._allowRepeating)
                return;

            if (this._repeatBufferSize != -1)
                if (this._repeatBufferSize >= this._usedNames.Count)
                    ClearHistory();

            this._usedNames.Add(name);
        }

        public void ClearHistory()
        {
            this._usedNames.Clear();
        }
        #endregion
    }
}
