﻿using UnityEngine;

namespace QumNaming
{
    [System.Serializable]
    public class NamesCollection
    {
        public readonly int namesCount;
        public readonly string[] names;
        [Range(0f,1f)]
        public readonly float chance;

        public NamesCollection(string[] names, float chance = 1f)
        {
            this.names = names;
            this.chance = chance;
            this.namesCount = this.names.Length;
        }

        public NamesCollection(float chance = 1f, params string[] names)
        {
            this.names = names;
            this.chance = chance;
            this.namesCount = this.names.Length;
        }

        public bool Chance => this.chance > Random.value;

        public string RandomName => names[Random.Range(0, this.namesCount)];
    }
}
