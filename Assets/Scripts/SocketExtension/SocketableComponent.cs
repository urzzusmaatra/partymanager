﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PofyTools;
using System;

public class SocketableComponent : MonoBehaviour, ICanBeSocketed
{
    public Socket Socket { get; private set; }

    public bool IsEquipped { get; private set; }

    private bool _isInitialized = false;
    public bool IsInitialized => this._isInitialized;


    private void Awake()
    {
        Initialize();
    }

    public Action<IHaveSockets> OnSocketableEquiped;
    public void Equip(SocketActionRequest request, bool inPlace = false)
    {
        this.Socket = request.Socket;
        this.IsEquipped = true;

        request.Item.transform.SetParent(request.Socket.transform);
        if(!inPlace)
        {
            request.Item.transform.localPosition = Socket.socketPositionOffset;
        }

        OnSocketableEquiped?.Invoke(request.Owner);
    }

    public bool Initialize()
    {
        if(!this._isInitialized)
        {
            this._isInitialized = true;
            return true;
        }

        return false;
    }

    public SocketActionRequest ResolveRequest(SocketActionRequest request)
    {
        if(!this.IsEquipped)
        {
            request.ApproveByItem(this);
        }
        else
        {
            request.RevokeApproval();
        }

        return request;
    }

    public Action<IHaveSockets> OnSocketableUnequiped;

    public void Unequip(SocketActionRequest request)
    {
        this.Socket = null;
        this.IsEquipped = false;
        request.Item.transform.parent = null;
        OnSocketableUnequiped?.Invoke(request.Owner);
    }
}
