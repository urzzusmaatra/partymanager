﻿using Extensions;
using PofyTools;
using System;
using System.Collections.Generic;
using UnityEngine;

public class SocketOwnerComponent : MonoBehaviour, IHaveSockets
{
    private bool _isInitialized = false;
    public bool IsInitialized => this._isInitialized;

    private List<Socket> _sockets;

    private void Awake()
    {
        Initialize();
    }

    public bool AddSocket(Socket socket)
    {
        return this._sockets.AddOnce(socket);
    }

    public Socket GetSocket(string id)
    {
        foreach (var socket in this._sockets)
        {
            if (socket.id.Equals(id))
            {
                return socket;
            }
        }

        return null;
    }

    public Socket GetSocket(int hash)
    {
        foreach (var socket in this._sockets)
        {
            if(socket.IdHash == hash)
            {
                return socket;
            }
        }

        return null;
    }

    public bool Initialize()
    {
        if(!this._isInitialized)
        {
            this._isInitialized = true;
            this._sockets = new List<Socket>(GetComponentsInChildren<Socket>());
            return true;
        }

        return false;
    }

    public Action<ICanBeSocketed> OnItemSocketed;
    public void OnItemEquip(SocketActionRequest request)
    {
        OnItemSocketed?.Invoke(request.Item);
    }

    public Action<ICanBeSocketed> OnItemUnsocketed;
    public void OnItemUnequip(SocketActionRequest request)
    {
        OnItemUnsocketed?.Invoke(request.Item);
        Destroy(request.Item.transform.gameObject, 1f);
    }

    public bool RemoveSocket(Socket socket)
    {
        return this._sockets.Remove(socket);
    }

    public SocketActionRequest ResolveRequest(SocketActionRequest request)
    {
        request.ApproveByOwner(this);
        return request;
    }

    public bool UnequipAll(SocketActionRequest.SocketingParty approvedBy = SocketActionRequest.SocketingParty.None)
    {
        foreach (var socket in this._sockets)
        {
            if (!socket.IsEmpty)
            {
                foreach (var item in socket.Items)
                {
                    SocketActionRequest.TryUnequipItemFromOwner(this, item, approvedBy: approvedBy);
                }
            }
        }

        return true;
    }
}
