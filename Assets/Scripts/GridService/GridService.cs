using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridService : IGridService
{
    public GridService() { }

    public IGridService CreateBasicService() { return new GridService(); }

    private Grid _grid;
    public Grid GetGrid => this._grid;

    public void ProvideGrid(Grid grid)
    {
        this._grid = grid;
    }
}
