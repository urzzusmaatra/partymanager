﻿using UnityEngine;
using FrostSnail.Events;
using FrostSnail.Events.Internal;

namespace Worlds.Events
{
    public static class WorldChannel
    {
        private static IEventChannel channel;
        public static IEventChannel Channel
        {
            get
            {
                //take care for this chaching here when reloading scenes etc.
                if (channel == null)
                {
                    var channelObject = new GameObject("WorldChannel", typeof(EventChannelComponent));
                    channelObject.TryGetComponent(out channel);
                }

                return channel;
            }
        }
    }
}