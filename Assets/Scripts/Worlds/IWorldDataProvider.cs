﻿namespace Worlds
{
    public interface IWorldDataProvider
    {
        float FloraStrenght { get; }
        float RockStrenght { get; }
        float WaterStrenght { get; }
        float GrassStrenght { get; }
        uint WorldSeed { get; }

        //TODO: Eventualy switch to this type of data
        //float Temperature { get; }
        //float Humidity { get; }
        //float Height { get; }

    }
}