﻿using UnityEngine;
using Worlds.Internal;
using FrostSnail.Events;
using Worlds.Events;

namespace Worlds.Implementations
{
    //this will later hold it's own noise evaluators to gain these values
    //current values represent 1x1 position i world which is 50x50 (or X * Y) in map

    [RequireComponent(typeof(World))]
    [RequireComponent(typeof(SeedProviderComponent))]
    public class FakeWorldDataProvider : MonoBehaviour, IWorldDataProvider
    {
        [SerializeField, Range(0f, 1f)]
        private float floraStrenght = 1f;
        public float FloraStrenght => floraStrenght;

        [SerializeField, Range(0f, 1f)]
        private float rockStrenght = 1f;
        public float RockStrenght => rockStrenght;

        [SerializeField, Range(0f, 1f)]
        private float waterStrenght = 1f;
        public float WaterStrenght => waterStrenght;

        [SerializeField, Range(0f, 1f)]
        private float grassStrenght = 1f;
        public float GrassStrenght => grassStrenght;

        private SeedProviderComponent seedProvider;
        public uint WorldSeed => seedProvider.GetSeed();

        private EventReceiver eventHandler;

        private void Start()
        {
            ///testing out the principle of dependency injection
            ///as we get agility that implementation depends on the higher structural entity
            ///higher structural entity becomes agnostic on where required objects existance and managament

            GetComponent<World>().SetDataProvider(this);

            seedProvider = GetComponent<SeedProviderComponent>();

            eventHandler = EventReceiverFactory.CreateEventReceiver(WorldChannel.Channel);
            eventHandler.RegisterMethod<RequestSeedRegenerationEvent>(RequestSeedRegenerationEventReaction);
        }

        private void OnDestroy()
        {
            eventHandler.UnregisterMethod<RequestSeedRegenerationEvent>(RequestSeedRegenerationEventReaction);
            eventHandler.UnregisterComponent(WorldChannel.Channel);
        }
        private void RequestSeedRegenerationEventReaction(RequestSeedRegenerationEvent data) => seedProvider.GenerateSeed();
    }
}