﻿using UnityEngine;

namespace Worlds.Implementations
{
    public class SeedProviderComponent : MonoBehaviour
    {
        [SerializeField]
        private uint seed;
        public uint GetSeed() => seed;

        [Sirenix.OdinInspector.Button("Generate Seed")]
        public void GenerateSeed()
        {
            uint first = (uint)Random.Range(0, int.MaxValue) << 16;
            uint second = (uint)Random.Range(0, int.MaxValue);
            uint fullRange = first | second;
            seed = fullRange;
        }
    }
}