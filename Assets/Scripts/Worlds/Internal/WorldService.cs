using FrostSnail.Events;
using UnityEngine;
using Utility.Logging;

namespace Worlds.Internal
{
    public class WorldService : IWorldService
    {
        public World World { private get; set; }
        private Vector2Int position = Vector2Int.zero;
        public static WorldService CreateBasicService() => new WorldService();

        public WorldChunkInfo CurrentWorldChunkInfo => World.GetWorldChunkInfo(position);
        public uint Seed => World.Seed;


        public IEventChannel GetChannel
        {
            get
            {
                if (World.TryGetComponent(out IEventChannel eventChannel))
                {
                    return eventChannel;
                }

                Log.PushError(WorldLogChannel.NAME, "World event channel not setup on world object.", World.transform);
                return null;
            }
        }

        public void ChangePosition(Vector2Int newPosition) => position = newPosition;

        public void MoveUp() => ChangePosition(position + Vector2Int.up);
        public void MoveDown() => ChangePosition(position + Vector2Int.down);
        public void MoveLeft() => ChangePosition(position + Vector2Int.left);
        public void MoveRight() => ChangePosition(position + Vector2Int.right);

    }
}