﻿using UnityEngine;
using Services;
using Services.Internal;

namespace Worlds.Internal
{
    [RequireComponent(typeof(World))]
    public class WorldServiceProvider : MonoBehaviour
    {
        private void Start()
        {
            var worldService = (WorldService)ServicesConfiguration.GetWorldService();
            worldService.World = GetComponent<World>();
            GameServices.Provide(worldService);
        }
    }
}