﻿using Utility.Logging.Internal;

namespace Worlds.Internal
{
    public class WorldLogChannelHandler : ChannelHandler
    {
        public override string ChannelName => WorldLogChannel.NAME;
    }

    public class WorldLogChannel
    {
        public const string NAME = "WorldChannel";
    }
}