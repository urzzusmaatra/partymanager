﻿using UnityEngine;

namespace Worlds.Internal
{
    public class World : MonoBehaviour
    {
        private IWorldDataProvider dataProvider;

        internal void SetDataProvider(IWorldDataProvider dataProvider)
        {
            this.dataProvider = dataProvider;
        }

        internal WorldChunkInfo GetWorldChunkInfo(Vector2Int position)
        {
            var chunkInfo = new WorldChunkInfo();

            chunkInfo.floraStrenght = dataProvider.FloraStrenght;
            chunkInfo.waterStrenght = dataProvider.WaterStrenght;
            chunkInfo.grassStrenght = dataProvider.GrassStrenght;
            chunkInfo.rockStrenght = dataProvider.RockStrenght;

            return chunkInfo;
        }

        internal uint Seed => dataProvider.WorldSeed;
    }
}