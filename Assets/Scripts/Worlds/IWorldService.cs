﻿using Services;
using FrostSnail.Events;

namespace Worlds
{
    public interface IWorldService : IService
    {
        WorldChunkInfo CurrentWorldChunkInfo { get; }
        uint Seed { get; }

        IEventChannel GetChannel { get; }
    }
}